﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

public class DigitPrint : MonoBehaviour
{

    [SerializeField]
    private int number = 0;

    private float blinkTimer = 0;

    [SerializeField]
    private bool blink = false;

    [SerializeField]
    private float maxWidth = 5;//Fixed sizes for now
    [SerializeField]
    private float maxHeight = 8;//Fixed sizes for now

    [SerializeField]
    private GameObject[] digitPieces;
    /*	Initialization Procedure
	 *	Each digit is composed of the following pieces in order
	 *	[0] = Top Line
	 *	[1] = Top Left Edge
	 *	[2] = Mid Line
	 *	[3] = Bottom Left Edge
	 *	[4] = Bottom Line
	 *	[5] = Top Right Edge
	 *	[6] = Bottom Right Edge
	 *	[7] = Top Vertical Middle
	 *	[8] = Bottom Vertical Middle*
	 *	Turn/activate any of them to create a correct cardinal digit
	 *		in the function changeDigitTo(int cardinalDigit)
	//*/

    //	void Awake()
    //	{
    //		char[] charArray;
    //
    //		char[] delimiterComma = {',',':','>'};
    //
    //		string[] msgSplits =_str.Split(delimiterComma);
    //
    //		charArray = new char[globalSplitStr.Length];
    //		for (int i = 0; i < globalSplitStr.Length; i++)
    //		{
    //			charArray[i] = globalSplitStr[i];
    //		}
    //	}
    //
    //
    //	void displayGlyph(string _str)
    //	{
    //
    //	}

    void Start()
    {
        digitToggles = new bool[digitPieces.Length];

        for (int i = 0; i < digitPieces.Length; i++)
        {
            digitToggles[i] = digitPieces[i].activeSelf;
        }

        changeDigitTo(0);
    }

    public float getWidth()
    {
        return maxWidth;
    }
    public float getHeight()
    {
        return maxHeight;
    }

    bool[] digitToggles;

    void Awake()
    {
        blinkTimer = Time.time;
    }

    public void setNumber(int _number)
    {
        number = _number;
    }

    public void changeDigitTo(int cardinalDigit)
    {
        //	First turn all of them on
        foreach (GameObject gObj in digitPieces)
        {
            gObj.SetActive(true);
        }
        switch (number)
        {
            case 0:
                digitPieces[2].SetActive(false);
                break;
            case 1:
                digitPieces[0].SetActive(false);
                digitPieces[1].SetActive(false);
                digitPieces[2].SetActive(false);
                digitPieces[3].SetActive(false);
                digitPieces[4].SetActive(false);

                break;
            case 2:
                digitPieces[1].SetActive(false);
                digitPieces[6].SetActive(false);
                break;
            case 3:
                digitPieces[1].SetActive(false);
                digitPieces[3].SetActive(false);
                break;
            case 4:
                digitPieces[0].SetActive(false);
                digitPieces[3].SetActive(false);
                digitPieces[4].SetActive(false);
                break;
            case 5:
                digitPieces[5].SetActive(false);
                digitPieces[3].SetActive(false);
                break;
            case 6:
                digitPieces[5].SetActive(false);
                break;
            case 7:
                digitPieces[1].SetActive(false);
                digitPieces[2].SetActive(false);
                digitPieces[3].SetActive(false);
                digitPieces[4].SetActive(false);
                break;
            case 9:
                digitPieces[3].SetActive(false);
                digitPieces[4].SetActive(false);
                break;
            case 8:
            default:
                break;
        }
        digitPieces[7].SetActive(false);
        digitPieces[8].SetActive(false);

    }

    // Use this for initialization


    // Update is called once per frame
    void Update()
    {
        if (blink)
        {

            if (blinkTimer > (Time.time + .5f))
            {
                blinkTimer = Time.time;
            }
        }
    }
}

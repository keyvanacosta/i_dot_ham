﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class AudioMechanics : MonoBehaviour
{

	AudioSource speaker;

	public AudioClip[] sfxList;

	public AudioClip[] musicList;

	private Dictionary<string,AudioClip> soundByName;

	[SerializeField]
	private float mainVolume = .5f;

	void Awake()
	{

		speaker = gameObject.AddComponent<AudioSource>() as AudioSource;

		soundByName = new Dictionary<string, AudioClip>();

		foreach (AudioClip sfx in sfxList)
		{
			soundByName.Add(sfx.name, sfx);
		}
		if (sfxList.Length > 0)
		{
			soundByName.Add("", sfxList[0]);
		}
		foreach (AudioClip song in musicList)
		{
			soundByName.Add(song.name, song);
		}
	}

	// Use this for initialization
	void Start()
	{
		//speaker.PlayOneShot(soundByName["Junk"]);
	}

	void setVolume(float _vol = -1)
	{
//		Debug.Log ("Sound Volume Set");
		if (_vol < 0)
			speaker.volume = mainVolume;
		speaker.volume = _vol;
	}

	void setvolume(float _vol = -1)
	{
//		Debug.Log ("Sound Volume Set");
		if (_vol < 0)
			speaker.volume = mainVolume;
		speaker.volume = _vol;
	}

	void Play(string _str = "")
	{
//		Debug.Log ("Sound Play");
//		if (soundByName.Count > 0)
//		{
//			if (_str == "")
//			{
//				speaker.PlayOneShot(sfxList[0]);
//				return;
//			}
//		}	

		speaker.PlayOneShot(soundByName[_str]);
	}

	void play(string _str = "")
	{
		//		Debug.Log ("Sound Play");
		speaker.PlayOneShot(soundByName[_str]);
	}

	// Update is called once per frame
	void Update()
	{
		
	}
}

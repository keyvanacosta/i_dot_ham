﻿using UnityEngine;
using System.Collections;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

namespace MuninuM
{
    public partial class GenericBehavior : MonoBehaviour
    {

        /// <summary>
        /// Accessors for private variables
        /// </summary>
        public bool ObjectPause
        {
            get { return objectPause; }
            set { objectPause = value; }
        }

        public bool ExpandPrivates
        {
            get { return expandPrivates; }
            set { expandPrivates = value; }
        }

        public bool DontDestroy
        {
            get { return dontDestroy; }
            set { dontDestroy = value; }
        }

        public bool UseRealGravity
        {
            get { return useRealGravity; }
            set { useRealGravity = value; }
        }

        public bool UseFakeGravity
        {
            get { return useFakeGravity; }
            set { useFakeGravity = value; }
        }

        public bool CanInteract
        {
            get { return canInteract; }
            set { canInteract = value; }
        }

        private bool HasRigidbody
        {
            get { return hasRigidbody; }
        }

        private bool HasCollider
        {
            get { return hasCollider; }
        }

        private bool HasRenderer
        {
            get { return hasRenderer; }
        }


        //		public IEnumerator callEvent(string _eventName)
        //		{
        //
        //
        //			return null;
        //		}

    }
}
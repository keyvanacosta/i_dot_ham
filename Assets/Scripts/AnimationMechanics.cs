﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//using System.IO;
/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        ANIMATION INTEGRATION for Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/
public enum VariableType
{
    UNDEFINED = 0,
    BOOL = 1,
    INT = 2,
    FLOAT = 3,
    TRIGGER = 4,
}

[System.Serializable]
public class animGroup
{
    public string name;
    public VariableType variableType = VariableType.UNDEFINED;
    public float values;
    public string stateOwner;

    public animGroup()
    {
        name = "_BlankName";
        variableType = VariableType.UNDEFINED;
        values = 0;
        stateOwner = string.Empty;
    }

}

public class AnimationMechanics : MonoBehaviour
{

    public Animator animator;
    char[] delimiterColon = { ':' };

    /*private List<GameObject> createdObjects;*/
    //private Dictionary<string, Action> boolList;
    private Dictionary<string, bool> boolList;
    private Dictionary<string, float> floatList;
    private Dictionary<string, int> intList;
    private Dictionary<string, string> triggerList;

    public List<animGroup> varsList;

    private Dictionary<string, animGroup> animationVarList;

    public float aniTime = 1;

    // Use this for initialization
    void Awake()
    {
        animationVarList = new Dictionary<string, animGroup>();
        boolList = new Dictionary<string, bool>();
        floatList = new Dictionary<string, float>();
        intList = new Dictionary<string, int>();
        triggerList = new Dictionary<string, string>();
        //animator = new Animator();	
    }

    void Start()
    {
        foreach (animGroup ag in varsList)
        {
            //bool found = false;
            switch (ag.variableType)
            {
                case VariableType.BOOL:
                    boolList.Add(ag.name, (ag.values > 0 ? true : false));
                    break;
                case VariableType.FLOAT:
                    floatList.Add(ag.name, ag.values);
                    break;
                case VariableType.INT:
                    intList.Add(ag.name, (int)ag.values);
                    break;
                case VariableType.TRIGGER:
                default:
                    triggerList.Add(ag.name, ag.name);
                    break;
            }
            animationVarList.Add(ag.name, ag);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //"type.name.value.state"
    public void registeranimation(string registerString)
    {

        string[] msgSplits = registerString.Split(delimiterColon);
        bool found = false;
        switch (msgSplits[0])
        {
            case "bool":
                foreach (animGroup ag in varsList)
                {
                    if (ag.name == msgSplits[1] && ag.stateOwner == msgSplits[3])
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    boolList.Add(msgSplits[1], (msgSplits[2].ToLower() == "true" ? true : false));
                    animGroup tAnimGroup = new animGroup();
                    tAnimGroup.name = msgSplits[1];
                    tAnimGroup.variableType = VariableType.BOOL;
                    tAnimGroup.values = (msgSplits[2].ToLower() == "true" ? 1 : 0);
                    tAnimGroup.stateOwner = msgSplits[3];
                    varsList.Insert(0, tAnimGroup);
                    animationVarList.Add(tAnimGroup.name, tAnimGroup);
                }
                break;
            case "float":
                foreach (animGroup ag in varsList)
                {
                    if (ag.name == msgSplits[1] && ag.stateOwner == msgSplits[3])
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    floatList.Add(msgSplits[1], Convert.ToSingle(msgSplits[2].ToLower()));
                    animGroup tAnimGroup = new animGroup();
                    tAnimGroup.name = msgSplits[1];
                    tAnimGroup.variableType = VariableType.FLOAT;
                    tAnimGroup.values = Convert.ToSingle(msgSplits[2].ToLower());
                    tAnimGroup.stateOwner = msgSplits[3];
                    varsList.Insert(0, tAnimGroup);
                    animationVarList.Add(tAnimGroup.name, tAnimGroup);
                    //floatList.Add(msgSplits[1], Convert.ToSingle(msgSplits[2].ToLower()));
                }
                break;
            case "int":
                foreach (animGroup ag in varsList)
                {
                    if (ag.name == msgSplits[1] && ag.stateOwner == msgSplits[3])
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    intList.Add(msgSplits[1], Convert.ToInt32(msgSplits[2].ToLower()));
                    animGroup tAnimGroup = new animGroup();
                    tAnimGroup.name = msgSplits[1];
                    tAnimGroup.variableType = VariableType.INT;
                    tAnimGroup.values = Convert.ToInt32(msgSplits[2].ToLower());
                    tAnimGroup.stateOwner = msgSplits[3];
                    varsList.Insert(0, tAnimGroup);
                    animationVarList.Add(tAnimGroup.name, tAnimGroup);
                    //intList.Add(msgSplits[1], Convert.ToInt32(msgSplits[2].ToLower()));
                }
                break;
            default:
                foreach (animGroup ag in varsList)
                {
                    if (ag.name == msgSplits[1] && ag.stateOwner == msgSplits[3])
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    triggerList.Add(msgSplits[1], msgSplits[1]);
                    animGroup tAnimGroup = new animGroup();
                    tAnimGroup.name = msgSplits[1];
                    tAnimGroup.variableType = VariableType.INT;
                    tAnimGroup.values = 1;
                    tAnimGroup.stateOwner = msgSplits[3];
                    varsList.Insert(0, tAnimGroup);
                    animationVarList.Add(tAnimGroup.name, tAnimGroup);
                    //triggerList.Add(msgSplits[1], msgSplits[1]);
                }
                break;
        }

    }

    //"name.value"
    public void ModifyEntry(string registerString)
    {
        string[] msgSplits = registerString.Split(delimiterColon);
        animGroup tAnim = animationVarList[msgSplits[0]];
        bool tBool = false;
        switch (tAnim.variableType)
        {
            case VariableType.BOOL:

                switch (msgSplits[1].ToLower())
                {
                    case "1":
                    case "on":
                    case "true":
                        tBool = true;
                        break;
                    case "-1":
                    case "off":
                    case "false":
                        tBool = false;
                        break;
                    case "0":
                    case "toggle":
                    case "flip":
                        tBool = !(tAnim.values > 0 ? true : false);
                        tAnim.values = (tBool ? 1 : 0);
                        break;
                    default:
                        tBool = false;
                        break;
                }
                animator.SetBool(tAnim.name, tBool);
                break;
            case VariableType.FLOAT:
                tAnim.values = Convert.ToSingle(msgSplits[1]);
                animator.SetFloat(tAnim.name, tAnim.values);
                break;
            case VariableType.INT:
                tAnim.values = Convert.ToSingle(msgSplits[1]);
                animator.SetInteger(tAnim.name, (int)tAnim.values);
                break;
            case VariableType.TRIGGER:
            default:
                animator.SetTrigger(tAnim.name);
                break;
        }

    }

    public void CallAnimation(string registerString)
    {

        animGroup tAnim = animationVarList[registerString];

        //		if (!animator.GetCurrentAnimatorStateInfo(0).IsName(tAnim.stateOwner))
        //		{
        //			return;
        //		}

        switch (tAnim.variableType)
        {
            case VariableType.BOOL:
                animator.SetBool(tAnim.name, (tAnim.values > 0 ? true : false));
                break;
            case VariableType.FLOAT:
                animator.SetFloat(tAnim.name, tAnim.values);
                break;
            case VariableType.INT:
                animator.SetInteger(tAnim.name, (int)tAnim.values);
                break;
            case VariableType.TRIGGER:
            default:
                animator.SetTrigger(tAnim.name);
                break;
        }



        //		string[] msgSplits = registerString.Split(delimiterPeriod);
        //
        //		switch (msgSplits[0])
        //		{
        //			case "bool":
        //				animator.SetBool(msgSplits[1], (msgSplits[2].ToLower() == "true" ? true : false));
        //				break;			
        //			case "float":
        //				floatList.Add(msgSplits[1], Convert.ToSingle(msgSplits[2].ToLower()));
        //				break;
        //			case "int":
        //				intList.Add(msgSplits[1], Convert.ToInt32(msgSplits[2].ToLower()));
        //				break;
        //			default:
        //				triggerList.Add(msgSplits[1], msgSplits[1]);
        //				break;
        //		}


    }

    public void Play(string animationName)
    {
        animator.Play(animationName);
        //animator.SetBool(msgSplits[1],(msgSplits[2].ToLower()=="true"?true:false));
    }

    public void Stop()
    {
        //animator.Stop();
    }

}

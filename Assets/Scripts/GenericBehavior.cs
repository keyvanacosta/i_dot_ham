using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

namespace MuninuM
{
    [System.Serializable]
    public class GenericForces
    {
        public Vector3 force;
        public Vector3 impulse;
        public Vector3 acceleration;
        public Vector3 velocityChange;

        public GenericForces()
        {
            force = Vector3.zero;
            impulse = Vector3.zero;
            acceleration = Vector3.zero;
            velocityChange = Vector3.zero;
        }

        public void forceZero()
        {
            force = Vector3.zero;
        }

        public void forceSet(Vector3 t)
        {
            force = t;
        }

        public void impulseZero()
        {
            impulse = Vector3.zero;
        }

        public void impulseSet(Vector3 t)
        {
            impulse = t;
        }

        public void accelerationZero()
        {
            acceleration = Vector3.zero;
        }

        public void accelerationSet(Vector3 t)
        {
            acceleration = t;
        }

        public void velocityChangeZero()
        {
            velocityChange = Vector3.zero;
        }

        public void velocityChangeSet(Vector3 t)
        {
            velocityChange = t;
        }

        public void allZero()
        {
            force = Vector3.zero;
            impulse = Vector3.zero;
            acceleration = Vector3.zero;
            velocityChange = Vector3.zero;
        }
    }

    //	GenericBehavior Version "Yun6565"
    //	Created by Keyvan Acosta
    //	keyvan@muninum.com;
    //	Thanks go to the following friends:
    //	Juan Sanchez, Lee Wood, Brian Stabile


    [SelectionBase]
    public partial class GenericBehavior : MonoBehaviour
    {

        //        static public bool GlobalStopGBStatic = false;
        //        public bool globalStopGB = false;


        //
        //        public bool GlobalStopGB
        //        {
        //            //get { return globalStopGB; }
        //            get { return GlobalStopGBStatic; }
        //            set
        //            {
        //                //globalStopGB = value;
        //                //stopF();
        //                GlobalStopGBStatic = value;
        //
        //            }
        //        }

        //        static void stopF()
        //        {
        //            GlobalStopGBStatic = globalStopGB;
        //        }




        private GameObject objectOverride;

        GenericForces gfForce;
        GenericForces gfRForce;
        GenericForces gfTorque;
        GenericForces gfRTorque;

        [Tooltip("This is a scratch pad for your behavior. Keep these in your project notes.")]
        public string behaviorNotes = "";

        static Vector3 globalGravity = Physics.gravity;

        [SerializeField]
        Vector3 realGravity = Vector3.zero;

        //How much does it attract to/by other objects
        public float magneticForce = 0;
        //Is it a magnet with Poles, or does it contain ferromagnetic properties; i.e. metals
        public float electroMagneticProperty = 0;
        //This is a possible candidate to evaluate how to rotate/orient this object if it is a magnet by simulating pole flipping/rotation
        private Quaternion magneticQuaternion = Quaternion.identity;
        //private Quaternion storedQuaternion = Quaternion.identity; //Considered Deprecated;

        [Tooltip("This is the list of events that sum up to an objects behavior.")]
        public List<GameEvent> eventList;
        [Tooltip("This is the list of events that sum up to an objects behavior.")]
        public List<GameEvent> systemEventList;

        private IEnumerator coroutine;
        private bool continueBool;

        /// <summary>
        /// The object access array used to check for collisions and store prefabs to instantiate
        /// Made public for ease of access due to create() and Instantiate(gameFunction)
        /// </summary>
        [Tooltip("The object access array used to check for collisions and store prefabs to instantiate.")]
        public GameObject[] objectAccess;

        [SerializeField]
        private bool objectPause = false;
        [Tooltip("Expand the Privates View.")]
        [SerializeField]
        public bool expandPrivates = true;
        [Tooltip("Set to true to guarantee that this object isn't reset when switching scenes.")]
        [SerializeField]
        private bool dontDestroy = false;
        [Tooltip("Set to true to apply gravitational PULL, unlike a Rigidbody's Gravity Force. Effect will be dependant on this and other objects' masses and results may be unnoticeable.")]
        [SerializeField]
        private bool useRealGravity = false;
        [Tooltip("Set to true to apply a fake gravitational VECTOR, like a Rigidbody's Gravity Force. Effect is regardless of mass and appears as if it were a large vector over a flat plane that goes on forever.")]
        [SerializeField]
        private bool useFakeGravity = false;
        [Tooltip("Used for sending information to and from the event's amount/pastamount.")]
        [SerializeField]
        private float valueStorage = 0.0f;
        [Tooltip("Used for sending information to and from the event's amount/pastamount.")]
        [SerializeField]
        private string stringStorage = string.Empty;
        [Tooltip("Used for General manipulation/addition quasi\"force\" that is applied to this object.")]
        [SerializeField]
        public Vector3 vectorModifier;
        [Tooltip("The position offset used for reseting the game.")]
        [SerializeField]
        public Vector3 storedPosition = Vector3.zero;
        [Tooltip("The Rotation vector used to store between the fixed Update calls.")]
        [SerializeField]
        public Vector3 storedRotation = Vector3.zero;
        [SerializeField]
        private Vector3 storedRelativeRotation = Vector3.zero;



        [Tooltip("The Velocity vector used to store between the fixed Update calls.")]
        [SerializeField]
        private Vector3 storedForces = Vector3.zero;
        private Vector3 storedVector = Vector3.zero;

        /// <summary>
        /// Hidden from inspector for internal storage of other vectors
        /// </summary>
        public Vector3 upVector;
        public Vector3 forwardVector;
        public Vector3 rightVector;
        private Vector3 currentVelocity = Vector3.zero;

        /// <summary>
        /// These are colors that can be changed by calling the mainColor message in the msgFilter field
        /// </summary>
        [SerializeField]
        public Color mainColor = Color.white;
        [SerializeField]
        public Color otherColor = Color.black;
        private Color originalColor = Color.black;

        float timeStamp = 0;

        /// <summary>
        /// The current event that this entire class sends to other functions.
        /// It's used because the invoke method doesn't send parameters, 
        /// thus the event is used as a message/parameter container
        /// </summary>
        private GameEvent currentEvent;

        //private List<string> collisionList;aa
        public List<GameObject> createdObjects;
        //private Dictionary<string, Action> customEventList;
        public Dictionary<string, Action> customEventList;
        public Dictionary<string, GameEvent> messageList;
        public Dictionary<string, int> objectAccessList;
        //private Dictionary<string, ForceMode> forceDictionary;
        //private Dictionary<string, GameEvent> eventListCall;

        private string msgString = string.Empty;
        private float maxMagnitude = -1;
        private float maxMagnitudeAng = -1;
        private float minMagnitude = -1;
        private float minMagnitudeAng = -1;

        /// <summary>
        /// This is a static value shared across allGameEvents.
        /// This should control the speediness of the game.
        /// It should not be set to 0
        /// Functions that access it: timer(),
        /// </summary>
        private static float globalTimeScale = 1;
        private static int globalID = 0;
        private string myID = string.Empty;
        public float timeCounter = 0;
        public bool canInteract = true;

        //	Add friction
        //	Add gravity bolean
        //	Add lob amount
        public float mass = 1;

        public float forceParticle = 1;
        private int collided = 0;
        public GameObject collidedGO;

        public float[,] indexFloatArray;
        public string[,] indexStringArray;

        public bool indexFloatSet = false;
        public bool indexStringSet = false;

        public bool hasRigidbody = false;
        public bool hasCollider = false;
        public bool hasRenderer = false;

        Rigidbody storedRB = null;

        public void addGameEvent(
            bool _enabled,
            string _customName,
            InteractionType _inputType,
            KeyCode _key,
            string _inputString,
            string _letters,
            float _amount,
            float _oamount,
            Vector3 _dir,
            GameFunctions _action,
            PlayerFunctions _pAction,
            ObjectRelation _oRelation,
            ObjectNameType _oNameType,
            string _group,
            ObjectForce _force,
            bool _showEvent,
            string _word,
            string _msgFilter,
            string _objFilter,
            bool _alive)
        {

            //		eventName		= "_BLANKNAME";//_customName;
            //		key 			= _key;
            //		amount 			= _amount;
            //		vectorStorage 	= _dir;
            //		gameAction 		= _action;
            //		playerAction 	= _pAction;
            //		objFilter 		= _objFilter;
            //		word 			= _word;
            //		relation 		= _oRelation;
            //		filterType 		= _oNameType;
            //		on 				= _enabled;
            //		root 			= _root;
            //		force 			= _force;
            //		letters 			= _letters;
            //		alive 			= _alive;
            //		showEvent 		= _showEvent;
            //		otherAmount 	= _amount;		
            GameEvent ge = new GameEvent(
                               _enabled,
                               _customName,
                               _inputType,
                               _key,
                               _inputString,
                               _letters,
                               _amount,
                               _oamount,
                               _dir,
                               _action,
                               _pAction,
                               _oRelation,
                               _oNameType,
                               _group,
                               _force,
                               _showEvent,
                               _word,
                               _msgFilter,
                               _objFilter,
                               _alive
                           );

            eventList.Add(ge);

        }



        void Awake()
        {
            //GlobalStopGB = globalStopGB;
            //            if (globalStopGB == true)
            //            {
            //                Debug.Log("GENERIC BEHAVIOR GLOBALLY STOPPED");
            //                GlobalStopGBStatic = globalStopGB;
            //            }
            //
            //            if (GlobalStopGBStatic == false)
            //            {


            globalID++;
            myID = gameObject.name;
            continueBool = true;

            if (!GetComponent<GenericBehavior>().isActiveAndEnabled)
            {
                return;
            }//!GetComponent<GenericBehavior>().isActiveAndEnabled

            gfForce = new GenericForces();
            gfRForce = new GenericForces();
            gfTorque = new GenericForces();
            gfRTorque = new GenericForces();

            if (dontDestroy == true)
            {
                DontDestroyOnLoad(gameObject.transform);
            }

            hasRigidbody = (gameObject.GetComponent<Rigidbody>() != null ? true : false);
            if (hasRigidbody == true)
            {
                storedRB = gameObject.GetComponent<Rigidbody>();
                if (storedRB.useGravity)
                {
                    Debug.Log("Universal Gravity will be applied to " + gameObject.name + " although the RB component will show it's turned off.");
                    globalGravity = Physics.gravity;
                    useFakeGravity = true;
                    storedRB.useGravity = false;
                }
                else
                {
                    useFakeGravity = false;
                }
                mass = storedRB.mass;

            }

            hasCollider = (gameObject.GetComponent<Collider>() != null ? true : false);
            hasRenderer = (gameObject.GetComponent<Renderer>() != null ? true : false);

            customEventList = new Dictionary<string, Action>();
            objectAccessList = new Dictionary<string, int>();
            //forceDictionary = new Dictionary<string, ForceMode>();
            messageList = new Dictionary<string, GameEvent>();
            //eventListCall = new Dictionary<string, GameEvent>();
            //collisionList = new List<string> ();
            createdObjects = new List<GameObject>();
            currentEvent = new GameEvent();

            storedPosition = gameObject.transform.position;

            upVector = gameObject.transform.up;
            forwardVector = gameObject.transform.forward;
            rightVector = gameObject.transform.right;

            upVector = Vector3.up;
            forwardVector = Vector3.forward;
            rightVector = Vector3.right;

            if (gameObject.GetComponent<Renderer>())
                originalColor = gameObject.GetComponent<Renderer>().material.color;

            foreach (GameEvent kList in eventList)
            {
                //START VALIDATING AND BEEEEE CAREFUL

                messageList.Add(kList.eventName, kList);
                currentEvent = kList;
                switch (kList.playerAction)
                {
                    case PlayerFunctions.AWAKE:
                    case PlayerFunctions.COLLISIONENTER:
                    case PlayerFunctions.COLLISIONEXIT:
                    case PlayerFunctions.COLLISIONSTAY:
                    case PlayerFunctions.INPUTDOWN:
                    case PlayerFunctions.INPUTPRESS:
                    case PlayerFunctions.INPUTANY:
                    case PlayerFunctions.INPUTUP:
                    case PlayerFunctions.MESSAGE_EVENT:
                    case PlayerFunctions.START:
                    case PlayerFunctions.SYSTEM:
                    case PlayerFunctions.TRIGGEREXIT:
                    case PlayerFunctions.TRIGGERENTER:
                    case PlayerFunctions.TRIGGERSTAY:
                    case PlayerFunctions.UPDATE:
                    case PlayerFunctions.UPDATEFIXED:
                    case PlayerFunctions.UPDATELATE:
                        switch (kList.gameAction)
                        {
                            case GameFunctions._NO_OP:
                                customEventList.Add(kList.eventName, nothing);
                                break;
                            case GameFunctions.BEHAVIOR:
                                customEventList.Add(kList.eventName, behavior);
                                break;
                            case GameFunctions.CALL_EVENT:
                                customEventList.Add(kList.eventName, call);
                                break;
                            case GameFunctions.COMPARE_DATA:
                                customEventList.Add(kList.eventName, compareMachine);
                                break;
                            case GameFunctions.COMPUTE:
                                customEventList.Add(kList.eventName, compute);
                                break;
                            case GameFunctions.DESTROY:
                                customEventList.Add(kList.eventName, destroy);
                                break;
                            case GameFunctions.GET_MOVE_DATA:
                                customEventList.Add(kList.eventName, get);
                                break;
                            case GameFunctions.IMITATE:
                                customEventList.Add(kList.eventName, imitate);
                                break;
                            case GameFunctions.INFO_IO:
                                customEventList.Add(kList.eventName, info_index);
                                break;
                            case GameFunctions.INSTANTIATE:
                                customEventList.Add(kList.eventName, create);
                                break;
                            case GameFunctions.LOAD_SCENE:
                                customEventList.Add(kList.eventName, load);
                                break;
                            case GameFunctions.MODIFY_VALUES:
                                customEventList.Add(kList.eventName, modify);
                                break;
                            case GameFunctions.MOTION:
                                customEventList.Add(kList.eventName, motion);
                                break;
                            case GameFunctions.TOGGLE_EVENT:
                                customEventList.Add(kList.eventName, toggle);
                                break;
                            case GameFunctions.PAUSE:
                                customEventList.Add(kList.eventName, pause);
                                break;
                            case GameFunctions.TIMER:
                                customEventList.Add(kList.eventName, timer);
                                break;
                            case GameFunctions.PLAY:
                                customEventList.Add(kList.eventName, playBehavior);
                                break;
                            case GameFunctions.SEND_MSG:
                                customEventList.Add(kList.eventName, send);
                                break;
                            case GameFunctions.REGISTER:
                                customEventList.Add(kList.eventName, register);
                                break;
                            case GameFunctions.RANDOMIZE:
                                customEventList.Add(kList.eventName, randomize);
                                break;
                            case GameFunctions.IF_EVENT_ON:
                                customEventList.Add(kList.eventName, if_event_on);
                                break;
                            case GameFunctions.SET:
                                customEventList.Add(kList.eventName, set);
                                break;
                        }
                        break;


                    default:
                        //Debug.Log ("DEFAULT");
                        break;
                }

            }

            foreach (GameEvent kList in eventList)
            {
                currentEvent = kList;
                switch (kList.playerAction)
                {
                    case PlayerFunctions.AWAKE:
                        if (kList.alive)
                        {
                            customEventList[kList.eventName].Invoke();
                        }
                        break;
                }
            }


            //}//GlobalGBSTOPSTATIC == TRUE
        }

        public void eventOnToggle(string _str)
        {
            //customEventList[_str].;

            GameEvent tEvent = null;

            string test = _str;
            test = _str.Replace("#", "");
            tEvent = messageList[test];
            if (tEvent != null)
            {
                tEvent.alive = true;
            }//return void;

            return;

        }

        //public void callEvent(string _str)
        //{
        //    GameEvent tEvent = null;

        //    string test = _str;
        //    test = _str.Replace("#", "");
        //    tEvent = messageList[test];
        //    print(test);


        //    if (tEvent != null)
        //    {
        //        //currentEvent.on = true;
        //        tEvent.alive = true;
        //        StartCoroutine("decodeMSGS", tEvent.msgFilter);
        //        //currentEvent.on = false;
        //    }//return void;

        //}


        public void eventOffToggle(string _str)
        {
            //customEventList[_str].;

            GameEvent tEvent = null;

            string test = _str;
            test = _str.Replace("#", "");
            tEvent = messageList[test];
            if (tEvent != null)
            {
                tEvent.alive = false;
            }//return void;

            return;

        }

        public void eventToggle(string _str)
        {
            //customEventList[_str].;

            GameEvent tEvent = null;

            string test = _str;
            test = _str.Replace("#", "");
            tEvent = messageList[test];
            if (tEvent != null)
            {
                tEvent.alive = !tEvent.alive;
            }//return void;

            return;

        }

        public void readFileIntoArray(string fileName)
        {

            char delimiterLine = '\n';
            char delimiterComma = ',';
            char delimiterDash = ';';
            //			char delimiterSemmiColon = ';';
            //		TextAsset textf = Resources.Load(fileName) as TextAsset;
            //		string[] infoCategories = textf.text.Split(delimiterLine);

            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamReader sr = new StreamReader(fs);
            string tStr = sr.ReadToEnd();
            fs.Close();
            sr.Close();
            string[] infoCategories = tStr.Split(delimiterLine);
            string[] arraySize = infoCategories[1].Split(delimiterDash);
            string[] stringsArray = infoCategories[2].Split(delimiterComma);

            int x, y;
            float tf;

            switch (infoCategories[0])
            {
                case "float":
                    x = Int32.Parse(arraySize[0]);
                    y = Int32.Parse(arraySize[1]);
                    indexFloatArray = new float[x, y];

                    for (int i = 0; i < x; i++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            tf = (float)Convert.ToSingle(stringsArray[i * (x) + j]);
                            indexFloatMap(i, j, tf, true);
                        }
                    }

                    break;
                default:
                    x = Int32.Parse(arraySize[0]);
                    y = Int32.Parse(arraySize[1]);
                    indexFloatArray = new float[x, y];
                    for (int i = 0; i < x; i++)
                    {
                        for (int j = i; j < y; j++)
                        {
                            indexStringMap(i * x, j, stringsArray[i * x + j], true);
                        }
                    }

                    break;
            }

        }

        public void writeArrayIntoFile(string fileName, string whichIndex = "float")
        {

            char delimiterLine = '\n';
            char delimiterComma = ',';
            char delimiterDash = '-';
            //			char delimiterSemmiColon = ';';

            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamReader sr = new StreamReader(fs);
            string tStr = sr.ReadToEnd();

            string[] infoCategories = tStr.Split(delimiterLine);//textf.text.Split(delimiterLine);
            string[] arraySize = infoCategories[1].Split(delimiterDash);
            string[] stringsArray = infoCategories[2].Split(delimiterComma);
            int x, y;

            switch (whichIndex)
            {
                case "float":
                    x = Int32.Parse(arraySize[0]);
                    y = Int32.Parse(arraySize[1]);
                    indexFloatArray = new float[x, y];
                    for (int i = 0; i < x; i++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            indexFloatMap(i * x, j, Convert.ToSingle(stringsArray[i * x + j]), true);
                        }
                    }
                    break;
                default:
                    x = Int32.Parse(arraySize[0]);
                    y = Int32.Parse(arraySize[1]);
                    indexFloatArray = new float[x, y];
                    for (int i = 0; i < x; i++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            indexStringMap(i * x, j, stringsArray[i * x + j], true);
                        }
                    }
                    break;
            }

        }

        public float accessEventFloats(string whichEvent, string whichMember, float value = 0, bool setGet = false)
        {
            GameEvent tEvent = null;

            string test = whichEvent;
            test = whichEvent.Replace("#", "");
            tEvent = messageList[test];
            if (tEvent == null)
                return value;
            float tfloat = 0;
            /// GET ANY OF THESE VARIABLES RETURNED
            if (!setGet)
            {

                switch (whichMember)
                {
                    case "amount":
                        tfloat = tEvent.amount;
                        break;
                    case "otheramount":
                        tfloat = tEvent.otherAmount;
                        break;
                    case "vectorstoragex":  //apply valuestorage to other variable;
                        tfloat = tEvent.vectorStorage.x;
                        break;
                    case "vectorstoragey":
                        tfloat = tEvent.vectorStorage.y;
                        break;
                    case "vectorstoragez":
                        tfloat = tEvent.vectorStorage.z;
                        break;
                }
            }
            else
            { //Set
                tfloat = value;
                switch (whichMember)
                {
                    case "amount":
                        tEvent.amount = tfloat;
                        break;
                    case "otheramount":
                        tEvent.otherAmount = tfloat;
                        break;
                    case "vectorstoragex":  //apply valuestorage to other variable;
                        tEvent.vectorStorage.x = tfloat;
                        break;
                    case "vectorstoragey":
                        tEvent.vectorStorage.y = tfloat;
                        break;
                    case "vectorstoragez":
                        tEvent.vectorStorage.z = tfloat;
                        break;
                }
            }

            return tfloat;

        }

        public string accessEventStrings(string whichEvent, string whichMember, string value = "", bool setGet = false)
        {
            GameEvent tEvent = null;

            string test = whichEvent;
            test = whichEvent.Replace("#", "");
            tEvent = messageList[test];
            if (tEvent == null)
                return value;
            string tstr = "";
            /// GET ANY OF THESE VARIABLES RETURNED
            if (!setGet)
            {
                switch (whichMember)
                {
                    case "word":
                        tstr = tEvent.word;
                        break;
                    case "string":
                        tstr = tEvent.letters;
                        break;
                    case "name":    //apply valuestorage to other variable;
                        tstr = tEvent.eventName;
                        break;
                    case "msgfilter":   //apply valuestorage to other variable;
                        tstr = tEvent.eventName;
                        break;
                    case "objfilter":   //apply valuestorage to other variable;
                        tstr = tEvent.eventName;
                        break;
                }
            }
            else
            { //Set
                tstr = value;
                switch (whichMember)
                {
                    case "word":
                        tEvent.word = tstr;
                        break;
                    case "string":
                        tEvent.letters = tstr;
                        break;
                    case "name":    //apply valuestorage to other variable;
                        tEvent.eventName = tstr;
                        break;
                    case "msgfilter":   //apply valuestorage to other variable;
                        tEvent.msgFilter = tstr;
                        break;
                    case "objfilter":   //apply valuestorage to other variable;
                        tEvent.objFilter = tstr;
                        break;

                }
            }

            return tstr;

        }

        public float accessPrivateFloats(string whichMember, float value = 0, bool setGet = false)
        {
            float tfloat = 0;
            /// GET ANY OF THESE VARIABLES RETURNED
            if (!setGet)
            {

                switch (whichMember)
                {
                    case "mass":
                        tfloat = mass;
                        break;
                    case "force":
                        tfloat = forceParticle;
                        break;
                    case "valuestorage":    //apply valuestorage to other variable;
                        tfloat = valueStorage;
                        break;
                    case "maxvelmagnitude":
                        tfloat = maxMagnitude;
                        break;
                    case "maxangmagnitude":
                        tfloat = maxMagnitudeAng;
                        break;
                    case "minvelmagnitude":
                        tfloat = minMagnitude;
                        break;
                    case "minangmagnitude":
                        tfloat = minMagnitudeAng;
                        break;
                    case "globaltimescale":
                        tfloat = globalTimeScale;
                        break;
                    case "colorR":
                        tfloat = mainColor.r;
                        break;
                    case "colorG":
                        tfloat = mainColor.g;
                        break;
                    case "colorB":
                        tfloat = mainColor.b;
                        break;
                    case "storedvelocityx":
                        tfloat = (hasRigidbody ? storedRB.velocity.x : storedForces.x);
                        break;
                    case "storedvelocityy":
                        tfloat = (hasRigidbody ? storedRB.velocity.y : storedForces.y);
                        break;
                    case "storedvelocityz":
                        tfloat = (hasRigidbody ? storedRB.velocity.y : storedForces.z);
                        break;
                }
            }
            else
            { //Set
                tfloat = value;
                switch (whichMember)
                {
                    case "mass":
                        mass = tfloat;
                        if (hasRigidbody)
                        {
                            storedRB.mass = tfloat;
                        }
                        break;
                    case "force":
                        forceParticle = tfloat;
                        break;
                    case "valuestorage":    //apply valuestorage to other variable;
                        valueStorage = tfloat;
                        break;
                    case "maxangmagnitude":
                        maxMagnitudeAng = tfloat;
                        break;
                    case "maxvelmagnitude":
                        maxMagnitude = tfloat;
                        break;
                    case "minangmagnitude":
                        minMagnitudeAng = tfloat;
                        break;
                    case "minvelmagnitude":
                        minMagnitude = tfloat;
                        break;
                    case "globaltimescale":
                        globalTimeScale = tfloat;
                        break;
                    case "colorR":
                        mainColor.r = tfloat;
                        break;
                    case "colorG":
                        mainColor.g = tfloat;
                        break;
                    case "colorB":
                        mainColor.b = tfloat;
                        break;
                }
            }
            return tfloat;
        }

        public string accessPrivateStrings(string whichMember, string value = "", bool setGet = false)
        {
            string tStr = string.Empty;
            /// GET ANY OF THESE VARIABLES RETURNED
            if (!setGet)
            {

                switch (whichMember)
                {
                    case "stringstorage":
                        tStr = stringStorage;
                        break;
                    case "behaviorNotes":
                        tStr = behaviorNotes;
                        break;
                }
            }
            else
            { //Set
                tStr = value;
                switch (whichMember)
                {
                    case "stringstorage":
                        stringStorage = tStr;
                        break;
                    case "behaviorNotes":
                        behaviorNotes = tStr;
                        break;
                }
            }
            return tStr;
        }

        void behavior()
        {

            if (!currentEvent.alive)
                return;

            GameEvent tEvent = currentEvent;

            Vector3 tVec = tEvent.vectorStorage;

            Vector3 tVecRB = Vector3.zero;
            tVecRB = currentVelocity;
            tVecRB.x *= tVec.x;
            tVecRB.y *= tVec.y;
            tVecRB.z *= tVec.z;

            tVecRB.x *= tEvent.amount;
            tVecRB.y *= tEvent.amount;
            tVecRB.z *= tEvent.amount;

            GameObject other;// = collidedGO;
            Vector3 tOtherVec;// = tVec;

            float distance = 0;
            float magnitude = 0;

            Vector3 tV;// = tOtherVec - tVec;//Vector3.MoveTowards(tVec, tOtherVec, 5);
            Vector3 tO;// = currentEvent.vectorStorage;


            string[] msgSplits = parseString(currentEvent.msgFilter);

            for (int i = 0; i < msgSplits.Length; i++)
            {

                switch (msgSplits[i])
                {
                    //switch (currentEvent.msgFilter)
                    //{

                    case "custom_behavior":
                        break;
                    case "reverse":

                        currentVelocity = Vector3.Reflect(currentVelocity, Vector3.back);

                        tVecRB = currentVelocity;
                        tVecRB.x *= tVec.x;
                        tVecRB.y *= tVec.y;
                        tVecRB.z *= tVec.z;

                        tVecRB.x *= tEvent.amount;
                        tVecRB.y *= tEvent.amount;
                        tVecRB.z *= tEvent.amount;

                        gfForce.velocityChange += tVecRB;
                        break;

                    case "follow_translate":
                        //                    float step = speed * Time.deltaTime;
                        //                    transform.position = Vector3.MoveTowards(transform.position, target.position, -step);
                        if (collided == 1)
                        {
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = collidedGO.transform.root.gameObject;
                                //Debug.Log("parent" + collidedGO.name);
                            }
                        }
                        else
                        {
                            if (currentEvent.objFilter.Length > 0)
                            {
                                collidedGO = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject;
                                //Debug.Log("object " + collidedGO.name);
                            }
                        }

                        //other = collidedGO;
                        //tOtherVec = tVec;

                        tOtherVec.x = collidedGO.transform.position.x;
                        tOtherVec.y = collidedGO.transform.position.y;
                        tOtherVec.z = collidedGO.transform.position.z;
                        tOtherVec.x *= currentEvent.vectorStorage.x;
                        tOtherVec.y *= currentEvent.vectorStorage.y;
                        tOtherVec.z *= currentEvent.vectorStorage.z;
                        tVec.x = gameObject.transform.position.x;
                        tVec.y = gameObject.transform.position.y;
                        tVec.z = gameObject.transform.position.z;
                        tVec.x *= currentEvent.vectorStorage.x;
                        tVec.y *= currentEvent.vectorStorage.y;
                        tVec.z *= currentEvent.vectorStorage.z;

                        tV = tOtherVec - tVec;

                        distance = Vector3.Distance(tVec, tOtherVec);
                        magnitude = Mathf.Abs(currentEvent.otherAmount);

                        if (currentEvent.otherAmount >= 0 && Mathf.Sign(currentEvent.amount) * magnitude > Mathf.Sign(currentEvent.amount) * distance)
                        {
                            //                            Debug.Log("distance reached:" + gameObject.name);
                            continue;
                        }

                        tOtherVec = Vector3.MoveTowards(tOtherVec, tVec, Mathf.Sign(currentEvent.amount) * magnitude);
                        //                        Debug.Log("beforeN()" + tOtherVec);
                        tOtherVec.x *= currentEvent.vectorStorage.x;
                        tOtherVec.y *= currentEvent.vectorStorage.y;
                        tOtherVec.z *= currentEvent.vectorStorage.z;

                        tOtherVec.Normalize();
                        //                        Debug.Log("N()" + tOtherVec + "tV:" + tV);

                        tV.Normalize();
                        tO = currentEvent.vectorStorage;
                        tO.x *= currentEvent.amount;
                        tO.y *= currentEvent.amount;
                        tO.z *= currentEvent.amount;

                        tV.Scale(tO);
                        //                        Debug.Log("tVN():" + tV);

                        transform.Translate(tV);

                        break;
                    case "follow":
                    case "follow_force":

                        if (collided == 1)
                        {
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = collidedGO.transform.root.gameObject;
                            }
                        }
                        else
                        {
                            if (currentEvent.objFilter.Length > 0)
                            {
                                collidedGO = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject;
                            }
                        }

                        tOtherVec.x = collidedGO.transform.position.x;
                        tOtherVec.y = collidedGO.transform.position.y;
                        tOtherVec.z = collidedGO.transform.position.z;
                        tOtherVec.x *= currentEvent.vectorStorage.x;
                        tOtherVec.y *= currentEvent.vectorStorage.y;
                        tOtherVec.z *= currentEvent.vectorStorage.z;
                        tVec.x = gameObject.transform.position.x;
                        tVec.y = gameObject.transform.position.y;
                        tVec.z = gameObject.transform.position.z;
                        tVec.x *= currentEvent.vectorStorage.x;
                        tVec.y *= currentEvent.vectorStorage.y;
                        tVec.z *= currentEvent.vectorStorage.z;

                        tV = tOtherVec - tVec;

                        magnitude = Mathf.Abs(currentEvent.otherAmount);
                        distance = Vector3.Distance(tVec, tOtherVec);
                        if (currentEvent.otherAmount >= 0 && Mathf.Sign(currentEvent.amount) * magnitude > Mathf.Sign(currentEvent.amount) * distance)
                        {
                            storedRB.Sleep();
                            continue;
                        }

                        //                        tOtherVec = Vector3.MoveTowards(tOtherVec, tVec, Mathf.Sign(currentEvent.amount) * magnitude);
                        //                        tOtherVec.x *= currentEvent.vectorStorage.x;
                        //                        tOtherVec.y *= currentEvent.vectorStorage.y;
                        //                        tOtherVec.z *= currentEvent.vectorStorage.z;
                        //
                        // Debug.Log("BF TV:" + tV + " tOVec" + tOtherVec);

                        //tOtherVec.Normalize();
                        tV.Normalize();
                        //Debug.Log("TV:" + tV + " tOVec" + tOtherVec);
                        //tV.Scale(tOtherVec);

                        tO = currentEvent.vectorStorage;
                        tO.x *= currentEvent.amount;
                        tO.y *= currentEvent.amount;
                        tO.z *= currentEvent.amount;

                        tV.Scale(Mathf.Sign(currentEvent.amount) * tO);
                        //  Debug.Log("After TV:" + tV + " tOVec" + tOtherVec);
                        setMotion(tV, "", "", currentEvent.force, currentEvent.word, currentEvent.amount, currentEvent.otherAmount);


                        break;
                    //                    case "gravitational":
                    //                        
                    //                        string[] objSplits = parseString(currentEvent.objFilter);
                    //                        Vector3 centralVector = Vector3.zero;
                    //                        for (int j = 0; j < objSplits.Length; j++)
                    //                        {
                    //                            
                    //                        }
                    //
                    //                        break;
                    case "magnetic":

                        //                        float polarSign = Mathf.Sign(currentEvent.amount);
                        //
                        //                        GenericBehavior tGB = collidedGO.GetComponentInParent<GenericBehavior>();
                        //
                        //                        string[] objSplits = parseString(currentEvent.objFilter);
                        //                        Vector3 centralVector = Vector3.zero;
                        //
                        //                        for (int j = 0; j < objSplits.Length; j++)
                        //                        {
                        //
                        //                        }
                        //
                        //                        if (collided == 1)
                        //                        {
                        //                            if (currentEvent.relation == ObjectRelation.PARENT)
                        //                            {
                        //                                collidedGO = collidedGO.transform.root.gameObject;
                        //                            }
                        //                        }
                        //                        else
                        //                        {
                        //                            if (currentEvent.objFilter.Length > 0)
                        //                            {
                        //                                collidedGO = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject;
                        //                            }
                        //                        }                            
                        //
                        //                        tOtherVec.x = collidedGO.transform.position.x;
                        //                        tOtherVec.y = collidedGO.transform.position.y;
                        //                        tOtherVec.z = collidedGO.transform.position.z;
                        //                        tOtherVec.x *= currentEvent.vectorStorage.x;
                        //                        tOtherVec.y *= currentEvent.vectorStorage.y;
                        //                        tOtherVec.z *= currentEvent.vectorStorage.z;
                        //                        tVec.x = transform.position.x;
                        //                        tVec.y = transform.position.y;
                        //                        tVec.z = transform.position.z;
                        //                        tVec.x *= currentEvent.vectorStorage.x;
                        //                        tVec.y *= currentEvent.vectorStorage.y;
                        //                        tVec.z *= currentEvent.vectorStorage.z;
                        //
                        //                        tV = tOtherVec - tVec;
                        //
                        //                        magnitude = Mathf.Abs(currentEvent.otherAmount);
                        //                        distance = Vector3.Distance(tVec, tOtherVec);
                        //                        if (currentEvent.otherAmount >= 0 && Mathf.Sign(currentEvent.amount) * magnitude > Mathf.Sign(currentEvent.amount) * distance)
                        //                        {
                        //                            storedRB.Sleep();
                        //                            continue;
                        //                        }
                        //
                        //                        tOtherVec = Vector3.MoveTowards(tVec, tOtherVec, Mathf.Sign(currentEvent.amount) * magnitude);//currentEvent.amount);
                        //                        tOtherVec.x *= currentEvent.vectorStorage.x;
                        //                        tOtherVec.y *= currentEvent.vectorStorage.y;
                        //                        tOtherVec.z *= currentEvent.vectorStorage.z;
                        //
                        //                        tOtherVec.Normalize();
                        //                        tV.Scale(tOtherVec);
                        //                        tV.Normalize();
                        //                        tO = currentEvent.vectorStorage;
                        //                        tO.x *= Mathf.Sign(currentEvent.amount) * currentEvent.amount;
                        //                        tO.y *= Mathf.Sign(currentEvent.amount) * currentEvent.amount;
                        //                        tO.z *= Mathf.Sign(currentEvent.amount) * currentEvent.amount;
                        //
                        //                        tV.Scale(tO);
                        //
                        //                        setMotion(tV, "", "", currentEvent.force, currentEvent.word, currentEvent.amount, currentEvent.otherAmount);
                        //
                        break;
                    case "inertia":
                    case "lookat":
                        lookAt(currentEvent);
                        break;

                    case "hover":

                        break;
                    case "bounce":
                        currentVelocity = Vector3.Reflect(currentVelocity, storedVector);

                        tVecRB = currentVelocity;
                        tVecRB.x *= tVec.x;
                        tVecRB.y *= tVec.y;
                        tVecRB.z *= tVec.z;

                        tVecRB.x *= tEvent.amount;
                        tVecRB.y *= tEvent.amount;
                        tVecRB.z *= tEvent.amount;

                        gfForce.velocityChange += tVecRB;
                        break;
                    case "vectorreflect":
                    default:

                        currentVelocity = Vector3.Reflect(currentVelocity, storedVector);

                        tVecRB = currentVelocity;
                        tVecRB.x *= tVec.x;
                        tVecRB.y *= tVec.y;
                        tVecRB.z *= tVec.z;

                        tVecRB.x *= tEvent.amount;
                        tVecRB.y *= tEvent.amount;
                        tVecRB.z *= tEvent.amount;

                        //gfForce.velocityChange += tVecRB;
                        storedRB.velocity = tVecRB;
                        break;
                }
            }
            currentEvent = tEvent;
        }

        /// <summary>
        /// Todo:
        /// 	Hook up to call an external gameobjects call
        /// </summary>
        public void call()
        {
            GameEvent tEvent = currentEvent;
            currentEvent.on = true;

            StartCoroutine("decodeMSGS", tEvent.msgFilter);
            currentEvent.on = false;
        }



        public GameEvent callObjectEvent(string eventName, bool run = false, float tdelay = -1.0f)
        {

            currentEvent.on = run;
            //decodeMSGS(eventName);
            StartCoroutine("decodeMSGS", eventName);

            return currentEvent;
        }
        //Future Check = RotationAngle, DistanceTo, CountObjects, ValueStorage, speed, Position, Collision
        void compute()
        {

            if (!currentEvent.alive)
                return;

            string[] msgSplits = parseString(currentEvent.msgFilter);
            string[] objSplits = parseString(currentEvent.objFilter);
            RaycastHit hit;
            for (int i = 0; i < msgSplits.Length; i++)
            {

                switch (msgSplits[i])
                {
                    case "distance":
                        if (currentEvent.objFilter.Length <= 0)
                        {
                            currentEvent.amount = Vector3.Distance(gameObject.transform.position, objectAccess[objectAccessList[currentEvent.objFilter]].gameObject.transform.position);
                        }
                        else
                        {
                            currentEvent.amount = Vector3.Distance(gameObject.transform.position, currentEvent.vectorStorage);
                        }
                        break;
                    case "velmagnitude":
                        if (currentEvent.objFilter.Length <= 0)
                        {
                            currentEvent.amount = Vector3.Magnitude(storedForces);
                        }
                        else
                        {
                            GenericBehavior tGB = (GenericBehavior)objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>();
                            currentEvent.amount = Vector3.Magnitude(tGB.getPrivateVectors("storedForces"));
                        }
                        break;
                    case "orientation":
                        if (currentEvent.objFilter.Length <= 0)
                        {
                            currentEvent.vectorStorage = gameObject.transform.rotation.eulerAngles;
                        }
                        else
                        {
                            currentEvent.vectorStorage = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject.transform.rotation.eulerAngles;
                        }
                        break;
                    case "raycasttouch":

                        if (currentEvent.objFilter.Length <= 0)
                        {
                            currentEvent.vectorStorage = gameObject.transform.rotation.eulerAngles;
                        }
                        else
                        {
                            currentEvent.vectorStorage = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject.transform.rotation.eulerAngles;
                        }

                        currentEvent.on = Physics.Raycast(gameObject.transform.position, forwardVector + currentEvent.vectorStorage, out hit, currentEvent.amount);


                        break;
                    case "valuestorages":
                        if (currentEvent.objFilter.Length <= 0)
                            return;
                        currentEvent.amount = 0;
                        for (int j = 0; j < objSplits.Length; j++)
                        {
                            currentEvent.amount += objectAccess[objectAccessList[objSplits[j]]].gameObject.GetComponent<GenericBehavior>().accessPrivateFloats("valuestorage");
                            ;
                        }
                        break;
                    case "count":
                    default:
                        if (currentEvent.objFilter.Length <= 0)
                            return;
                        currentEvent.amount = GameObject.FindGameObjectsWithTag(currentEvent.objFilter).Length;
                        break;

                }//Switch

            }//For

        }

        void playBehavior()
        {

            if (currentEvent.alive == false)
            {
                return;
            }

            GameEvent tEvent = currentEvent;

            string strLobj = string.Empty;

            char[] delimiterComma = { ',', ':', '>' };
            char[] delimiterMsgObj = { '@' };
            string _str = tEvent.msgFilter;
            string _str2 = tEvent.objFilter;
            if (_str.Length == 0)
            {
                return;
            }

            //	Split the MsgFilter
            string[] msgSplits = _str.Split(delimiterComma);
            //	Split the objFilter
            string[] objSplits = _str2.Split(delimiterMsgObj);

            strLobj = objSplits[0];
            if (objSplits.Length > 1)
            {
                strLobj = objSplits[1];
            }

            //	GameObject go = objectAccess[objectAccessList[strLobj]];
            //	GameObject go = gameObject;
            //GameObject go = objectAccess[objectAccessList[strLobj]];

            GameObject go = gameObject;

            if (currentEvent.objFilter.Length > 0)
            {
                go = objectAccess[objectAccessList[currentEvent.objFilter]];
            }
            switch (currentEvent.word)
            {
                case "Sound":
                    break;
                case "animation:trigger":
                    go.SendMessage("CallAnimation", currentEvent.msgFilter);
                    break;
                case "animation:on":
                    go.SendMessage("CallAnimation", currentEvent.msgFilter);
                    break;
                case "animation:toggle":
                    go.SendMessage("ModifyEntry", currentEvent.msgFilter + ":toggle");
                    break;
                case "animation:amount":
                    go.SendMessage("ModifyEntry", currentEvent.msgFilter + ":" + currentEvent.amount.ToString());
                    break;
                case "animation:otheramount":
                    go.SendMessage("ModifyEntry", currentEvent.msgFilter + ":" + currentEvent.otherAmount.ToString());
                    break;
                default:
                    if (_str2.Length == 0)
                    {
                        //	This is for keeping standard functionality with Classes that have the function "play"
                        go.SendMessage("Play", currentEvent.amount);
                        return;
                    }

                    go.SendMessage("setVolume", tEvent.amount);
                    for (int i = 0; i < msgSplits.Length; i++)
                    {
                        go.SendMessage("Play", msgSplits[i]);
                    }
                    break;
            }

        }

        //Dont send msgs that rely on collision, or anything to do with a particular Object Access List
        //	Only use it to change member's privates: accessPrivates() Set, etc.
        //void send(GameEvent _ge = null)--deprecated
        void send()
        {

            GameEvent _ge = currentEvent;

            string strLobj = string.Empty;

            char[] delimiterComma = { ',' };
            char[] delimiterColon = { ':' };
            char[] delimiterPeriod = { '.' };

            string _str2 = _ge.msgFilter;
            if (_str2.Length == 0)
            {
                Debug.Log("Message Unsent: EMPTY MESSAGE ON MSGFILTER");
                return;
            }

            string[] msgCombos = _str2.Split(delimiterComma);

            for (int i = 0; i < msgCombos.Length; i++)
            {
                string[] msgSplits = msgCombos[i].Split(delimiterColon);
                strLobj = msgSplits[0];
                if (msgSplits.Length > 1)
                {
                    strLobj = msgSplits[1];
                }

                GameObject go = gameObject;

                if (_ge.objFilter.Length > 0)
                {
                    go = objectAccess[objectAccessList[_ge.objFilter]];
                }

                string[] _str = msgSplits[0].Split(delimiterPeriod);

                switch (_str[0])
                {
                    case "debug":
                        switch (_str[1])
                        {
                            case "log":
                                Debug.Log(strLobj);
                                break;
                            case "valuestorage":
                                Debug.Log("valuestorage:" + valueStorage);
                                break;
                            case "stringstorage":
                                Debug.Log("stringstorage:" + stringStorage);
                                break;
                            case "currentvelocity":
                                Debug.Log("currentvelocity:" + currentVelocity);
                                break;
                            case "forward":
                                Debug.Log("forward:" + forwardVector);
                                Debug.Log("Vforward:" + Vector3.forward);
                                break;
                            case "up":
                                Debug.Log("up:" + upVector);
                                break;
                            case "floatindex":
                                if (!indexFloatSet)
                                {
                                    Debug.Log("IndexFloatArray size is undefined/null.");
                                    continue;
                                }
                                _str2 = "";
                                for (int j = 0; j < indexFloatArray.GetLength(0); j++)
                                {
                                    _str2 += "[";
                                    for (int k = 0; k < indexFloatArray.GetLength(1); k++)
                                    {
                                        //Debug.Log("["+j.ToString()+","+k.ToString()+"]:"+indexFloatArray[j,k].ToString() + ",");
                                        _str2 += indexFloatArray[j, k].ToString() + (k == indexFloatArray.GetLength(1) - 1 ? "" : ",");
                                    }
                                    _str2 += "]\n";
                                }
                                Debug.Log(_str2);
                                break;
                            case "stringindex":
                                _str2 = "";
                                for (int j = 0; j < indexStringArray.GetLength(0); j++)
                                {
                                    _str2 += "[";
                                    for (int k = 0; k < indexStringArray.GetLength(1); k++)
                                    {
                                        //Debug.Log("["+j.ToString()+","+k.ToString()+"]:"+indexFloatArray[j,k].ToString() + ",");
                                        _str2 += indexStringArray[j, k].ToString() + (k == indexStringArray.GetLength(1) - 1 ? "" : ",");
                                    }
                                    _str2 += "]\n";
                                }
                                Debug.Log(_str2);
                                break;
                            case "name":
                                Debug.Log("name:" + gameObject.name);
                                break;
                            case "tag":
                                Debug.Log("tag:" + gameObject.tag);
                                break;
                            case "position":
                                Debug.Log("position:" + gameObject.transform.position);
                                break;
                            case "rotation":
                                Debug.Log("rotation:" + gameObject.transform.rotation.eulerAngles);
                                break;
                            case "scale":
                                Debug.Log("scale:" + gameObject.transform.localScale);
                                break;
                        }
                        break;
                    case "message":
                        switch (_str[1])
                        {
                            case "amount":
                                go.SendMessage(currentEvent.word, currentEvent.amount);
                                break;
                            default:
                                if (_str[1].Length > 0)
                                {
                                    go.SendMessage(currentEvent.word, _str[1]);
                                }
                                break;
                        }
                        break;
                }

            }

        }

        void load()
        {
            GameEvent tempEvent = currentEvent;
            if (!tempEvent.alive)
            {
                return;
            }
            Scene thisScene = SceneManager.GetActiveScene();
            string currentScene = thisScene.name;
            Debug.Log("Leaving " + currentScene);

            switch (tempEvent.playerAction)
            {
                case PlayerFunctions.UPDATE:
                case PlayerFunctions.UPDATEFIXED:
                case PlayerFunctions.UPDATELATE:
                case PlayerFunctions.START:
                    switch (currentEvent.msgFilter)
                    {
                        case "combinescene":

                            break;
                        case "animationint":

                            break;
                        case "animationfloat":

                            break;
                        case "animationbool":

                            break;
                        default:
                            break;
                    }
                    break;
                case PlayerFunctions.AWAKE:
                case PlayerFunctions._NOTHING:
                case PlayerFunctions.SYSTEM:
                    ///	DO NOTHING FOR NOW
                    break;
                default:
                    switch (currentEvent.msgFilter)
                    {

                        case "unload":
                            break;
                        case "scene_default":
                        default:
                            if (tempEvent.msgFilter.Length > 0)
                            {
                                if (currentScene == tempEvent.msgFilter)
                                {
                                    Debug.Log("RESETTING SCENE");
                                    SceneManager.LoadScene(currentScene);
                                }
                                else
                                {
                                    Scene tScene = SceneManager.GetSceneByName("ExampleScenes/LivingRoom");//tempEvent.msgFilter);
                                    Debug.Log("name:" + tScene.name + tScene.IsValid().ToString() + tScene.path);
                                    if (tScene.name == null)
                                    {
                                        Debug.Log("Loading " + tempEvent.msgFilter + " SCENE");
                                        SceneManager.LoadScene(tempEvent.msgFilter);
                                    }
                                    else
                                    {
                                        Debug.Log("Scene:\"" + tempEvent.msgFilter + "\" Not found in the Build Settings Scene Menu (shift+(Ctrl/CMD) + B");
                                    }

                                }
                            }
                            else
                            { //load the same scene
                                Debug.Log("No scene requested - RESETTING SCENE");
                                SceneManager.LoadScene(currentScene);
                            }
                            break;
                    }
                    break;
            }
            tempEvent.on = false;
            currentEvent = tempEvent;

        }

        public Vector3 getPrivateVectors(string whichVector)
        {
            Vector3 dummy = Vector3.zero;

            switch (whichVector)
            {
                case "currentVelocity":
                    dummy = currentVelocity;
                    break;
                case "vectorModifier":
                    dummy = vectorModifier;
                    break;
                case "storedVector":
                    dummy = storedVector;
                    break;
                case "storedPosition":
                    dummy = storedPosition;
                    break;
                case "storedRotation":
                    dummy = storedRotation;
                    break;
                case "storedForces":
                    dummy = storedForces;
                    break;
                case "upVector":
                    dummy = upVector;
                    break;
                case "forwardVector":
                    dummy = forwardVector;
                    break;
                case "rightVector":
                    dummy = rightVector;
                    break;
                default:
                    break;
            }

            return dummy;
        }

        //	Only called from Message Send
        public GameEvent setRunTempEvent(GameEvent _te, bool run = false)
        {
            GameEvent tempEvent = currentEvent;

            currentEvent = _te;

            currentEvent.on = true;
            call();
            currentEvent.on = tempEvent.on;

            currentEvent = tempEvent;

            return currentEvent;
        }

        void modify()
        {
            GameEvent tempEvent = currentEvent;
            if (!tempEvent.alive)
            {
                return;
            }
            string _str = tempEvent.msgFilter;
            char[] delimiterComma = { ',', ':', '>' };
            char[] delimiterPeriod = { '.' };
            if (_str.Length == 0)
            {
                return;
            }

            string[] msgSplits = _str.Split(delimiterComma);
            float tfloat = 0;
            GameObject go;
            for (int i = 0; i < msgSplits.Length; i++)
            {
                string[] msgSubSplits = _str.Split(delimiterPeriod);
                go = gameObject;
                if (tempEvent.objFilter.Length > 0)
                {
                    go = objectAccess[objectAccessList[currentEvent.objFilter]];
                }

                switch (msgSplits[i])
                {
                    case "stringstorageletter":

                        _str = go.GetComponent<GenericBehavior>().accessPrivateStrings("stringstorage");
                        switch ((int)tempEvent.otherAmount)
                        {
                            case 0://add
                                if (_str.Length + 1 < 140)
                                {
                                    _str += currentEvent.letters;
                                }
                                break;
                            case 9://Set
                                _str = currentEvent.letters;
                                break;
                        }
                        _str = go.GetComponent<GenericBehavior>().accessPrivateStrings("stringstorage", _str, true);
                        break;
                    case "stringstorageword":

                        _str = go.GetComponent<GenericBehavior>().accessPrivateStrings("stringstorage");

                        switch ((int)tempEvent.otherAmount)
                        {
                            case 0://add
                                if (_str.Length + 1 < 140)
                                {
                                    _str += currentEvent.word;
                                }
                                break;
                            case 9://Set
                                _str = currentEvent.word;
                                break;
                        }
                        _str = go.GetComponent<GenericBehavior>().accessPrivateStrings("stringstorage", _str, true);
                        break;
                    default:

                        if (msgSubSplits[0] == "eventsNumber")
                        {
                            try
                            {
                                tfloat = go.GetComponent<GenericBehavior>().accessEventFloats(msgSubSplits[1], msgSubSplits[2]);
                            }
                            catch (System.Exception e)
                            {
                                Debug.Log("Your eventNumbers code is written in a way that it's causing a bug to occur. Verify all strings." + e.ToString());
                            }
                        }
                        else
                        {
                            tfloat = go.GetComponent<GenericBehavior>().accessPrivateFloats(msgSplits[i]);
                        }

                        //Modifiers available (otherAmount): add, mult, round, abs, pow, sqrt, pingpong, lerp
                        switch ((int)tempEvent.otherAmount)
                        {
                            case 0://add
                                tfloat += tempEvent.amount;
                                break;
                            case 1://multiply
                                tfloat *= tempEvent.amount;
                                break;
                            case 2://round
                                tfloat = Mathf.Round(tfloat);
                                break;
                            case 3://abs
                                tfloat = Mathf.Abs(tfloat);
                                break;
                            case 4://pow
                                tfloat = Mathf.Pow(tfloat, tempEvent.amount);
                                break;
                            case 5://sqrt
                                tfloat = Mathf.Sqrt(tfloat);
                                break;
                            case 6://pingpong
                                tfloat = Mathf.PingPong(tfloat, tempEvent.amount);
                                break;
                            case 7://Lerp
                                tfloat = Mathf.Lerp(tempEvent.vectorStorage.x, tempEvent.vectorStorage.y, Time.deltaTime * tempEvent.amount);
                                break;
                            case 8://Modulus
                                tfloat = (tempEvent.amount == 0 ? 0 : tfloat % Mathf.Abs(Mathf.Round(tempEvent.amount)));
                                break;
                            case 9://Set
                                tfloat = tempEvent.amount;
                                break;

                        }

                        if (msgSubSplits[0] == "eventsNumber")
                        {
                            try
                            {
                                tfloat = go.GetComponent<GenericBehavior>().accessEventFloats(msgSubSplits[1], msgSubSplits[2], tfloat, true);
                            }
                            catch (System.Exception e)
                            {
                                Debug.Log("Your eventNumbers code is written in a way that it's causing a bug to occur. Verify all strings." + e.ToString());
                            }
                        }
                        else
                        {
                            //tfloat = go.GetComponent<GenericBehavior> ().accessPrivateFloats (msgSplits [i]);
                            tfloat = go.GetComponent<GenericBehavior>().accessPrivateFloats(msgSplits[i], tfloat, true);
                        }

                        break;
                }

            }//FOR

            currentEvent = tempEvent;
        }

        public void setValueStorage(float _vs)
        {
            valueStorage = _vs;
        }


        public void setObjAccessSub(GameObject _tObj, int _place)
        {
            ///	CHECK IF OUT OF BOUNDS
            if (_place >= objectAccess.Length || _place < 0)
            {
                return;
            }
            objectAccess[_place] = _tObj;
        }

        public void setCurrentEvent(GameEvent _CE)
        {
            currentEvent = _CE;
        }

        void create()
        {
            if (currentEvent.alive)
            {
                Vector3 rootPos = currentEvent.vectorStorage;
                for (int i = 0; i < currentEvent.otherAmount + currentEvent.amount; i++)
                {
                    Vector3 tVec = Vector3.zero;

                    bool uniqueName = false;

                    tVec.x += i * currentEvent.amount;
                    tVec.y += i * currentEvent.amount;
                    tVec.z += i * currentEvent.amount;

                    currentEvent.vectorStorage.x *= tVec.x;
                    currentEvent.vectorStorage.y *= tVec.y;
                    currentEvent.vectorStorage.z *= tVec.z;

                    currentEvent.vectorStorage.x += currentEvent.amount * currentEvent.otherAmount;

                    //	make prefabs withGameEvents attached
                    //	Change the game events currentEvent.obj to whatever is needed
                    //	Change the msgFilter to then decode and call that new objects->gameEvent->decodeMsgs to set it up ingame
                    //This will be a limitation and a limited number of things can be set, depending on how its prefab is created.
                    GameObject tObj = Instantiate(objectAccess[objectAccessList[currentEvent.objFilter]], Vector3.zero, Quaternion.identity) as GameObject;

                    string[] msgSplits = parseString(currentEvent.msgFilter);

                    for (int j = 0; j < msgSplits.Length; j++)
                    {

                        switch (msgSplits[j])
                        {
                            case "aschild":
                                tObj.transform.SetParent(gameObject.transform);
                                print(tObj.transform.parent.name);
                                currentEvent.vectorStorage.x += gameObject.transform.position.x;
                                currentEvent.vectorStorage.y += gameObject.transform.position.y;
                                currentEvent.vectorStorage.z += gameObject.transform.position.z;
                                tObj.transform.position = currentEvent.vectorStorage;
                                break;
                            case "position":
                                currentEvent.vectorStorage = rootPos;
                                tObj.transform.Translate(currentEvent.vectorStorage);
                                break;
                            case "uniquename":
                                uniqueName = true;
                                break;
                            default:
                                currentEvent.vectorStorage = gameObject.transform.position;
                                tObj.transform.Translate(currentEvent.vectorStorage);
                                break;
                        }
                    }

                    globalID++;

                    if (uniqueName == true)
                    {
                        tObj.name = currentEvent.word + ":" + globalID;//UnityEngine.Random.Range(0.0f, 1000.0f);
                    }
                    else
                    {
                        tObj.name = currentEvent.objFilter;//UnityEngine.Random.Range(0.0f, 1000.0f);
                    }

                    ///	SET THE SIZE OF THE ObjectAccess array
                    if (tObj.GetComponent<GenericBehavior>() != null)
                    {
                        tObj.GetComponent<GenericBehavior>().objectAccess = new GameObject[objectAccess.Length];
                    }
                    else
                    {
                        createdObjects.Add(tObj);
                        return;
                    }

                    for (int j = 0; j < objectAccess.Length; j++)
                    {

                        tObj.GetComponent<GenericBehavior>().objectAccess[j] = objectAccess[j];
                    }

                    ///	SET THE CURRENT EVENT SO IT KNOWS WHERE TO START??

                    tObj.GetComponent<GenericBehavior>().setCurrentEvent(tObj.GetComponent<GenericBehavior>().eventList[0]);

                    //					VESTIGE/*
                    //					foreach (GameEvent ge in tObj.GetComponent<GenericBehavior>().eventList)
                    //					{
                    //
                    //						//					ge.on = true;
                    //						ge.alive = true;
                    //
                    //					}*/

                    createdObjects.Add(tObj);

                    switch (currentEvent.playerAction)
                    {
                        case PlayerFunctions.INPUTDOWN:
                            //currentEvent.toggleOnOff();
                            currentEvent.on = false;
                            break;
                        case PlayerFunctions.COLLISIONENTER:
                            //currentEvent.toggleOnOff();
                            currentEvent.on = true;
                            break;
                        default:
                            currentEvent.on = false;
                            break;
                    }

                }

            }
        }


        /// <summary>
        /// Toggle an event's on or disabled states. It shuts itself(on) off.
        /// 	Amount values affect on:
        /// 		1 to turn on
        /// 		-1 to turn off
        /// 		0 to toggle
        /// 	PastAmount values affect active:
        /// 		1 to turn on
        /// 		-1 to turn off
        /// 		0 to toggle
        ///	IMPORTANT:
        /// 	Use any value other that {1,0,-1} in the amount or pastamount so that it doesn't touch either 
        /// TODO:
        /// 	Connect to gameObject
        /// </summary>
        public void toggle()
        {

            if (currentEvent.alive == false)
                return;

            GameEvent tempEvent = currentEvent;
            GameEvent originalEvent = currentEvent;
            //			string test = tempEvent.msgFilter;

            if (tempEvent.msgFilter.Length > 0)
            {

                char[] delimiterComma = { ',' };
                string _str = tempEvent.msgFilter;

                string[] msgSplits = _str.Split(delimiterComma);
                //				float tfloat = 0;
                ///	For future version in which an event can call another object's event.
                //GameObject go;



                for (int i = 0; i < msgSplits.Length; i++)
                {
                    int tInt = msgSplits[i].IndexOf('#');
                    tInt = (tInt <= -1 ? -1 : ++tInt);

                    //                    int tInt = msgSplits[i].IndexOf('#');
                    //                    tInt = (tInt <= -1 ? -1 : ++tInt);
                    if (currentEvent.objFilter.Length > 0)
                    {
                        _str = msgSplits[i].Substring(tInt);
                        //                        if (originalEvent.relation == ObjectRelation.PARENT)
                        //                        {
                        //                            collidedGO = collidedGO.transform.root.gameObject;
                        //                        }
                        if (currentEvent.filterType == ObjectNameType.NAME)
                        {

                            if (originalEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = collidedGO.transform.root.gameObject;
                            }
                            else
                            {
                                collidedGO = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject;
                            }
                            //                            if (collided == 0)
                            //                            {
                            //                                
                            //                                tempEvent = objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>().messageList[_str];
                            //                            }
                            //                            else
                            //                            {
                            tempEvent = collidedGO.GetComponent<GenericBehavior>().messageList[_str];
                            //                            }
                            //                            
                        }
                        else
                        {
                            if (originalEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = collidedGO.transform.root.gameObject;
                            }
                            //                            else
                            //                            {
                            //                                collidedGO = objectAccess[objectAccessList[currentEvent.objFilter]].gameObject;
                            //                            }
                            tempEvent = collidedGO.GetComponent<GenericBehavior>().messageList[_str];

                            //                            if (currentEvent.relation == ObjectRelation.PARENT)
                            //                            {
                            //                                collidedGO = other.transform.root.gameObject;
                            //                            }                            


                            //tempEvent = collidedGO.GetComponent<GenericBehavior>().messageList[_str];
                        }
                        //customEventList[currentEvent.eventName].Invoke();
                    }
                    else if (tInt >= 0)
                    {
                        _str = msgSplits[i].Substring(tInt);
                        tempEvent = messageList[_str];
                    }

                    //msgString = tempEvent.eventName;
                    //Debug.Log("Active:" + tempEvent.alive + ",On:" + tempEvent.on);
                    switch ((int)originalEvent.amount)
                    {
                        case 0:
                            tempEvent.on = !tempEvent.on;
                            break;
                        case -1:
                            tempEvent.on = false;
                            break;
                        case 1:
                            tempEvent.on = true;
                            break;
                        default:
                            break;
                    }

                    switch ((int)originalEvent.otherAmount)
                    {
                        case 0:
                            tempEvent.alive = !tempEvent.alive;
                            break;
                        case -1:
                            tempEvent.alive = false;
                            break;
                        case 1:
                            //							Debug.Log(tempEvent.alive);
                            tempEvent.alive = true;
                            break;
                        default:
                            break;
                    }
                    //Debug.Log("2Active:" + tempEvent.alive + ",2On:" + tempEvent.on);
                }
            }
            //	ERASE THE MESSAGESTRING SINCE IT HAS RETURNED FROM BEING SENT
            msgString = string.Empty;

            currentEvent = originalEvent;


        }

        /// <summary>
        /// USED to DEACTIVATE and do Nothing.
        /// Effectively this should leave the object dead to broadcasting and doing anything
        /// Other object may still interact with it, for example collisions
        /// 	"all" USE THE MESSAGE "all" in the msgFilter to deactivate all theGameEvents in this genericBehavior
        /// 	"#___" to make that object do nothing i.e. deaden it
        /// 	"" to deactivate itself:GameEvent
        /// </summary>
        void nothing()
        {
            /// Deactivate the msg
            string test = currentEvent.msgFilter;
            if (test.Length > 0)
            {
                int tInt = test.IndexOf('#') + 1;

                if (tInt > 0)
                {
                    test = test.Substring(tInt);
                    currentEvent = messageList[test];
                }

            }

            switch (currentEvent.msgFilter)
            {
                case "all":
                    foreach (KeyValuePair<string, GameEvent> ge in messageList)
                    {
                        ge.Value.alive = false;
                        ge.Value.on = false;
                    }

                    break;
                case "active":
                    foreach (KeyValuePair<string, GameEvent> ge in messageList)
                    {
                        ge.Value.alive = false;
                    }

                    break;
                case "on":
                    foreach (KeyValuePair<string, GameEvent> ge in messageList)
                    {
                        ge.Value.on = false;
                    }

                    break;
                default:
                    break;
            }

        }

        void lookAt(GameEvent _ce = null)
        {

            if (_ce == null)
                _ce = currentEvent;

            Vector3 tVec = Vector3.zero;
            Quaternion tQuat = Quaternion.identity;

            //	If the currentEvent.ObjName contains something, look at that object as a point in space
            if (_ce.objFilter.Length > 0)
            {

                tVec = objectAccess[objectAccessList[_ce.objFilter]].GetComponent<Transform>().position;
                tVec -= gameObject.transform.position;
                tQuat = Quaternion.LookRotation(tVec);

                gameObject.transform.rotation = tQuat;
            }
            else
            {//	look at a point in space specified in currentEvent.vectorStorage
                tVec = _ce.vectorStorage;
                tVec -= gameObject.transform.position;
                tQuat = Quaternion.LookRotation(tVec);
                gameObject.transform.rotation = tQuat;
            }
            gameObject.transform.Rotate(_ce.vectorStorage, _ce.amount);
        }


        #region SET function

        void set()
        {
            if (currentEvent.alive == false)
                return;


            bool setTrue = false;
            GameEvent tEvent;
            //Dummy Vector(s) used for temporary storage of a Vector within the switches scope
            Vector3 tVec = Vector3.zero;
            GameObject tGo;
            tGo = gameObject;
            //GenericBehavior gb;
            if (currentEvent.objFilter.Length > 0)
            {
                if (currentEvent.filterType == ObjectNameType.NAME)
                {
                    tGo = objectAccess[objectAccessList[currentEvent.objFilter]];
                }
                if (currentEvent.filterType == ObjectNameType.TAG)
                {
                    if (collided != 0 && collidedGO != null)
                    {
                        tGo = collidedGO;
                    }
                }
                if (currentEvent.relation == ObjectRelation.ME)
                {
                    tGo = gameObject;
                }
            }

            Quaternion tQuat = Quaternion.identity;
            string[] msgSplits = parseString(currentEvent.msgFilter);

            for (int i = 0; i < msgSplits.Length; i++)
            {

                switch (msgSplits[i])
                {
                    //switch (tStr) {
                    case "amount":
                        currentEvent.amount = currentEvent.otherAmount;
                        break;
                    case "enablecaninteract":
                    case "caninteract":
                        if (currentEvent.objFilter != string.Empty)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>().canInteract =
                             ((currentEvent.amount == 0 ? !objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>().canInteract : (currentEvent.amount > 0 ? true : false)));
                        }
                        else
                        {
                            canInteract = ((currentEvent.amount == 0 ? !canInteract : (currentEvent.amount > 0 ? true : false)));
                        }
                        break;
                    case "dechild":
                        tGo.transform.DetachChildren();
                        break;
                    case "deltatime":
                        if (Time.timeScale > 0)
                            Time.timeScale = currentEvent.amount;
                        else
                            Time.timeScale = currentEvent.otherAmount;
                        break;
                    case "deparent":
                        tGo.transform.parent = null;//SetParent();//DetachChildren();
                        break;
                    case "enablerenderer":
                        if (!hasRenderer)
                            return;
                        if (currentEvent.objFilter != string.Empty)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Renderer>().enabled = (currentEvent.amount == 0 ? !objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Renderer>().enabled : (currentEvent.amount > 0 ? true : false));
                        }
                        else
                        {
                            gameObject.GetComponent<Renderer>().enabled = (currentEvent.amount == 0 ? !gameObject.GetComponent<Renderer>().enabled : (currentEvent.amount > 0 ? true : false));
                        }
                        break;
                    case "enablegameobject":
                        if (currentEvent.objFilter != string.Empty)
                        {
                            //objectAccess[objectAccessList[currentEvent.objFilter]].activeInHierarchy
                            objectAccess[objectAccessList[currentEvent.objFilter]].SetActive((currentEvent.amount == 0 ? !objectAccess[objectAccessList[currentEvent.objFilter]].activeInHierarchy : (currentEvent.amount > 0 ? true : false)));
                        }
                        else
                        {
                            gameObject.SetActive((currentEvent.amount == 0 ? !gameObject.activeInHierarchy : (currentEvent.amount > 0 ? true : false)));
                        }
                        break;
                    case "enablecollider":
                        if (!hasCollider)
                            return;

                        if (currentEvent.objFilter != string.Empty)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Collider>().enabled = (currentEvent.amount == 0 ? !objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Collider>().enabled : (currentEvent.amount > 0 ? true : false));
                        }
                        else
                        {
                            gameObject.GetComponent<Collider>().enabled = (currentEvent.amount == 0 ? !gameObject.GetComponent<Collider>().enabled : (currentEvent.amount > 0 ? true : false));
                        }
                        break;
                    case "enableistrigger":
                        if (!hasCollider)
                            return;

                        if (currentEvent.objFilter != string.Empty)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Collider>().isTrigger = (currentEvent.amount == 0 ? !objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Collider>().isTrigger : (currentEvent.amount > 0 ? true : false));
                        }
                        else
                        {
                            gameObject.GetComponent<Collider>().isTrigger = (currentEvent.amount == 0 ? !gameObject.GetComponent<Collider>().isTrigger : (currentEvent.amount > 0 ? true : false));
                        }
                        break;
                    case "exit":
                        Application.Quit();
                        break;
                    case "forcemodifier":
                        forceParticle = currentEvent.amount;
                        break;
                    case "forwardvector":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().forwardVector = currentEvent.vectorStorage;
                        }
                        else
                        {
                            forwardVector = currentEvent.vectorStorage;
                        }
                        break;
                    case "freezerotations":
                        tVec = currentEvent.vectorStorage;
                        if (hasRigidbody)
                        {
                            storedRB.constraints = (tVec.x > 0 ? RigidbodyConstraints.FreezeRotationX : RigidbodyConstraints.None) | (tVec.y > 0 ? RigidbodyConstraints.FreezeRotationY : RigidbodyConstraints.None) | (tVec.z > 0 ? RigidbodyConstraints.FreezeRotationZ : RigidbodyConstraints.None);
                        }
                        break;
                    case "freezepositions":
                        if (hasRigidbody)
                        {
                            tVec = currentEvent.vectorStorage;
                            storedRB.constraints = (tVec.x > 0 ? RigidbodyConstraints.FreezePositionX : RigidbodyConstraints.None) | (tVec.y > 0 ? RigidbodyConstraints.FreezePositionY : RigidbodyConstraints.None) | (tVec.z > 0 ? RigidbodyConstraints.FreezePositionZ : RigidbodyConstraints.None);
                        }
                        break;
                    case "globalgravity":
                        Physics.gravity = currentEvent.vectorStorage;
                        globalGravity = currentEvent.vectorStorage;
                        break;

                    case "kinematic":
                    case "enablekinematic":
                        if (hasRigidbody)
                        {
                            storedRB.isKinematic = (currentEvent.amount == 0 ? !storedRB.isKinematic : (currentEvent.amount > 0 ? true : false));
                            //storedRB.isKinematic = (currentEvent.amount > 0 ? true : !storedRB.isKinematic);
                        }
                        break;
                    case "lookat":
                        lookAt(currentEvent);
                        break;
                    case "layer":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].layer = (int)currentEvent.amount;
                        }
                        else
                        {
                            gameObject.layer = (int)currentEvent.amount;
                        }
                        break;
                    case "mass":
                        if (hasRigidbody)
                        {
                            storedRB.mass = currentEvent.amount;
                            mass = currentEvent.amount;
                        }
                        break;
                    case "maxangularvelocity":

                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().accessPrivateFloats("maxangmagnitude", currentEvent.amount, true);
                        }
                        else
                        {
                            maxMagnitudeAng = currentEvent.amount;
                        }
                        break;

                    case "maxvelmagnitude":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().accessPrivateFloats("maxvelmagnitude", currentEvent.amount, true);
                        }
                        else
                        {
                            maxMagnitude = currentEvent.amount;
                        }
                        break;
                    case "minangularvelocity":

                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().accessPrivateFloats("minangmagnitude", currentEvent.amount, true);
                        }
                        else
                        {
                            minMagnitudeAng = currentEvent.amount;
                        }
                        break;

                    case "minvelmagnitude":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().accessPrivateFloats("minvelmagnitude", currentEvent.amount, true);
                        }
                        else
                        {
                            minMagnitude = currentEvent.amount;
                        }
                        break;
                    case "maincolor":
                        gameObject.GetComponent<Renderer>().material.color = mainColor;
                        break;
                    case "name":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].name = currentEvent.word;
                        }
                        else
                        {
                            gameObject.name = currentEvent.word;
                        }
                        break;
                    case "originalcolor":
                        gameObject.GetComponent<Renderer>().material.color = originalColor;
                        break;
                    case "othercolor":
                        gameObject.GetComponent<Renderer>().material.color = otherColor;
                        break;
                    case "objectpause":
                        objectPause = (!objectPause);
                        break;
                    case "otheramount":
                        currentEvent.otherAmount = currentEvent.amount;
                        break;
                    case "on":
                        setTrue = true;
                        break;
                    case "off":
                        setTrue = false;
                        break;
                    case "parent":
                        gameObject.transform.SetParent(tGo.transform);
                        break;
                    case "parentme":
                        gameObject.transform.SetParent(tGo.transform, true);
                        if (tGo.GetComponent<GenericBehavior>() != null)
                        {
                            maxMagnitude = tGo.GetComponent<GenericBehavior>().accessPrivateFloats("maxvelmagnitude");
                            maxMagnitude = tGo.GetComponent<GenericBehavior>().accessPrivateFloats("minvelmagnitude");
                            maxMagnitude = tGo.GetComponent<GenericBehavior>().accessPrivateFloats("maxangmagnitude");
                            maxMagnitude = tGo.GetComponent<GenericBehavior>().accessPrivateFloats("minangmagnitude");
                        }
                        else
                        {
                            maxMagnitude = -1;
                            minMagnitude = -1;
                            maxMagnitudeAng = -1;
                            minMagnitudeAng = -1;
                        }
                        break;
                    case "parentit":
                        gameObject.transform.position = currentEvent.vectorStorage;//tGo.gameObject.transform.position;
                        tGo.transform.SetParent(gameObject.transform);
                        break;
                    case "position":
                        tVec = currentEvent.vectorStorage;
                        tVec.x *= currentEvent.amount;
                        tVec.y *= currentEvent.amount;
                        tVec.z *= currentEvent.amount;
                        tGo.transform.position = tVec;
                        break;
                    case "realgravity":
                        realGravity = currentEvent.vectorStorage;
                        break;
                    case "resetangular":
                        if (hasRigidbody)
                        {
                            storedRB.angularVelocity = Vector3.zero;
                        }
                        break;
                    case "resetrotation":
                        tVec.x = tGo.transform.rotation.eulerAngles.x * currentEvent.vectorStorage.x;
                        tVec.y = tGo.transform.rotation.eulerAngles.y * currentEvent.vectorStorage.y;
                        tVec.z = tGo.transform.rotation.eulerAngles.z * currentEvent.vectorStorage.z;

                        tGo.transform.eulerAngles = tVec;//Quaternion.identity;
                        break;
                    case "rotationidentity":
                        tGo.transform.rotation = Quaternion.identity;
                        break;
                    case "rightvector":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().rightVector = currentEvent.vectorStorage;
                        }
                        else
                        {
                            rightVector = currentEvent.vectorStorage;
                        }
                        break;
                    case "rotate":
                        tVec = Vector3.zero;
                        tVec = currentEvent.vectorStorage;
                        tVec.x *= currentEvent.amount;
                        tVec.y *= currentEvent.amount;
                        tVec.z *= currentEvent.amount;
                        if (currentEvent.objFilter.Length > 0)
                        {
                            gameObject.transform.RotateAround(objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().position, upVector, currentEvent.amount);
                        }
                        else
                        {
                            gameObject.transform.Rotate(tVec.x, tVec.y, tVec.z);
                        }

                        break;
                    case "torotation":
                    case "rotation":
                        tVec = Vector3.zero;
                        tVec = currentEvent.vectorStorage;
                        tVec.x *= currentEvent.amount;
                        tVec.y *= currentEvent.amount;
                        tVec.z *= currentEvent.amount;
                        if (currentEvent.objFilter.Length > 0)
                        {
                            if (currentEvent.otherAmount != 0)
                            {
                                objectAccess[objectAccessList[currentEvent.objFilter]].transform.eulerAngles = tVec;
                            }
                            else
                            {
                                gameObject.transform.eulerAngles = objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().eulerAngles;
                            }
                        }
                        else
                        {
                            gameObject.transform.eulerAngles = tVec;
                        }
                        break;
                    case "storedvector"://can be used to "add" an artificial gravity
                        storedVector = currentEvent.vectorStorage;
                        break;
                    case "stop":
                        tVec = currentEvent.vectorStorage;
                        coroutine = StopMotion();
                        StartCoroutine(coroutine);
                        if (hasRigidbody)
                        {
                            storedRB.velocity = Vector3.zero;
                            storedRB.angularVelocity = Vector3.zero;
                            storedRB.Sleep();
                        }
                        break;
                    //					case "delay":
                    //						coroutine = delayTime(currentEvent.amount);
                    //						StartCoroutine(coroutine);
                    //						break;					

                    case "scale":
                        tVec = Vector3.zero;
                        tVec = Vector3.zero;
                        tVec = currentEvent.vectorStorage;
                        tVec.x *= currentEvent.amount;
                        tVec.y *= currentEvent.amount;
                        tVec.z *= currentEvent.amount;
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tVec.Scale(objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().localScale);
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().localScale = tVec;
                        }
                        else
                        {
                            tVec.Scale(gameObject.transform.localScale);
                            gameObject.transform.localScale = tVec;
                        }
                        break;
                    case "scaleby":
                        tVec = Vector3.zero;
                        tVec = Vector3.zero;
                        tVec = currentEvent.vectorStorage;
                        tVec.x *= currentEvent.amount;
                        tVec.y *= currentEvent.amount;
                        tVec.z *= currentEvent.amount;
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tVec.Scale(objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().localScale);
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().localScale = tVec;
                        }
                        else
                        {
                            tVec.Scale(gameObject.transform.localScale);
                            gameObject.transform.localScale = tVec;
                        }
                        break;
                    case "setfloatindexsize":
                        if (currentEvent.amount == 0 || currentEvent.otherAmount == 0)
                        {
                            continue;
                        }
                        if (currentEvent.playerAction == PlayerFunctions.AWAKE)
                        {
                            indexFloatArray = new float[Mathf.Abs((int)currentEvent.amount), Mathf.Abs((int)currentEvent.otherAmount)];
                        }
                        break;
                    case "setstringindexsize":
                        if (currentEvent.amount == 0 || currentEvent.otherAmount == 0)
                        {
                            continue;
                        }
                        if (currentEvent.playerAction == PlayerFunctions.AWAKE)
                        {
                            indexStringArray = new string[Mathf.Abs((int)currentEvent.amount), Mathf.Abs((int)currentEvent.otherAmount)];
                        }

                        break;
                    case "specificcolor":
                        gameObject.GetComponent<Renderer>().material.color = new Color(currentEvent.vectorStorage.x, currentEvent.vectorStorage.y, currentEvent.vectorStorage.y, currentEvent.amount);
                        break;
                    case "storedposition":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().storedPosition = currentEvent.vectorStorage;
                        }
                        else
                        {
                            storedPosition = currentEvent.vectorStorage;
                        }
                        break;
                    case "toposition":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].transform.position = currentEvent.vectorStorage;
                        }
                        else
                        {
                            gameObject.transform.position = currentEvent.vectorStorage;
                        }

                        break;
                    case "toscale":
                        tVec = Vector3.zero;
                        tVec = Vector3.zero;
                        tVec = currentEvent.vectorStorage;
                        tVec.x *= currentEvent.amount;
                        tVec.y *= currentEvent.amount;
                        tVec.z *= currentEvent.amount;
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<Transform>().localScale = tVec;
                        }
                        else
                        {
                            gameObject.transform.localScale = tVec;
                        }
                        break;
                    case "translate":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.transform.Translate(currentEvent.vectorStorage);
                        }
                        else
                        {
                            gameObject.transform.Translate(currentEvent.vectorStorage);
                        }
                        break;


                    case "tag":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].tag = currentEvent.word;
                        }
                        else
                        {
                            gameObject.transform.Translate(currentEvent.vectorStorage);
                        }
                        break;
                    case "tostoredposition":
                        gameObject.transform.position = storedPosition;
                        break;
                    case "unparent":
                        tGo.transform.DetachChildren();
                        break;
                    case "unchild":
                        tGo.transform.parent = null;//SetParent();//DetachChildren();
                        break;
                    case "upvector":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            tGo.GetComponent<GenericBehavior>().upVector = currentEvent.vectorStorage;
                        }
                        else
                        {
                            upVector = currentEvent.vectorStorage;
                        }
                        break;

                    case "enablegravity":
                        if (hasRigidbody)
                        {
                            storedRB.useGravity = (currentEvent.amount == 0 ? !storedRB.useGravity : (currentEvent.amount > 0 ? true : false));
                        }
                        break;
                    case "use_gb_fakegravity":
                        useFakeGravity = (currentEvent.amount == 0 ? !useFakeGravity : (currentEvent.amount > 0 ? true : false));
                        if (useFakeGravity == false)
                        {
                            timeCounter = 0;
                        }
                        else
                        {
                            timeCounter = Time.time;
                        }
                        /*setTrue = true;
                        currentEvent.on = useFakeGravity;*/
                        //Debug.Log("Apply GlobalGravity:" + useFakeGravity.ToString());
                        break;
                    case "use_gb_realgravity":
                        useRealGravity = (currentEvent.amount == 0 ? !useRealGravity : (currentEvent.amount > 0 ? true : false));
                        ;
                        break;
                    case "valuestorage":
                        valueStorage = currentEvent.amount;
                        break;
                    case "vectormodifier"://can be used to "add" an artificial gravity
                        vectorModifier = currentEvent.vectorStorage;
                        break;
                    case "velocity":
                        tVec.x = currentEvent.amount;
                        tVec.y = currentEvent.amount;
                        tVec.z = currentEvent.amount;

                        tVec.x *= currentEvent.vectorStorage.x;
                        tVec.y *= currentEvent.vectorStorage.y;
                        tVec.z *= currentEvent.vectorStorage.z;

                        if (hasRigidbody == true)
                        {
                            tGo.GetComponent<Rigidbody>().velocity = gameObject.transform.rotation * tVec;
                            tGo.GetComponent<Rigidbody>().AddForce(tGo.GetComponent<Rigidbody>().velocity, ForceMode.VelocityChange);
                        }
                        break;
                    default:
                        ////string _str = msgsplits[k].Trim(delimiterHash);
                        //						int tInt = msgSplits[i].IndexOf('#') + 1;
                        //
                        //						if (tInt > 0)
                        //						{
                        //							msgSplits[i] = msgSplits[i].Substring(tInt);
                        //							tEvent = currentEvent;
                        //							currentEvent = messageList[msgSplits[i]];
                        //							customEventList[currentEvent.eventName].Invoke();
                        //						}
                        //if ()
                        break;



                }//SWITCH
            }
            currentEvent.on = setTrue;//false;
        }

        #endregion



        public float indexFloatMap(int x, int y, float _val = 0, bool write = false)
        {
            if (x * y < 0 || x >= indexFloatArray.GetLength(0) || y >= indexFloatArray.GetLength(1))
            {
                Debug.Log("Behavior:" + gameObject.name + ", Event:" + currentEvent.eventName + "has tried to access the indexMap, but amount or other amount  are out of range");
                return -1;
            }

            if (!write)
            {
                return indexFloatArray[x, y];
            }
            else
            {
                indexFloatArray[x, y] = _val;
            }

            return indexFloatArray[x, y];
        }

        public string indexStringMap(int x, int y, string _str = "", bool write = false)
        {
            if (x * y < 0)
            {
                Debug.Log("Behavior:" + gameObject.name + ", Event:" + currentEvent.eventName + "has tried to access the indexMap, but amount or other amount  are out of range");
            }
            if (!write)
            {
                return indexStringArray[x, y];
            }
            else
            {
                indexStringArray[x, y] = _str;
            }

            return indexStringArray[x, y];

        }

        void info_index()
        {
            if (currentEvent.alive == false)// || currentEvent.on == false) 
                return;

            float tFloat = 0;
            string tStr = currentEvent.msgFilter;

            tStr = "";

            //Dummy Vector(s) used for temporary storage of a Vector within the switches scope
            //			Vector3 tVec = Vector3.zero;

            //			Quaternion tQuat = Quaternion.identity;
            string[] msgSplits = parseString(currentEvent.msgFilter);

            for (int i = 0; i < msgSplits.Length; i++)
            {

                switch (msgSplits[i])
                {
                    case "gotostoredposition":
                        break;
                    case "upvector":
                        break;
                    case "dontdestroyonload":
                        break;
                    case "setstartposition":
                        break;
                    case "objectPause":
                        break;
                    case "maxvelmagnitude":
                        break;
                    case "maxangmagnitude":
                        break;
                    case "minvelmagnitude":
                        break;
                    case "minangmagnitude":
                        break;
                    case "scoreamount":
                        break;
                    case "minmax":
                        break;
                    case "all":
                        break;
                    case "velocity":
                        break;
                    case "rotation":
                        break;
                    case "scale":
                        break;
                    case "clearstringindex":
                        break;
                    case "clearfloatindex":
                        if (indexFloatSet)
                        {
                            Array.Clear(indexFloatArray, 0, indexFloatArray.Length);
                            indexFloatSet = false;
                        }

                        break;
                    case "readfile":

                        //				if (indexStringSet)
                        //				{
                        //					Array.Clear(indexStringArray, 0, indexStringArray.Length);
                        //				}
                        if (indexFloatSet)
                        {
                            Array.Clear(indexFloatArray, 0, indexFloatArray.Length);
                        }

                        readFileIntoArray(currentEvent.objFilter);
                        indexFloatSet = true;


                        break;
                    case "writefile":

                        //				if (indexStringSet)
                        //				{
                        //					Array.Clear(indexStringArray, 0, indexStringArray.Length);
                        //				}
                        if (!indexFloatSet)
                        {
                            Debug.Log("File was not written; index was empty.");
                            continue;
                        }

                        writeArrayIntoFile(currentEvent.objFilter);

                        break;
                    case "searchstringindex":
                        if (indexStringSet)
                        {
                            for (int j = 0; j < indexStringArray.GetLength(0); j++)
                            {
                                for (int k = 0; k < indexStringArray.GetLength(1); k++)
                                {
                                    tStr = indexStringMap(j, k);
                                    if (tStr == currentEvent.word)
                                    {
                                        currentEvent.vectorStorage.x = j;
                                        currentEvent.vectorStorage.y = k;
                                        j = indexStringArray.GetLength(0);
                                        k = indexStringArray.GetLength(1);
                                    }
                                }
                            }
                        }
                        break;
                    case "searchfloatindex":
                        if (indexFloatSet)
                        {
                            for (int j = 0; j < indexFloatArray.GetLength(0); j++)
                            {
                                for (int k = 0; k < indexFloatArray.GetLength(1); k++)
                                {
                                    tFloat = indexFloatMap(j, k);
                                    if (tFloat == currentEvent.amount)
                                    {
                                        currentEvent.vectorStorage.x = j;
                                        currentEvent.vectorStorage.y = k;
                                        j = indexStringArray.GetLength(0);
                                        k = indexStringArray.GetLength(1);
                                        currentEvent.on = true;
                                    }
                                }

                            }
                        }
                        break;
                    case "setstringindex":
                        if (indexStringSet)
                        {
                            indexStringMap((int)currentEvent.vectorStorage.x, (int)currentEvent.vectorStorage.y, currentEvent.word, true);
                        }
                        break;
                    case "setfloatindex":
                        if (indexFloatSet)
                        {
                            indexFloatMap((int)currentEvent.vectorStorage.x, (int)currentEvent.vectorStorage.y, currentEvent.amount, true);
                        }
                        break;
                    case "setfloatindexrange":
                        if (indexFloatSet)
                        {
                            for (int j = (int)currentEvent.vectorStorage.x; j < (int)currentEvent.amount; j++)
                            {
                                for (int k = (int)currentEvent.vectorStorage.y; k < (int)currentEvent.otherAmount; k++)
                                {
                                    indexFloatMap((int)currentEvent.vectorStorage.x, (int)currentEvent.vectorStorage.y, currentEvent.vectorStorage.z, true);
                                }
                            }
                        }
                        break;
                    //case "indexfloatarraysize":
                    case "setstringindexsize":
                        if ((int)currentEvent.vectorStorage.x <= 0 || (int)currentEvent.vectorStorage.y <= 0)
                        {
                            Debug.Log("OutOfBounds string array setsize in " + gameObject.name + ":" + currentEvent.eventName);
                            continue;
                        }
                        if (currentEvent.playerAction == PlayerFunctions.AWAKE && !indexStringSet)
                        {
                            indexStringArray = new string[Mathf.Abs((int)currentEvent.amount), Mathf.Abs((int)currentEvent.otherAmount)];
                            indexStringSet = true;
                        }

                        break;
                    case "setfloatindexsize":
                        if ((int)currentEvent.vectorStorage.x <= 0 || (int)currentEvent.vectorStorage.y <= 0)
                        {
                            Debug.Log("OutOfBounds float array setsize in " + gameObject.name + ":" + currentEvent.eventName);
                            continue;
                        }
                        if (!indexFloatSet)
                        {
                            //indexFloatArray = new float[Mathf.Abs((int)currentEvent.amount), Mathf.Abs((int)currentEvent.otherAmount)];
                            indexFloatArray = new float[Mathf.Abs((int)currentEvent.vectorStorage.x), Mathf.Abs((int)currentEvent.vectorStorage.y)];
                            for (int j = 0; j < (int)currentEvent.vectorStorage.x; j++)
                            {
                                for (int k = 0; k < (int)currentEvent.vectorStorage.y; k++)
                                {
                                    indexFloatMap(j, k, currentEvent.vectorStorage.z, true);
                                }
                            }
                            indexFloatSet = true;
                        }
                        break;
                }
            }
        }

        #region Randomize function

        void randomize()
        {

            string _str = currentEvent.msgFilter;

            char[] delimiterComma = { ',', ':', '>' };

            // if no string msg is sent then there's nothing to do; so exit this function!
            if (_str.Length == 0)
            {
                return;
            }

            if (currentEvent.alive == false)
            {
                return;
            }

            string[] msgSplits = _str.Split(delimiterComma);

            for (int i = 0; i < msgSplits.Length; i++)
            {
                float tempAmount = 0;
                switch (msgSplits[i])
                {
                    case "seed":
                        if (currentEvent.amount < 0)
                            UnityEngine.Random.seed = (int)System.DateTime.Now.ToBinary();
                        else
                            UnityEngine.Random.seed = (int)currentEvent.amount;

                        break;
                    case "diceroll":
                        for (int j = 0; j < currentEvent.vectorStorage.x; j++)
                        {
                            currentEvent.amount += UnityEngine.Random.Range(1, (int)currentEvent.vectorStorage.y);
                        }
                        break;
                    case "vectormodifier":
                        vectorModifier.x = UnityEngine.Random.Range(0.0f, currentEvent.vectorStorage.x);
                        vectorModifier.y = UnityEngine.Random.Range(0.0f, currentEvent.vectorStorage.y);
                        vectorModifier.z = UnityEngine.Random.Range(0.0f, currentEvent.vectorStorage.z);
                        break;
                    case "storedposition":
                        tempAmount = UnityEngine.Random.Range(currentEvent.otherAmount, currentEvent.amount);
                        storedPosition.x = tempAmount * currentEvent.vectorStorage.x;
                        tempAmount = UnityEngine.Random.Range(currentEvent.otherAmount, currentEvent.amount);
                        storedPosition.y = tempAmount * currentEvent.vectorStorage.y;
                        tempAmount = UnityEngine.Random.Range(currentEvent.otherAmount, currentEvent.amount);
                        storedPosition.z = tempAmount * currentEvent.vectorStorage.z;
                        break;
                    case "velocity":
                        tempAmount = UnityEngine.Random.Range(currentEvent.otherAmount, currentEvent.amount);
                        Vector3 tVec = currentEvent.vectorStorage;

                        tVec.x *= tempAmount;
                        tVec.y *= tempAmount;
                        tVec.z *= tempAmount;

                        Vector3 tVecRB = vectorModifier;

                        tVecRB.x += tVec.x;
                        tVecRB.y += tVec.y;
                        tVecRB.z += tVec.z;

                        storedForces += tVecRB;
                        break;
                    case "amount":
                        tempAmount = currentEvent.amount;
                        currentEvent.amount = UnityEngine.Random.Range(0.0f, currentEvent.otherAmount);
                        currentEvent.otherAmount = tempAmount;
                        break;

                    case "setvaluestoragetoamount":
                    case "sendamount":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>().accessPrivateFloats("valuestorage", currentEvent.amount, true);
                        }
                        else
                            valueStorage = currentEvent.amount;
                        break;
                    case "sendvaluestorage":
                        if (currentEvent.objFilter.Length > 0)
                        {
                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>().accessPrivateFloats("valuestorage", valueStorage, true);
                        }

                        break;
                    case "valuestorage":
                    default:
                        tempAmount = UnityEngine.Random.Range(currentEvent.otherAmount, currentEvent.amount);
                        if (currentEvent.objFilter.Length > 0)
                        {

                            objectAccess[objectAccessList[currentEvent.objFilter]].GetComponent<GenericBehavior>().setValueStorage(tempAmount);
                        }
                        else
                        {
                            valueStorage = tempAmount;//currentEvent.amount;
                        }
                        break;
                }

            }

            msgString = string.Empty;

        }

        #endregion

        // Use this for initialization

        #region Start Function

        void Start()
        {

            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (objectAccess.Length == 0)
                {
                    objectAccess = new GameObject[1];
                    objectAccess[0] = gameObject;
                    objectAccessList.Add("gameObject", 0);
                }
                else
                {
                    GameObject[] tArr = new GameObject[objectAccess.Length + 1];
                    tArr[0] = gameObject;
                    objectAccessList.Add("gameObject", 0);

                    for (int i = 1; i < tArr.Length; i++)
                    {
                        tArr[i] = objectAccess[i - 1];
                    }

                    objectAccess = null;
                    objectAccess = new GameObject[tArr.Length];
                    objectAccess[0] = tArr[0];

                    for (int i = 1; i < tArr.Length; i++)
                    {
                        //if (objectAccess[i] != null)
                        //{
                        objectAccess[i] = tArr[i];
                        objectAccessList.Add(tArr[i].name, i);
                        //}
                    }
                }

                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;
                    msgString = currentEvent.objFilter;
                    if (kList.alive)
                    {
                        if (kList.playerAction == PlayerFunctions.START)
                        {
                            switch (kList.gameAction)
                            {
                                case GameFunctions.INFO_IO:
                                    customEventList[kList.eventName].Invoke();
                                    break;
                                default:
                                    break;

                            }
                        }
                    }
                }

                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;
                    msgString = currentEvent.objFilter;
                    //kList.otherAmount = kList.amount;
                    if (kList.alive)
                    {
                        if (kList.playerAction == PlayerFunctions.START)
                        {
                            currentEvent.on = true;
                            switch (kList.gameAction)
                            {
                                case GameFunctions.BEHAVIOR:
                                    behavior();
                                    break;
                                case GameFunctions.COMPARE_DATA:
                                    compareMachine();
                                    break;
                                case GameFunctions.GET_MOVE_DATA:
                                    get();
                                    break;
                                case GameFunctions.COMPUTE:
                                    compute();
                                    break;
                                case GameFunctions.CALL_EVENT:
                                    call();
                                    break;
                                case GameFunctions.DESTROY:
                                    destroy();
                                    break;
                                case GameFunctions.IF_EVENT_ON:
                                    if_event_on();
                                    break;
                                case GameFunctions.INSTANTIATE:
                                    create();
                                    break;
                                case GameFunctions.IMITATE:
                                    imitate();//customEventList[kList.eventName].Invoke();
                                              //init ();
                                    break;
                                case GameFunctions.RANDOMIZE:
                                    randomize();
                                    break;
                                case GameFunctions.SET:
                                    set();
                                    break;
                                case GameFunctions.SEND_MSG:
                                    send();
                                    break;
                                case GameFunctions.MOTION:
                                    motion();
                                    break;
                                case GameFunctions.MODIFY_VALUES:
                                    modify();
                                    break;
                                case GameFunctions.REGISTER:
                                    register();
                                    break;
                                case GameFunctions.PAUSE:
                                    currentEvent.on = true;
                                    pause();
                                    break;
                                case GameFunctions.PLAY:
                                    playBehavior();
                                    break;
                                case GameFunctions.TIMER:
                                    //currentEvent.on = true;
                                    currentEvent.playerAction = PlayerFunctions.SYSTEM;
                                    timer();
                                    break;
                                default:
                                    break;

                            }
                        }
                    }
                }

                if (hasRigidbody)
                {
                    if (storedRB.useGravity)
                    {
                        globalGravity = Physics.gravity;

                    }
                }


            }//!GetComponent<GenericBehavior>().isActiveAndEnabled


        }

        #endregion

        void imitate()
        {
            if (currentEvent.alive == false)
                return;

            GameObject tObj = gameObject;
            if (currentEvent.objFilter.Length == 0)
            {
                if (gameObject.transform.parent.gameObject != gameObject)
                {
                    tObj = gameObject.transform.parent.gameObject;
                    //return;
                }
            }
            else
            {
                tObj = objectAccess[objectAccessList[currentEvent.objFilter]];
            }

            switch (currentEvent.gameAction)
            {
                case GameFunctions.IMITATE:

                    Vector3 tVec = Vector3.zero;
                    Vector3 tVec2 = Vector3.zero;
                    Quaternion tQuat = Quaternion.identity;
                    //					Quaternion tQuat2 = Quaternion.identity;
                    switch (currentEvent.msgFilter)
                    {
                        case "position":
                            if (hasRigidbody == true)
                            {
                                tVec = tObj.GetComponent<Rigidbody>().position;
                                tVec2 = gameObject.transform.position;

                                tVec.x *= currentEvent.vectorStorage.x;
                                tVec.y *= currentEvent.vectorStorage.y;
                                tVec.z *= currentEvent.vectorStorage.z;

                                if (currentEvent.vectorStorage.x != 0)
                                    tVec2.x = tVec.x + currentEvent.amount;
                                if (currentEvent.vectorStorage.y != 0)
                                    tVec2.y = tVec.y + currentEvent.amount;
                                if (currentEvent.vectorStorage.z != 0)
                                    tVec2.z = tVec.z + currentEvent.amount;

                                storedRB.MovePosition(tVec2);
                                return;

                            }
                            tVec = tObj.GetComponent<Transform>().position;
                            tVec2 = gameObject.transform.position;

                            tVec.x *= currentEvent.vectorStorage.x;
                            tVec.y *= currentEvent.vectorStorage.y;
                            tVec.z *= currentEvent.vectorStorage.z;

                            if (currentEvent.vectorStorage.x != 0)
                                tVec2.x = tVec.x + currentEvent.amount;
                            if (currentEvent.vectorStorage.y != 0)
                                tVec2.y = tVec.y + currentEvent.amount;
                            if (currentEvent.vectorStorage.z != 0)
                                tVec2.z = tVec.z + currentEvent.amount;

                            gameObject.GetComponent<Transform>().position = tVec2;

                            break;
                        case "movement":

                            if (hasRigidbody == true)
                            {
                                //tVec = objectAccess [objectAccessList [currentEvent.objFilter]].GetComponent<GenericBehavior> ().storedForces;
                                tVec = tObj.GetComponent<GenericBehavior>().storedForces;
                                tVec.x *= currentEvent.vectorStorage.x;
                                tVec.y *= currentEvent.vectorStorage.y;
                                tVec.z *= currentEvent.vectorStorage.z;
                                storedForces += tVec;
                                //QUESTIONABLE SINCE VELOCITY IS BEING PROCESSED IN FIXEDUPDATE
                                storedRB.velocity = tVec;
                                return;
                            }
                            tVec = tObj.GetComponent<Transform>().position;
                            tVec.x *= currentEvent.vectorStorage.x;
                            tVec.y *= currentEvent.vectorStorage.y;
                            tVec.z *= currentEvent.vectorStorage.z;

                            gameObject.GetComponent<Transform>().position = tVec;

                            break;
                        case "scale":
                            tVec = tObj.GetComponent<Transform>().localScale;
                            tVec2 = gameObject.transform.localScale;

                            tVec.x *= currentEvent.vectorStorage.x;
                            tVec.y *= currentEvent.vectorStorage.y;
                            tVec.z *= currentEvent.vectorStorage.z;

                            if (currentEvent.vectorStorage.x != 0)
                                tVec2.x = tVec.x * currentEvent.amount;
                            if (currentEvent.vectorStorage.y != 0)
                                tVec2.y = tVec.y * currentEvent.amount;
                            if (currentEvent.vectorStorage.z != 0)
                                tVec2.z = tVec.z * currentEvent.amount;

                            gameObject.transform.localScale = tVec2;

                            break;
                        case "rotation":
                            tQuat = tObj.transform.rotation;
                            //							tQuat2 = gameObject.transform.rotation;

                            //							float tFloat = currentEvent.amount;

                            tQuat.x *= currentEvent.vectorStorage.x;// * tFloat;
                            tQuat.y *= currentEvent.vectorStorage.y;// * tFloat;
                            tQuat.z *= currentEvent.vectorStorage.z;// * tFloat;


                            //if (hasRigidbody == true)
                            //{

                            //storedRB.rotation = tQuat;
                            gameObject.transform.rotation = tQuat;
                            //}
                            break;
                        case "color":
                            if (gameObject.GetComponent<Renderer>() != null)
                            {
                                gameObject.GetComponent<Renderer>().material.color = tObj.GetComponent<Renderer>().material.color;
                            }

                            break;
                        default:

                            break;
                    }

                    break;
            }


        }

        string[] parseString(string stringToParse)
        {
            char[] delimiterComma = { ',', ':', '>' };

            if (stringToParse.Length == 0)
            {
                return new string[0];
            }

            string[] msgSplits = stringToParse.Split(delimiterComma);

            return msgSplits;
        }
        void doMSG(string _str)
        {
            currentEvent.on = true;
            customEventList[_str].Invoke();
            currentEvent.on = false;
        }
        //IEnumerator
        GameEvent decodeMSG(string _str)
        {
            GameEvent tEvent = currentEvent;
            string test = string.Empty;
            if (_str.Length > 0)
            {
                int tInt = _str.IndexOf('#') + 1;
                if (tInt > 0)
                {
                    test = _str.Substring(tInt);
                    tEvent = messageList[test];
                }
                msgString = tEvent.eventName;
                if (msgString == string.Empty)
                {
                    return null;
                }
                return tEvent;
            }
            return null;
        }
        public void callEvent(string _str)
        {
            GameEvent tEvent = null;
            GameEvent tEventMSG = currentEvent;
            string test = _str;
            test = _str.Replace("#", "");
            tEvent = messageList[test];

            if (tEvent != null)
            {
                currentEvent = tEvent;
                tEvent.alive = true;//currentEvent.on = true;
                StartCoroutine("decodeMSGS", tEvent.msgFilter);//currentEvent.on = false;
            }//return void;
            currentEvent = tEventMSG;
        }
        IEnumerator decodeMSGS(string _str)
        {
            GameEvent tEvent = currentEvent;
            GameEvent tEventMSG;
            string test = string.Empty;
            char[] delimiterComma = { ',', ':', '>' };
            if (_str.Length > 0)
            {
                string[] msgSplits = _str.Split(delimiterComma);
                for (int i = 0; i < msgSplits.Length; i++)
                {
                    currentEvent = decodeMSG(msgSplits[i]);
                    if (currentEvent.gameAction == GameFunctions.PAUSE && currentEvent.msgFilter == "eventdelay")
                    {
                        yield return new WaitForSeconds(currentEvent.amount);
                        continue;
                    }
                    switch (currentEvent.gameAction)
                    {
                        default:
                            doMSG(currentEvent.eventName);
                            break;
                    }
                    msgString = string.Empty;
                }
            }
            currentEvent = tEvent;
            yield return null;
            //return null;
        }

        /// <summary>
        /// Compares the elements listed into the objFilter box. 
        /// 
        /// '_' character ahead is used for explanation purposes; 
        /// do not use in actual message
        /// 	_event/private: 	//	Use event if comparing an event's variables; private for the GenBehavior's private members
        /// 	_Score1:			//	The event to use
        /// 	_vectorstoragex	//	The variable to use from even or private
        /// 	_@ScoreZoneLeft		//	The Object to read from
        /// 	
        /// </summary>
        #region CompareMachine
        void compareMachine()
        {

            GameEvent tEvent = currentEvent;
            string strLtyp = string.Empty;
            string strLevt = string.Empty;
            string strLmsg = string.Empty;
            string strLobj = string.Empty;
            string str2 = string.Empty;
            GameObject tGO = gameObject;

            char[] delimiterComma = { ',', '&', '|' };
            char[] delimiterMsgObj = { '@', ':' };
            string _str = tEvent.objFilter;
            string _str2 = tEvent.msgFilter;

            if (!tEvent.alive)
            {
                return;
            }

            if (_str.Length == 0 || _str2.Length == 0)
            {
                return;
            }

            //	Split the ObjName
            string[] objSplits = _str.Split(delimiterComma);

            //	Split the first (or only) ObjName into Command(probably accessPrivates():GameObjectToCall
            string[] objCommand = objSplits[0].Split(delimiterMsgObj);

            if (objCommand.Length <= 1)
            {
                Debug.Log("bad compare_data event: gameObject(" + gameObject.name + "),event: " + currentEvent.eventName);
                return;
            }
            Vector3 tStore = tEvent.vectorStorage;
            string leftIndex = string.Empty;
            string rightIndex = string.Empty;
            int comparisonType = 0;
            int resultValue = -1;

            strLtyp = objCommand[0];

            switch (strLtyp)
            {
                case "private":

                    //			strLevt = objCommand [1];
                    strLmsg = objCommand[1];
                    if (objCommand.Length > 1)
                    {
                        strLobj = objCommand[2];
                        if (strLobj == "gameObject")
                        {
                            tGO = gameObject;//objectAccess[objectAccessList[strLobj]];
                        }
                        else
                        {
                            tGO = objectAccess[objectAccessList[strLobj]];
                        }
                    }
                    else
                    {
                        strLobj = gameObject.name;
                        //tGO = objectAccess[objectAccessList[strLobj]];
                    }

                    switch (strLmsg)
                    {
                        case "floatindex":
                            tStore.x = tGO.GetComponent<GenericBehavior>().accessPrivateFloats(strLmsg);
                            tStore.y = tGO.GetComponent<GenericBehavior>().indexFloatMap((int)tEvent.amount, (int)tEvent.otherAmount);
                            break;
                        case "stringindex":
                            rightIndex = tGO.GetComponent<GenericBehavior>().indexStringMap((int)tEvent.amount, (int)tEvent.otherAmount);
                            leftIndex = tEvent.word;
                            comparisonType = 1;
                            break;
                        case "stringstorage":
                            Debug.Log(stringStorage);
                            rightIndex = tGO.GetComponent<GenericBehavior>().accessPrivateStrings(strLmsg);
                            leftIndex = tEvent.word;
                            comparisonType = 1;
                            break;
                        default:
                            tStore.x = tGO.GetComponent<GenericBehavior>().accessPrivateFloats(strLmsg);
                            tStore.y = tEvent.amount;
                            break;
                    }
                    break;
                case "event":
                    //			strLtyp = objCommand [0];
                    strLevt = objCommand[1];
                    strLmsg = objCommand[2];
                    if (objCommand.Length > 2)
                    {
                        strLobj = objCommand[3];
                        if (strLobj == "gameObject")
                        {
                            tGO = gameObject;//objectAccess[objectAccessList[strLobj]];
                        }
                        else
                        {
                            tGO = objectAccess[objectAccessList[strLobj]];
                        }
                    }
                    else
                    {
                        strLobj = gameObject.name;
                        //tGO = objectAccess[objectAccessList[strLobj]];
                    }
                    tStore.x = tGO.GetComponent<GenericBehavior>().accessEventFloats(strLevt, strLmsg);
                    switch (strLmsg)
                    {
                        case "mass":
                        case "force":
                        case "valuestorage":
                        case "maxvelmagnitude":
                        case "maxangmagnitude":
                        case "minvelmagnitude":
                        case "minangmagnitude":
                        case "globaltimescale":
                        case "colorR":
                        case "colorG":
                        case "colorB":
                        case "storedvelocityx":
                        case "storedvelocityy":
                        case "storedvelocityz":
                            tStore.x = tGO.GetComponent<GenericBehavior>().accessPrivateFloats(strLmsg);
                            //tStore.x = objectAccess [objectAccessList [strLobj]].GetComponent<GenericBehavior> ().accessEventFloats (strLevt, strLmsg);
                            tStore.y = tGO.GetComponent<GenericBehavior>().accessEventFloats(strLevt, "amount");
                            //tStore.y = tEvent.amount;
                            break;
                        case "amount":
                        case "otheramount":
                        case "vectorstoragex":
                        case "vectorstoragey":
                        case "vectorstoragez":
                            tStore.x = tGO.GetComponent<GenericBehavior>().accessEventFloats(strLevt, strLmsg);
                            tStore.y = tEvent.amount;
                            break;
                        case "floatindex":
                            tStore.y = tGO.GetComponent<GenericBehavior>().indexFloatMap((int)tEvent.amount, (int)tEvent.otherAmount);
                            break;
                        case "stringindex":
                            rightIndex = tGO.GetComponent<GenericBehavior>().indexStringMap((int)tEvent.amount, (int)tEvent.otherAmount);
                            comparisonType = 1;
                            break;
                        case "stringstorage":
                            rightIndex = tGO.GetComponent<GenericBehavior>().accessPrivateStrings(strLmsg);
                            leftIndex = tEvent.word;
                            comparisonType = 1;
                            break;
                        default:
                            tStore.y = tEvent.amount;
                            break;
                    }
                    break;
                case "floatindex":
                    strLtyp = objCommand[0];
                    //			strLevt = objCommand [1];
                    strLmsg = objCommand[1];
                    if (objCommand.Length > 1)
                    {
                        strLobj = objCommand[2];
                        if (strLobj == "gameObject")
                        {
                            tGO = gameObject;//objectAccess[objectAccessList[strLobj]];
                        }
                        else
                        {
                            tGO = objectAccess[objectAccessList[strLobj]];
                        }
                    }
                    else
                    {
                        strLobj = gameObject.name;
                        //tGO = objectAccess[objectAccessList[strLobj]];
                    }
                    tStore = tEvent.vectorStorage;
                    tStore.x = tGO.GetComponent<GenericBehavior>().indexFloatMap((int)tStore.x, (int)tStore.y);
                    switch (strLmsg)
                    {
                        case "mass":
                        case "force":
                        case "valuestorage":    //apply valuestorage to other variable;
                        case "maxvelmagnitude":
                        case "maxangmagnitude":
                        case "globaltimescale":
                        case "colorR":
                        case "colorG":
                        case "colorB":
                        case "storedvelocityx":
                        case "storedvelocityy":
                        case "storedvelocityz":
                            tStore.y = tGO.GetComponent<GenericBehavior>().accessPrivateFloats(strLmsg);
                            break;
                        case "amount":
                        case "otheramount":
                        case "vectorstoragex":
                        case "vectorstoragey":
                        case "vectorstoragez":
                            tStore.y = tGO.GetComponent<GenericBehavior>().accessEventFloats(strLevt, strLmsg);
                            break;
                        case "floatindex":
                            tStore.y = tGO.GetComponent<GenericBehavior>().indexFloatMap((int)tEvent.amount, (int)tEvent.otherAmount);
                            break;
                        case "stringindex":
                            tStore.y = tGO.GetComponent<GenericBehavior>().indexFloatMap((int)tEvent.amount, (int)tEvent.otherAmount);
                            break;
                        default:
                            tStore.y = tEvent.amount;
                            break;
                    }
                    break;
                case "stringindex":
                    strLtyp = objCommand[0];
                    //			strLevt = objCommand [1];
                    strLmsg = objCommand[1];
                    if (objCommand.Length > 1)
                    {
                        strLobj = objCommand[2];
                        if (strLobj == "gameObject")
                        {
                            tGO = gameObject;//objectAccess[objectAccessList[strLobj]];
                        }
                        else
                        {
                            tGO = objectAccess[objectAccessList[strLobj]];
                        }
                    }
                    else
                    {
                        strLobj = gameObject.name;
                        //tGO = objectAccess[objectAccessList[strLobj]];
                    }
                    tStore = tEvent.vectorStorage;
                    //leftIndex = objectAccess [objectAccessList [strLobj]].GetComponent<GenericBehavior> ().indexStringMap ((int)tEvent.amount,(int)tEvent.otherAmount);
                    leftIndex = tGO.GetComponent<GenericBehavior>().indexStringMap((int)tStore.x, (int)tStore.y);
                    comparisonType = 1;
                    break;

            }

            switch (comparisonType)
            {
                case 0:
                    resultValue = compareFloats(tStore);
                    break;
                case 1:
                    resultValue = compareString(leftIndex, rightIndex, tEvent.word);
                    break;
                default:
                    resultValue = compareFloats(tStore);
                    break;
            }


            if (resultValue == 0)
            {
                StartCoroutine("decodeMSGS", tEvent.msgFilter);
            }

            currentEvent = tEvent;

            if (str2 == string.Empty)
                return;

        }

        #endregion

        #region Compare floats

        int compareFloats(Vector3 info)
        {

            int result = -1;

            switch ((int)info.z)
            {
                case 0:// Equal
                    if (info.x == info.y)
                    {
                        result = 0;
                    }
                    break;
                case 1:// NOTEqual
                    if (info.x != info.y)
                    {
                        result = 0;
                    }
                    break;
                case 2:// GreaterThan
                    if (info.x > info.y)
                    {
                        result = 0;
                    }
                    else if (info.x < info.y)
                    {
                        result = 1;
                    }
                    else
                        result = -1;
                    break;
                case 3:// GreaterThanOrEqual
                    if (info.x >= info.y)
                    {
                        result = 0;
                    }
                    else if (info.x < info.y)
                    {
                        result = -1;
                    }
                    break;
                case 4:// LesserThan
                    if (info.x < info.y)
                    {
                        result = 0;
                    }
                    else if (info.x > info.y)
                    {
                        result = 1;
                    }
                    else
                        result = -1;
                    break;
                case 5:// LesserThanOrEqual
                    if (info.x <= info.y)
                    {
                        result = 0;
                    }
                    else if (info.x > info.y)
                    {
                        result = -1;
                    }
                    break;

            }

            return result;
        }

        #endregion

        #region Compare Strings

        int compareString(string x, string y, string _str)
        {

            int result = -1;

            switch ((int)currentEvent.vectorStorage.z)
            {
                case 0:// Equal
                    if (x == y)
                    {
                        result = 0;
                    }
                    break;
                case 1:// NOTEqual
                    if (x != y)
                    {
                        result = 0;
                    }
                    break;
                case 2:// GreaterThan
                    if (x.Length > y.Length)
                    {
                        result = 0;
                    }
                    else if (x.Length < y.Length)
                    {
                        result = 1;
                    }
                    else
                        result = -1;
                    break;
                case 3:// GreaterThanOrEqual
                    if (x.Length >= y.Length)
                    {
                        result = 0;
                    }
                    else if (x.Length < y.Length)
                    {
                        result = -1;
                    }
                    break;
                case 4:// LesserThan
                    if (x.Length < y.Length)
                    {
                        result = 0;
                    }
                    else if (x.Length > y.Length)
                    {
                        result = 1;
                    }
                    else
                        result = -1;
                    break;
                case 5:// LesserThanOrEqual
                    if (x.Length <= y.Length)
                    {
                        result = 0;
                    }
                    else if (x.Length > y.Length)
                    {
                        result = -1;
                    }
                    break;

            }

            return result;
        }

        #endregion


        int compare()
        {

            int result = -1;

            switch ((int)currentEvent.vectorStorage.z)
            {
                case 0:// Equal
                    if (currentEvent.vectorStorage.x == currentEvent.vectorStorage.y)
                    {
                        result = 0;
                    }
                    break;
                case 1:// NOTEqual
                    if (currentEvent.vectorStorage.x != currentEvent.vectorStorage.y)
                    {
                        result = 0;
                    }
                    break;
                case 2:// GreaterThan
                    if (currentEvent.vectorStorage.x > currentEvent.vectorStorage.y)
                    {
                        result = 0;
                    }
                    else if (currentEvent.vectorStorage.x < currentEvent.vectorStorage.y)
                    {
                        result = 1;
                    }
                    else
                        result = -1;
                    break;
                case 3:// GreaterThanOrEqual
                    if (currentEvent.vectorStorage.x >= currentEvent.vectorStorage.y)
                    {
                        result = 0;
                    }
                    else if (currentEvent.vectorStorage.x < currentEvent.vectorStorage.y)
                    {
                        result = -1;
                    }
                    break;
                case 4:// LesserThan
                    if (currentEvent.vectorStorage.x < currentEvent.vectorStorage.y)
                    {
                        result = 0;
                    }
                    else if (currentEvent.vectorStorage.x > currentEvent.vectorStorage.y)
                    {
                        result = 1;
                    }
                    else
                        result = -1;
                    break;
                case 5:// LesserThanOrEqual
                    if (currentEvent.vectorStorage.x <= currentEvent.vectorStorage.y)
                    {
                        result = 0;
                    }
                    else if (currentEvent.vectorStorage.x > currentEvent.vectorStorage.y)
                    {
                        result = -1;
                    }
                    break;

            }

            return result;
        }

        //SETTING AMOUNT TO -1 DOES IT REPEATEDLY LIKE A LOOP
        void timer()
        {

            GameEvent tempEvent = currentEvent;
            if (tempEvent.vectorStorage.x + Mathf.Epsilon > 0)
            {

                if (tempEvent.on == false)
                {
                    if (tempEvent.amount == 0)
                    {
                        tempEvent.on = false;
                        return;
                    }
                }
                currentEvent.on = false;
                ///	If you want the timer to run independently from the game being paused use
                switch ((int)tempEvent.otherAmount)
                {
                    case 1:
                        tempEvent.vectorStorage.x -= Time.unscaledDeltaTime;
                        break;
                    case 2:
                        tempEvent.vectorStorage.x -= Time.deltaTime;
                        break;
                    case 0:
                    default:
                        if (Time.realtimeSinceStartup >= (timeStamp + tempEvent.vectorStorage.z))
                        {
                            if (tempEvent.vectorStorage.z == 0)
                            {
                                tempEvent.vectorStorage.z = Time.deltaTime;
                            }
                            tempEvent.vectorStorage.x -= tempEvent.vectorStorage.z;
                            timeStamp = Time.realtimeSinceStartup;

                        }
                        break;
                }

            }
            else
            {

                tempEvent.on = true;
                //decodeMSGS(tempEvent.msgFilter);
                //StartCoroutine("decodeMSGS", tempEvent.msgFilter);
                tempEvent.vectorStorage.x = tempEvent.vectorStorage.y;

                ///REPEATS THE NUMBER OF TIMES SAID IN AMOUNT
                ///	-1	- Does it infinitelly
                /// 0 	- Does it ONCE
                /// n 	- Does it n times
                if (tempEvent.amount <= -1)
                {
                    StartCoroutine("decodeMSGS", tempEvent.msgFilter);
                    return;
                }

                if (Mathf.Abs(tempEvent.amount + Mathf.Epsilon) > 0)
                {
                    tempEvent.amount--;
                    tempEvent.on = true;
                    if (tempEvent.amount < 0)
                    {
                        tempEvent.amount = 0;
                        return;
                    }
                }
                else
                {
                    tempEvent.on = false;
                    return;
                }
                StartCoroutine("decodeMSGS", tempEvent.msgFilter);

            }

            currentEvent = tempEvent;

        }


        #region Pause

        void pause()
        {

            GameEvent tempEvent = currentEvent;

            if (!tempEvent.alive)
            {
                return;
            }

            switch (tempEvent.msgFilter)
            {
                case "eventdelay":
                    //StopCoroutine("waitTime");
                    //continueBool = false;
                    //StartCoroutine("waitTime", 5);
                    //Debug.Log("waiting:" + continueBool.ToString());

                    //waitTime(tempEvent.amount);

                    break;
                default:
                    switch (tempEvent.playerAction)
                    {
                        case PlayerFunctions.START:
                        case PlayerFunctions.COLLISIONENTER:
                        case PlayerFunctions.TRIGGERENTER:
                        case PlayerFunctions.MESSAGE_EVENT:
                        case PlayerFunctions.INPUTPRESS:
                        case PlayerFunctions.INPUTDOWN:
                        case PlayerFunctions.INPUTANY:
                        case PlayerFunctions.INPUTUP:

                            switch (tempEvent.relation)
                            {

                                case ObjectRelation.ME: //PAUSE INDEFINITELY
                                    if (objectPause == false)
                                    {
                                        objectPause = true;//!currentEvent.on;
                                    }
                                    else
                                    { //if (currentEvent.on == false)

                                        objectPause = false;

                                    }

                                    break;
                                case ObjectRelation.ALL: //PAUSE INDEFINITELY
                                default:
                                    if (tempEvent.amount == -1)
                                    {
                                        Time.timeScale = Mathf.Abs(Time.timeScale + tempEvent.amount);
                                        globalTimeScale = Time.timeScale;

                                    }
                                    else
                                    {
                                        Time.timeScale = Mathf.Abs(tempEvent.amount);//globalTimeScale;
                                        globalTimeScale = Time.timeScale;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
            }


            currentEvent = tempEvent;
        }

        #endregion

        #region Score Behavior

        void register()
        {

            char[] delimiterComma = { ',' };

            string[] msgSplits = currentEvent.msgFilter.Split(delimiterComma);
            GameObject go = gameObject;

            if (currentEvent.objFilter.Length > 0)
            {
                go = objectAccess[objectAccessList[currentEvent.objFilter]];
            }

            for (int j = 0; j < msgSplits.Length; j++)
            {
                //	Store the value in as many places listed in the msgFilter Field for this event
                switch (msgSplits[j])
                {
                    case "amount":
                        currentEvent.amount += currentEvent.amount;
                        break;
                    case "vectorstoragex":
                        currentEvent.vectorStorage.x += currentEvent.amount;
                        break;
                    case "vectorstoragey":
                        currentEvent.vectorStorage.y += currentEvent.amount;
                        break;
                    case "vectorstoragez":
                        currentEvent.vectorStorage.z += currentEvent.amount;
                        break;
                    case "otheramount":
                        currentEvent.otherAmount += currentEvent.amount;
                        break;
                    case "score":
                    case "scoremechanic":
                        go.SendMessage("Play", currentEvent.amount);
                        break;
                    case "registeranimation.bool":
                        go.SendMessage("animation", "bool." + currentEvent.word + "." + (currentEvent.amount > 0 ? "true" : "false"));
                        break;
                    case "registeranimation.float":
                        go.SendMessage("animation", "float." + currentEvent.word + "." + currentEvent.amount.ToString());
                        break;
                    case "registeranimation.int":
                        go.SendMessage("animation", "int." + currentEvent.word + "." + ((int)currentEvent.amount).ToString());
                        break;
                    case "registeranimation.trigger":
                        go.SendMessage("animation", "trigger." + currentEvent.word + "." + ((int)currentEvent.amount).ToString());
                        break;
                    case "valuestorage":
                    case "":
                    default:
                        go.GetComponent<GenericBehavior>().valueStorage += currentEvent.amount;
                        break;
                }//	Switch ...msgFilter
            }//	For int j

        }

        #endregion

        #region IF if_event_on()

        void if_event_on()
        {
            char[] delimiterEqual = { '=' };
            char[] delimiterComma = { ',' };

            string[] msgSplit = currentEvent.msgFilter.Split(delimiterEqual);
            string[] msgSplitsChecks = msgSplit[0].Split(delimiterComma);
            string[] msgSplitsResults = msgSplit[1].Split(delimiterComma);

            for (int i = 0; i < msgSplitsChecks.Length; i++)
            {
                int tInt = msgSplitsChecks[i].IndexOf('#') + 1;

                if (tInt > 0)
                {
                    msgSplitsChecks[i] = msgSplitsChecks[i].Substring(tInt);
                }

                if (!messageList[msgSplitsChecks[i]].on)
                {

                    return;
                }
            }

            for (int j = 0; j < msgSplitsResults.Length; j++)
            {
                decodeMSG(msgSplitsResults[j]);
            }

        }

        #endregion

        IEnumerator waitTime(float _seconds = 0.0f)
        {
            //while (!continueBool)
            //			while (true)
            //			{
            yield return new WaitForSeconds(_seconds);
            //continueBool = true;
            Debug.Log(_seconds + " passed");
            //			}
            yield return null;
        }

        /// <summary>
        ///	Get information from one place (event/privates/files/indexes) to 
        ///	another place.
        /// </summary>
        #region GET Game Action
        void get()
        {
            GameEvent tEvent = currentEvent;

            if (!tEvent.alive)
            {
                return;
            }

            string _str = tEvent.objFilter;

            if (_str.Length == 0)
            {
                return;
            }

            char[] delimiterInto = { '>' };
            char[] delimiterMsgObj = { '@', ':' };

            string strFromTyp = string.Empty;
            string strFromEvt = string.Empty;
            string strFromMsg = string.Empty;
            string strFromObj = string.Empty;
            string strIntoTyp = string.Empty;
            string strIntoEvt = string.Empty;
            string strIntoMsg = string.Empty;
            string strIntoObj = string.Empty;
            Vector3 tStore = currentEvent.vectorStorage;
            bool tryparseFlag = true;

            string[] fetchPhrases = tEvent.objFilter.Split(delimiterInto);
            string[] phrase1 = fetchPhrases[0].Split(delimiterMsgObj);
            string[] phrase2 = fetchPhrases[1].Split(delimiterMsgObj);

            //			string leftIndex = string.Empty;
            //			string rightIndex = string.Empty;
            int dataType = 0;
            float resultValue = -1;
            string resultString = string.Empty;

            strFromTyp = phrase1[0];
            strIntoTyp = phrase2[0];

            switch (currentEvent.msgFilter)
            {
                case "date.full":
                    resultString = System.DateTime.Now.Date.ToString();
                    dataType = 1;
                    break;
                case "date.day":
                    resultString = System.DateTime.Now.Day.ToString();
                    dataType = 1;
                    break;
                case "date.weekday":
                    resultString = System.DateTime.Now.DayOfWeek.ToString();
                    dataType = 1;
                    break;
                case "date.dayofyear":
                    resultString = System.DateTime.Now.DayOfYear.ToString();
                    dataType = 1;
                    break;
                case "time.hour":
                    resultString = System.DateTime.Now.Hour.ToString();
                    dataType = 1;
                    break;
                case "time.minute":
                    resultString = System.DateTime.Now.Minute.ToString();
                    dataType = 1;
                    break;
                case "time.seconds":
                    resultString = System.DateTime.Now.Second.ToString();
                    dataType = 1;
                    break;
                case "time.full":
                    resultString = System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();
                    dataType = 1;
                    break;
                case "time.millisecond":
                    resultString = System.DateTime.Now.Millisecond.ToString();
                    dataType = 1;
                    break;
                case "input":
                case "velocity":
                case "mass":
                case "vectors":
                //case "joypad.axis":
                case "axis":
                    break;
                case "other":
                    break;
                case "findgameobject":
                    GameObject temp = GameObject.Find(currentEvent.objFilter);
                    GameObject[] tempArray = new GameObject[objectAccess.Length + 1];
                    for (int i = 0; i < objectAccess.Length; i++)
                    {
                        tempArray[i] = objectAccess[i];
                    }
                    tempArray[objectAccess.Length] = temp;
                    objectAccess = tempArray;
                    break;
                case "data":
                    break;
            }

            switch (strFromTyp)
            {

                case "vectors":
                    if (phrase1.Length > 3)
                    {
                        strFromEvt = phrase1[1];
                        strFromMsg = phrase1[2];
                        strFromObj = phrase1[3];
                    }
                    else
                    {
                        strFromMsg = phrase1[1];
                        strFromObj = phrase1[2];//gameObject.name;
                    }
                    //					Debug.Log("OBJ:" + strFromObj + ",MSG:" + strFromMsg + ",EVT:" + strFromEvt + ",:" + tStore);
                    switch (strFromMsg)
                    {
                        case "transform_position":
                            tStore = objectAccess[objectAccessList[strFromObj]].transform.position;
                            dataType = 2;
                            break;
                        case "transform_localposition":
                            tStore = objectAccess[objectAccessList[strFromObj]].transform.localPosition;
                            dataType = 2;
                            break;
                        case "transform_rotation":
                            tStore = objectAccess[objectAccessList[strFromObj]].transform.eulerAngles;
                            dataType = 2;
                            break;
                        case "transform_localrotation":
                            tStore = objectAccess[objectAccessList[strFromObj]].transform.localRotation.eulerAngles;
                            dataType = 2;
                            break;
                        case "transform_scale":
                            tStore = objectAccess[objectAccessList[strFromObj]].transform.localScale;
                            dataType = 2;
                            break;
                        case "vectorstorage":
                            tStore.x = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessEventFloats(strFromEvt, "vectorstoragex");
                            tStore.y = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessEventFloats(strFromEvt, "vectorstoragey");
                            tStore.z = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessEventFloats(strFromEvt, "vectorstoragez");
                            //objectAccess[objectAccessList[strFromObj]].= tStore;
                            //							Debug.Log(tStore);
                            dataType = 2;
                            break;
                        default:
                            resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strFromMsg);
                            break;
                    }

                    //					Debug.Log(tStore);
                    break;
                case "axis":
                    if (phrase1.Length > 3)
                    {
                        strFromEvt = phrase1[1];
                        strFromMsg = phrase1[2];
                        strFromObj = phrase1[3];
                    }
                    else
                    {
                        strFromMsg = phrase1[1];
                        strFromObj = phrase1[2];//gameObject.name;
                    }
                    //					Debug.Log("OBJ:" + strFromObj + ",MSG:" + strFromMsg + ",EVT:" + strFromEvt + ",:" + tStore);
                    switch (strFromMsg)
                    {
                        case "joypad":
                            resultValue = Input.GetAxis(phrase1[1]);
                            break;
                        case "mouse":
                            resultValue = Input.GetAxis(phrase1[1]);
                            break;
                        default:
                            //resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strFromMsg);
                            break;
                    }
                    break;
                case "private":
                    strFromMsg = phrase1[1];
                    if (phrase1.Length > 1)
                    {
                        strFromObj = phrase1[2];
                    }
                    else
                    {
                        strFromObj = gameObject.name;
                    }

                    switch (strFromMsg)
                    {
                        case "floatindex":
                            resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().indexFloatMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount);
                            break;
                        case "stringindex":
                            resultString = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().indexStringMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount);
                            dataType = 1;
                            break;
                        case "stringstorage":
                            resultString = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessPrivateStrings(strFromMsg);
                            dataType = 1;
                            break;
                        default:
                            resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strFromMsg);
                            break;
                    }
                    break;
                case "event":
                    strFromEvt = phrase1[1];
                    strFromMsg = phrase1[2];
                    if (phrase1.Length > 2)
                    {
                        strFromObj = phrase1[3];
                    }
                    else
                    {
                        strFromObj = gameObject.name;
                    }
                    //				tStore.x = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessEventFloats(strFromEvt, strFromMsg);
                    switch (strFromMsg)
                    {

                        case "amount":
                        case "otheramount":
                        case "vectorstoragex":
                        case "vectorstoragey":
                        case "vectorstoragez":
                            resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessEventFloats(strFromEvt, strFromMsg);
                            break;
                        case "word":
                        case "string":
                        case "stringstorage":
                            resultString = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessEventStrings(strFromEvt, strFromMsg);
                            dataType = 1;
                            break;
                        default:
                            break;
                    }
                    break;
                case "floatindex":
                    strFromTyp = phrase1[0];
                    //			strLevt = objCommand [1];
                    strFromMsg = phrase1[1];
                    if (phrase1.Length > 1)
                    {
                        strFromObj = phrase1[2];
                    }
                    else
                    {
                        strFromObj = gameObject.name;
                    }
                    tStore = tEvent.vectorStorage;
                    resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().indexFloatMap((int)tStore.x, (int)tStore.y);

                    break;
                case "stringindex":
                    strFromTyp = phrase1[0];
                    //			strLevt = objCommand [1];
                    strFromMsg = phrase1[1];
                    if (phrase1.Length > 1)
                    {
                        strFromObj = phrase1[2];
                    }
                    else
                    {
                        strFromObj = gameObject.name;
                    }
                    tStore = tEvent.vectorStorage;
                    resultString = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().indexStringMap((int)tStore.x, (int)tStore.y);
                    dataType = 1;
                    break;

            }


            float resultTmp = resultValue;
            /// Place Values into
            switch (strIntoTyp)
            {
                case "vectors":
                    if (phrase2.Length > 3)
                    {
                        strIntoEvt = phrase2[1];
                        strIntoMsg = phrase2[2];
                        strIntoObj = phrase2[3];
                    }
                    else
                    {
                        strIntoMsg = phrase2[1];
                        strIntoObj = phrase2[2];//gameObject.name;
                    }
                    //					Debug.Log(strIntoObj + ":" + strIntoMsg + ":" + strIntoEvt + ":" + tStore);
                    switch (strIntoMsg)
                    {
                        case "transform_position":
                            objectAccess[objectAccessList[strIntoObj]].transform.position = tStore;
                            dataType = 2;
                            break;
                        case "transform_localposition":
                            objectAccess[objectAccessList[strIntoObj]].transform.localPosition = tStore;
                            dataType = 2;
                            break;
                        case "transform_rotation":
                            objectAccess[objectAccessList[strIntoObj]].transform.eulerAngles = tStore;
                            dataType = 2;
                            break;
                        case "transform_localrotation":
                            objectAccess[objectAccessList[strIntoObj]].transform.eulerAngles = tStore;
                            dataType = 2;
                            break;
                        case "transform_scale":
                            objectAccess[objectAccessList[strIntoObj]].transform.localScale = tStore;
                            dataType = 2;
                            break;
                        case "vectorstorage":
                            //							Debug.Log(strIntoObj + strIntoEvt + tStore);

                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventFloats(strIntoEvt, "vectorstoragex", tStore.x, true);
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventFloats(strIntoEvt, "vectorstoragey", tStore.y, true);
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventFloats(strIntoEvt, "vectorstoragez", tStore.z, true);
                            //objectAccess[objectAccessList[strFromObj]].= tStore;
                            dataType = 2;
                            break;
                        default:
                            resultValue = objectAccess[objectAccessList[strFromObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strFromMsg);
                            break;
                    }
                    break;
                case "private":
                    strIntoMsg = phrase2[1];
                    if (phrase2.Length > 1)
                    {
                        strIntoObj = phrase2[2];
                    }
                    else
                    {
                        strIntoObj = gameObject.name;
                    }
                    switch (strIntoMsg)
                    {
                        case "floatindex":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexFloatMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "stringindex":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexStringMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            break;
                        case "stringstorage":

                            tryparseFlag = float.TryParse(resultString, out resultValue);
                            if (tryparseFlag)
                            {
                                resultValue = resultTmp;
                            }
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessPrivateStrings(strIntoMsg, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            break;
                        default:
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strIntoMsg, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                    }
                    break;
                case "event":
                    strIntoEvt = phrase2[1];
                    strIntoMsg = phrase2[2];
                    if (phrase2.Length > 2)
                    {
                        strIntoObj = phrase2[3];
                    }
                    else
                    {
                        strIntoObj = gameObject.name;
                    }
                    //tStore.x = objectAccess[objectAccessList[strLobj]].GetComponent<GenericBehavior>().accessEventFloats(strLevt, strLmsg);
                    switch (strIntoMsg)
                    {
                        case "mass":
                        case "force":
                        case "valuestorage":
                        case "maxvelmagnitude":
                        case "maxangmagnitude":
                        case "minvelmagnitude":
                        case "minangmagnitude":
                        case "globaltimescale":
                        case "colorR":
                        case "colorG":
                        case "colorB":
                        case "storedvelocityx":
                        case "storedvelocityy":
                        case "storedvelocityz":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strIntoMsg, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "amount":
                        case "otheramount":
                        case "vectorstoragex":
                        case "vectorstoragey":
                        case "vectorstoragez":
                            //resultString = float.TryParse(resultString, resultValue);

                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventFloats(strIntoEvt, strIntoMsg, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "floatindex":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexFloatMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "stringindex":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexStringMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            break;
                        case "word":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventStrings(strIntoEvt, strIntoMsg, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            break;
                        case "string":
                            resultString = (dataType == 0 ? resultValue.ToString() : resultString);
                            //resultString = resultString.Substring(0, 1);
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventStrings(strIntoEvt, strIntoMsg, resultString, true);
                            break;
                            //						case "stringstorage":
                            //							objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventStrings(strIntoEvt, strIntoMsg, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            //							break;
                            //					default:
                            //						tStore.y = currentEvent.amount;
                            //						break;
                    }
                    break;
                case "floatindex":
                    strIntoTyp = phrase2[0];
                    //			strLevt = objCommand [1];
                    strIntoMsg = phrase2[1];
                    if (phrase2.Length > 1)
                    {
                        strIntoObj = phrase2[2];
                    }
                    else
                    {
                        strIntoObj = gameObject.name;
                    }
                    tStore = tEvent.vectorStorage;
                    tStore.x = objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexFloatMap((int)tStore.x, (int)tStore.y);
                    switch (strIntoMsg)
                    {
                        case "mass":
                        case "force":
                        case "valuestorage":    //apply valuestorage to other variable;
                        case "maxvelmagnitude":
                        case "maxangmagnitude":
                        case "minvelmagnitude":
                        case "minangmagnitude":
                        case "globaltimescale":
                        case "colorR":
                        case "colorG":
                        case "colorB":
                        case "storedvelocityx":
                        case "storedvelocityy":
                        case "storedvelocityz":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessPrivateFloats(strIntoMsg, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "amount":
                        case "otheramount":
                        case "vectorstoragex":
                        case "vectorstoragey":
                        case "vectorstoragez":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessEventFloats(strIntoEvt, strIntoMsg, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "floatindex":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexFloatMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue : float.Parse(resultString)), true);
                            break;
                        case "stringindex":
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexStringMap(
                                (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            break;
                        default:
                            objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().accessPrivateStrings(strIntoMsg, (dataType == 0 ? resultValue.ToString() : resultString), true);
                            break;
                    }
                    break;
                case "stringindex":
                    strIntoTyp = phrase2[0];
                    //			strLevt = objCommand [1];
                    strIntoMsg = phrase2[1];
                    if (phrase2.Length > 1)
                    {
                        strIntoObj = phrase2[2];
                    }
                    else
                    {
                        strIntoObj = gameObject.name;
                    }
                    tStore = tEvent.vectorStorage;
                    objectAccess[objectAccessList[strIntoObj]].GetComponent<GenericBehavior>().indexStringMap(
                        (int)currentEvent.amount, (int)currentEvent.otherAmount, (dataType == 0 ? resultValue.ToString() : resultString), true);
                    break;

            }

        }

        #endregion

        #region KEYS

        void OnMouseDown()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (canInteract == false)
                    return;
                if (currentEvent.playerAction != PlayerFunctions.INPUTDOWN)
                    return;
                string[] phrase1 = currentEvent.word.Split(':');
                switch (phrase1[0])
                {
                    case "detectOnMouseDown:":
                        if (Input.GetMouseButtonDown(Convert.ToInt32(phrase1[1])))
                        {
                            currentEvent.on = true;
                            customEventList[currentEvent.eventName].Invoke();
                        }
                        else
                        {
                            currentEvent.on = false;
                        }
                        return;
                        break;

                    default:
                        break;
                }

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnMouseEnter()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {
                if (systemEventList.Count > 0)
                {
                    List<int> deleteList = new List<int>();
                    int index = -1;
                    foreach (GameEvent systemGameEvent in systemEventList)
                    {
                        index++;
                        if (systemGameEvent.word != "mouseenter" || systemGameEvent.alive == false)
                            break;
                        GameEvent temporaryStoredEvent = currentEvent;
                        currentEvent = systemGameEvent;
                        customEventList[systemGameEvent.eventName].Invoke();
                        currentEvent = temporaryStoredEvent;
                        deleteList.Add(index);
                    }
                    foreach (int intIndex in deleteList)
                    {
                        systemEventList.RemoveAt(intIndex);
                    }
                }
                return;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnMouseExit()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (systemEventList.Count > 0)
                {
                    List<int> deleteList = new List<int>();
                    int index = -1;
                    foreach (GameEvent systemGameEvent in systemEventList)
                    {
                        index++;
                        if (systemGameEvent.word != "mouseexit" || systemGameEvent.alive == false)
                            break;

                        GameEvent temporaryStoredEvent = currentEvent;
                        currentEvent = systemGameEvent;
                        customEventList[systemGameEvent.eventName].Invoke();
                        currentEvent = temporaryStoredEvent;
                        deleteList.Add(index);
                    }
                    foreach (int intIndex in deleteList)
                    {
                        systemEventList.RemoveAt(intIndex);
                    }
                }
                return;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnMouseOver()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (systemEventList.Count > 0)
                {
                    List<int> deleteList = new List<int>();
                    int index = -1;
                    foreach (GameEvent systemGameEvent in systemEventList)
                    {
                        index++;
                        if (systemGameEvent.word != "mouseover" || systemGameEvent.alive == false)
                            break;

                        GameEvent temporaryStoredEvent = currentEvent;
                        currentEvent = systemGameEvent;
                        customEventList[systemGameEvent.eventName].Invoke();
                        currentEvent = temporaryStoredEvent;
                        deleteList.Add(index);
                    }
                    foreach (int intIndex in deleteList)
                    {
                        systemEventList.RemoveAt(intIndex);
                    }
                }
                return;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnMouseDrag()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (canInteract == false)
                    return;
                if (currentEvent.playerAction != PlayerFunctions.INPUTPRESS)
                    return;

                //			Debug.Log("asdf");
                string[] phrase1 = currentEvent.word.Split(':');
                switch (phrase1[0])
                {
                    case "detectOnMouseDrag:":
                        if (Input.GetMouseButton(Convert.ToInt32(phrase1[1])))
                        {
                            currentEvent.on = true;
                            customEventList[currentEvent.eventName].Invoke();
                        }
                        else
                        {
                            currentEvent.on = false;
                        }
                        return;
                        break;

                    default:
                        break;
                }

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnMouseUp()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (canInteract == false)
                    return;
                if (currentEvent.playerAction != PlayerFunctions.INPUTUP)
                    return;
                string[] phrase1 = currentEvent.word.Split(':');
                switch (phrase1[0])
                {
                    case "detectOnMouseUp:":
                        if (Input.GetMouseButtonUp(Convert.ToInt32(phrase1[1])))
                        {
                            currentEvent.on = true;
                            customEventList[currentEvent.eventName].Invoke();
                        }
                        else
                        {
                            currentEvent.on = false;
                        }
                        return;
                        break;

                    default:
                        break;
                }

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        bool keypress()
        {
            if (canInteract == false)
                return false;

            //string[] phrase1 = currentEvent.word.Split(':');

            //switch (phrase1[0])
            switch (currentEvent.inputType)
            {
                //case "mousebutton":
                case InteractionType.JOYPAD:

                    //if (Input.GetMouseButton(Convert.ToInt32(phrase1[1])))
                    if (Input.GetButton(currentEvent.inputString))
                    {
                        currentEvent.on = true;
                        customEventList[currentEvent.eventName].Invoke();
                    }
                    else
                    {
                        currentEvent.on = false;
                    }
                    return true;
                    break;
                case InteractionType.MOUSE:

                    //if (Input.GetMouseButton(Convert.ToInt32(phrase1[1])))
                    if (Input.GetMouseButton(Convert.ToInt32(currentEvent.inputString)))
                    {
                        currentEvent.on = true;
                        customEventList[currentEvent.eventName].Invoke();
                    }
                    else
                    {
                        currentEvent.on = false;
                    }
                    return true;
                    break;
                //case "keyboard":
                case InteractionType.KEYBOARD:
                default:
                    if (Input.GetKey(currentEvent.key))
                    {
                        currentEvent.on = true;
                        customEventList[currentEvent.eventName].Invoke();
                    }
                    else
                    {
                        currentEvent.on = false;
                    }
                    return true;
                    break;
            }


        }

        bool keydown()
        {

            if (canInteract == false)
                return false;
            switch (currentEvent.inputType)
            {
                case InteractionType.JOYPAD:

                    //if (Input.GetMouseButton(Convert.ToInt32(phrase1[1])))
                    if (Input.GetButtonDown(currentEvent.inputString))
                    {
                        currentEvent.on = true;
                        customEventList[currentEvent.eventName].Invoke();
                    }
                    else
                    {
                        currentEvent.on = false;
                    }
                    return true;
                    break;
                case InteractionType.MOUSE:
                    if (Input.GetMouseButtonDown(Convert.ToInt32(currentEvent.inputString)))
                    {
                        customEventList[currentEvent.eventName].Invoke();
                        currentEvent.on = true;
                    }
                    if (Input.GetMouseButtonUp(Convert.ToInt32(currentEvent.inputString)))
                    {

                        currentEvent.on = false;
                    }
                    break;
                case InteractionType.KEYBOARD:
                default:
                    if (Input.GetKeyDown(currentEvent.key))
                    {
                        customEventList[currentEvent.eventName].Invoke();
                        currentEvent.on = true;

                    }
                    if (Input.GetKeyUp(currentEvent.key))
                    {

                        currentEvent.on = false;
                    }
                    break;
            }
            return true;
        }

        bool unKeypress()
        {

            if (canInteract == false)
                return false;
            switch (currentEvent.inputType)
            {
                case InteractionType.JOYPAD:

                    //if (Input.GetMouseButton(Convert.ToInt32(phrase1[1])))
                    if (Input.GetButtonUp(currentEvent.inputString))
                    {
                        currentEvent.on = true;
                        customEventList[currentEvent.eventName].Invoke();
                    }
                    else
                    {
                        currentEvent.on = false;
                    }
                    return true;
                    break;
                case InteractionType.MOUSE:
                    currentEvent.on = false;
                    if (Input.GetMouseButtonUp(Convert.ToInt32(currentEvent.inputString)))
                    {
                        customEventList[currentEvent.eventName].Invoke();
                        currentEvent.on = true;
                    }
                    break;
                case InteractionType.KEYBOARD:
                default:
                    if (Input.GetKeyUp(currentEvent.key))
                    {
                        currentEvent.on = false;

                        if (currentEvent.playerAction == PlayerFunctions.INPUTUP)
                        {
                            currentEvent.on = true;
                            customEventList[currentEvent.eventName].Invoke();
                            //	customEventList[currentEvent.eventName].Invoke();
                        }

                    }
                    break;
            }
            return true;
        }

        bool unKeydown()
        {

            if (canInteract == false)
                return false;
            switch (currentEvent.inputType)
            {

                case InteractionType.KEYBOARD:
                default:
                    if (Input.GetKeyDown(currentEvent.key))
                    {
                        currentEvent.on = false;
                        if (currentEvent.playerAction == PlayerFunctions.INPUTUP)
                        {
                            customEventList[currentEvent.eventName].Invoke();
                        }
                    }
                    break;
            }
            return true;
        }

        #endregion

        GameObject returnGO(GameEvent _ne, string str)
        {

            if (_ne.objFilter.Length < 1)
            {
                return null;
            }
            else if (!objectAccessList.ContainsKey(_ne.objFilter))
            {
                return null;
            }

            return objectAccess[objectAccessList[_ne.objFilter]];


        }

        /// <summary>
        /// Todo:
        /// 	1(clean out all memory objects and arrays)
        /// </summary>
        #region DESTROY GAMEACTION

        void destroy()
        {
            GameObject other;
            GameObject[] allObjects;

            switch (currentEvent.relation)
            {
                case ObjectRelation.IT://The other gameobject
                    if (currentEvent.objFilter.Length > 0)
                    {
                        Destroy(objectAccess[objectAccessList[currentEvent.objFilter]]);
                    }
                    break;
                case ObjectRelation.ALL:
                    foreach (GameObject go in GameObject.FindGameObjectsWithTag(currentEvent.objFilter))
                    {
                        Destroy(go);
                    }
                    break;
                case ObjectRelation.BOTH:
                    Destroy(objectAccess[objectAccessList[currentEvent.objFilter]]);
                    Destroy(gameObject);
                    break;
                case ObjectRelation.ALL_EXCEPT:
                    if (currentEvent.objFilter == string.Empty || currentEvent.msgFilter == string.Empty)
                    {
                        return;
                    }
                    other = GameObject.Find(currentEvent.objFilter);
                    allObjects = GameObject.FindGameObjectsWithTag(currentEvent.msgFilter);
                    foreach (GameObject go in allObjects)
                    {
                        if (go.activeInHierarchy)
                        {
                            if (go.name == other.name || go.name == gameObject.name)
                            {
                                continue;
                            }
                            Destroy(go);
                        }
                    }
                    if (other.tag == gameObject.name)
                    {
                        Destroy(other);
                    }
                    break;
                case ObjectRelation.ME://This one
                default:
                    Destroy(gameObject);
                    break;
            }

            globalID--;
            if (globalID < 0)
            {
                globalID = 0;
            }
        }

        #endregion

        void OnDestroy()
        {

        }

        void OnEnable()
        {

        }

        void OnDisable()
        {

        }

        #region MOTION GAMEACTION

        private IEnumerator motionCoroutine(float time)
        {
            while (true)
            {
                yield return new WaitForSeconds(time);
                print("WaitAndPrint " + Time.time);
            }
        }

        IEnumerator StopMotion()
        {
            yield return new WaitForFixedUpdate();
            if (hasRigidbody)
            {
                storedRB.velocity = Vector3.zero;
                storedRB.angularVelocity = Vector3.zero;
                storedRB.Sleep();
            }
        }

        IEnumerator delayTime(float t)
        {
            yield return new WaitForSeconds(t);

        }

        //		IEnumerator conditionalStop(float t)
        //		{
        //			yield return new WaitUntil(t);
        //			return;
        //		}

        public void setMotion(Vector3 v, string msg, string obj, ObjectForce fType, string fMode, float amt1, float amt2)
        {

            Vector3 tVec = v;
            //            tVec.x *= tEvent.amount;
            //            tVec.y *= tEvent.amount;
            //            tVec.z *= tEvent.amount;  

            Vector3 tVecRB = currentVelocity;

            tVecRB.x = v.x * amt1;
            tVecRB.y = v.y * amt1;
            tVecRB.z = v.z * amt1;

            if (hasRigidbody)
            {

                ObjectForce objForce = fType;

                if (storedRB.isKinematic)
                {
                    objForce = ObjectForce.TRANSLATION;
                }

                switch (objForce)
                {
                    case ObjectForce.TRANSLATION:
                        transform.Translate(tVecRB);
                        break;
                    case ObjectForce.ROTATION:
                        transform.Rotate(tVecRB);
                        //gameObject.transform.eulerAngles = tVec;
                        break;
                    case ObjectForce.RELATIVEFORCE:
                        switch (fMode)
                        {
                            case "impulse":
                                gfRForce.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfRForce.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfRForce.acceleration += tVecRB;
                                break;
                            case "set":
                                gfRForce.force = tVecRB;
                                gfRForce.impulse = tVecRB;
                                gfRForce.acceleration = tVecRB;
                                gfRForce.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfRForce.force.Scale(tVec);
                                gfRForce.impulse.Scale(tVec);
                                gfRForce.acceleration.Scale(tVec);
                                gfRForce.velocityChange.Scale(tVec);
                                storedRB.velocity.Scale(tVec);
                                storedRB.angularVelocity.Scale(tVec);
                                //storedRB.Sleep();
                                break;
                            case "sleep":
                                gfRForce.force = Vector3.zero;
                                gfRForce.impulse = Vector3.zero;
                                gfRForce.acceleration = Vector3.zero;
                                gfRForce.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfRForce.force += tVecRB;
                                break;
                        }
                        break;
                    case ObjectForce.TORQUE:
                        //  storedRotation += tVecRB;
                        switch (fMode)
                        {
                            case "impulse":
                                gfTorque.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfTorque.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfTorque.acceleration += tVecRB;
                                break;
                            case "set":
                                gfTorque.force = tVecRB;
                                gfTorque.impulse = tVecRB;
                                gfTorque.acceleration = tVecRB;
                                gfTorque.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfTorque.force = Vector3.zero;
                                gfTorque.impulse = Vector3.zero;
                                gfTorque.acceleration = Vector3.zero;
                                gfTorque.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfTorque.force += tVecRB;
                                break;
                        }
                        break;
                    case ObjectForce.RELATIVETORQUE:
                        switch (fMode)
                        {
                            case "impulse":
                                gfRTorque.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfRTorque.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfRTorque.acceleration += tVecRB;
                                break;
                            case "set":
                                gfRTorque.force = tVecRB;
                                gfRTorque.impulse = tVecRB;
                                gfRTorque.acceleration = tVecRB;
                                gfRTorque.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfRTorque.force = Vector3.zero;
                                gfRTorque.impulse = Vector3.zero;
                                gfRTorque.acceleration = Vector3.zero;
                                gfRTorque.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfRTorque.force += tVecRB;
                                break;
                        }
                        break;
                    case ObjectForce.FORCE:
                    default:
                        switch (fMode)
                        {
                            case "impulse":
                                gfForce.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfForce.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfForce.acceleration += tVecRB;
                                break;
                            case "set":
                                storedRB.velocity = tVecRB;
                                //                                gfForce.force = tVecRB;
                                //                                gfForce.impulse = tVecRB;
                                //                                gfForce.acceleration = tVecRB;
                                //                                gfForce.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfForce.force = Vector3.zero;
                                gfForce.impulse = Vector3.zero;
                                gfForce.acceleration = Vector3.zero;
                                gfForce.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfForce.force += tVecRB;
                                break;
                        }
                        break;
                }
            }
            else
            {
                switch (fType)
                {
                    case ObjectForce.TRANSLATION:
                        transform.Translate(tVecRB);
                        break;
                    case ObjectForce.ROTATION:
                        transform.Rotate(tVecRB);
                        break;
                    default:
                        break;
                }
            }
        }

        void motion()
        {


            GameEvent tEvent = currentEvent;

            if (!tEvent.alive)
                return;

            //			coroutine = motionCoroutine(2.0f);
            //			StartCoroutine(coroutine);

            Vector3 tVec = tEvent.vectorStorage;
            //            tVec.x *= tEvent.amount;
            //            tVec.y *= tEvent.amount;
            //            tVec.z *= tEvent.amount;	

            Vector3 tVecRB = currentVelocity;

            tVecRB.x = tEvent.vectorStorage.x * tEvent.amount;
            tVecRB.y = tEvent.vectorStorage.y * tEvent.amount;
            tVecRB.z = tEvent.vectorStorage.z * tEvent.amount;

            if (hasRigidbody)
            {

                ObjectForce objForce = tEvent.force;

                if (storedRB.isKinematic)
                {
                    objForce = ObjectForce.TRANSLATION;
                }

                switch (objForce)
                {
                    case ObjectForce.TRANSLATION:
                        transform.Translate(tVecRB);
                        break;
                    case ObjectForce.ROTATION:
                        transform.Rotate(tVecRB);
                        //gameObject.transform.eulerAngles = tVec;
                        break;
                    case ObjectForce.RELATIVEFORCE:
                        switch (currentEvent.msgFilter)
                        {
                            case "impulse":
                                gfRForce.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfRForce.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfRForce.acceleration += tVecRB;
                                break;
                            case "set":
                                gfRForce.force = tVecRB;
                                gfRForce.impulse = tVecRB;
                                gfRForce.acceleration = tVecRB;
                                gfRForce.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfRForce.force.Scale(tVec);
                                gfRForce.impulse.Scale(tVec);
                                gfRForce.acceleration.Scale(tVec);
                                gfRForce.velocityChange.Scale(tVec);
                                storedRB.velocity.Scale(tVec);
                                storedRB.angularVelocity.Scale(tVec);
                                //storedRB.Sleep();
                                break;
                            case "sleep":
                                gfRForce.force = Vector3.zero;
                                gfRForce.impulse = Vector3.zero;
                                gfRForce.acceleration = Vector3.zero;
                                gfRForce.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfRForce.force += tVecRB;
                                break;
                        }
                        break;
                    case ObjectForce.TORQUE:
                        //	storedRotation += tVecRB;
                        switch (currentEvent.msgFilter)
                        {
                            case "impulse":
                                gfTorque.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfTorque.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfTorque.acceleration += tVecRB;
                                break;
                            case "set":
                                gfTorque.force = tVecRB;
                                gfTorque.impulse = tVecRB;
                                gfTorque.acceleration = tVecRB;
                                gfTorque.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfTorque.force = Vector3.zero;
                                gfTorque.impulse = Vector3.zero;
                                gfTorque.acceleration = Vector3.zero;
                                gfTorque.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfTorque.force += tVecRB;
                                break;
                        }
                        break;
                    case ObjectForce.RELATIVETORQUE:
                        switch (currentEvent.msgFilter)
                        {
                            case "impulse":
                                gfRTorque.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfRTorque.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfRTorque.acceleration += tVecRB;
                                break;
                            case "set":
                                gfRTorque.force = tVecRB;
                                gfRTorque.impulse = tVecRB;
                                gfRTorque.acceleration = tVecRB;
                                gfRTorque.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfRTorque.force = Vector3.zero;
                                gfRTorque.impulse = Vector3.zero;
                                gfRTorque.acceleration = Vector3.zero;
                                gfRTorque.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfRTorque.force += tVecRB;
                                break;
                        }
                        break;
                    case ObjectForce.FORCE:
                    default:
                        switch (currentEvent.msgFilter)
                        {
                            case "impulse":
                                gfForce.impulse += tVecRB;
                                break;
                            case "velocitychange":
                                gfForce.velocityChange += tVecRB;
                                break;
                            case "acceleration":
                                gfForce.acceleration += tVecRB;
                                break;
                            case "set":
                                storedRB.velocity = tVecRB;
                                //                                gfForce.force = tVecRB;
                                //                                gfForce.impulse = tVecRB;
                                //                                gfForce.acceleration = tVecRB;
                                //                                gfForce.velocityChange = tVecRB;
                                break;
                            case "stop":
                                gfForce.force = Vector3.zero;
                                gfForce.impulse = Vector3.zero;
                                gfForce.acceleration = Vector3.zero;
                                gfForce.velocityChange = Vector3.zero;
                                storedRB.velocity = Vector3.zero;
                                storedRB.angularVelocity = Vector3.zero;
                                storedRB.Sleep();
                                break;
                            case "force":
                            default:
                                gfForce.force += tVecRB;
                                break;
                        }
                        break;
                }
            }
            else
            {
                switch (tEvent.force)
                {
                    case ObjectForce.TRANSLATION:
                        transform.Translate(tVecRB);
                        break;
                    case ObjectForce.ROTATION:
                        transform.Rotate(tVecRB);
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region TRIGGER UNITY

        void OnTriggerEnter(Collider other)
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                //			bool collided = false;
                GameEvent storeEvent = currentEvent;

                string othername = string.Empty;

                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;
                    if (!kList.alive || currentEvent.playerAction != PlayerFunctions.TRIGGERENTER)
                        continue;

                    switch (currentEvent.filterType)
                    {
                        case ObjectNameType.NAME:
                            othername = other.gameObject.name;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.name != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.name != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }
                            CollisionCallName(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                        case ObjectNameType.TAG:
                        default:
                            othername = other.gameObject.tag;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.tag != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.tag != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }
                            CollisionCallTag(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;

                            break;
                    }

                }
                currentEvent.on = false;
                //currentEvent = storeEvent;


            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnTriggerExit(Collider other)
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                //			bool collided = false;
                GameEvent storeEvent = currentEvent;

                string othername = string.Empty;

                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;
                    if (!kList.alive || currentEvent.playerAction != PlayerFunctions.TRIGGEREXIT)
                        continue;

                    switch (currentEvent.filterType)
                    {

                        case ObjectNameType.NAME:
                            othername = other.gameObject.name;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.name != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.name != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }
                            CollisionCallName(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;

                        case ObjectNameType.TAG:
                        default:
                            othername = other.gameObject.tag;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.tag != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.tag != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }

                            CollisionCallTag(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;

                            break;
                    }

                }
                currentEvent.on = false;
                currentEvent = storeEvent;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnTriggerStay(Collider other)
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                //			bool collided = false;
                GameEvent storeEvent = currentEvent;

                string othername = string.Empty;

                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;
                    if (!kList.alive || currentEvent.playerAction != PlayerFunctions.TRIGGERSTAY)
                        continue;

                    switch (currentEvent.filterType)
                    {
                        case ObjectNameType.NAME:
                            othername = other.gameObject.name;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.name != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.name != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }
                            CollisionCallName(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                        case ObjectNameType.TAG:
                        default:


                            othername = other.gameObject.tag;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.tag != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.tag != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }

                            CollisionCallTag(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            //currentEvent.on = false;
                            break;

                    }

                }
                currentEvent.on = false;
                currentEvent = storeEvent;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        #endregion

        void OnParticleCollision(GameObject other)
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                if (forceParticle == 0)
                    return;

                //Debug.Log ("Particle collisions");

                if (!hasCollider || !hasRigidbody)
                {
                    return;
                }
                if (!gameObject.GetComponent<Collider>().enabled || gameObject.GetComponent<Rigidbody>().isKinematic)
                {
                    return;
                }

                Vector3 direction = other.transform.position - gameObject.transform.position;

                gfForce.force += direction * forceParticle;


            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnCollisionEnter(Collision other)
        {

            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                //			bool collided = false;
                GameEvent storeEvent = currentEvent;

                string othername = string.Empty;
                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;
                    if (!currentEvent.alive || currentEvent.playerAction != PlayerFunctions.COLLISIONENTER)
                        continue;
                    switch (currentEvent.filterType)
                    {

                        case ObjectNameType.NAME:
                            othername = other.gameObject.name;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.name != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.name != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            storedVector = other.contacts[0].normal;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }

                            CollisionCallName(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                        case ObjectNameType.TAG:
                        default:

                            othername = other.gameObject.tag;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.tag != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.tag != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            storedVector = other.contacts[0].normal;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }

                            CollisionCallTag(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                    }
                    currentEvent.on = false;
                }

                currentEvent = storeEvent;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnCollisionStay(Collision other)
        {

            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                //			bool collided = false;
                GameEvent storeEvent = currentEvent;

                string othername = string.Empty;
                foreach (GameEvent kList in eventList)
                {

                    currentEvent = kList;
                    if (!currentEvent.alive || currentEvent.playerAction != PlayerFunctions.COLLISIONSTAY)
                        continue;
                    switch (currentEvent.filterType)
                    {

                        case ObjectNameType.NAME:
                            othername = other.gameObject.name;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.name != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.name != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            storedVector = other.contacts[0].normal;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }
                            CollisionCallName(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                        case ObjectNameType.TAG:
                        default:

                            othername = other.gameObject.tag;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.tag != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.tag != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            storedVector = other.contacts[0].normal;
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }

                            CollisionCallTag(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                    }
                    currentEvent.on = false;
                }

                currentEvent = storeEvent;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void OnCollisionExit(Collision other)
        {

            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {


                //			bool collided = false;
                GameEvent storeEvent = currentEvent;

                string othername = string.Empty;
                foreach (GameEvent kList in eventList)
                {


                    currentEvent = kList;
                    if (!currentEvent.alive || currentEvent.playerAction != PlayerFunctions.COLLISIONEXIT)
                        continue;
                    switch (currentEvent.filterType)
                    {

                        case ObjectNameType.NAME:
                            othername = other.gameObject.name;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.name != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.name != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            storedVector = other.gameObject.transform.position - gameObject.transform.position;
                            storedVector = storedVector.normalized;
                            foreach (ContactPoint contact in other.contacts)
                            {
                                storedVector = other.contacts[0].normal;

                            }
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }
                            CollisionCallName(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                        case ObjectNameType.TAG:
                        default:
                            othername = other.gameObject.tag;
                            switch (currentEvent.relation)
                            {
                                case ObjectRelation.ALL_EXCEPT:
                                    if (othername == currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ME:
                                    if (gameObject.tag != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.BOTH:
                                    if (gameObject.tag != currentEvent.objFilter || othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.IT:
                                case ObjectRelation._DEFAULT:
                                    if (othername != currentEvent.objFilter)
                                    {
                                        continue;
                                    }
                                    break;
                                case ObjectRelation.ALL:
                                default:
                                    break;
                            }
                            currentEvent.on = true;
                            storedVector = other.gameObject.transform.position - gameObject.transform.position;
                            storedVector = storedVector.normalized;
                            foreach (ContactPoint contact in other.contacts)
                            {
                                storedVector += other.contacts[0].normal;
                                storedVector = storedVector.normalized;

                            }
                            collided = 1;
                            collidedGO = other.gameObject;
                            if (currentEvent.relation == ObjectRelation.PARENT)
                            {
                                collidedGO = other.transform.root.gameObject;
                            }

                            CollisionCallTag(currentEvent, (currentEvent.relation == ObjectRelation.PARENT ? other.transform.root.gameObject : other.gameObject), othername);
                            collided = 0;
                            collidedGO = null;
                            break;
                    }
                    currentEvent.on = false;
                }

                currentEvent = storeEvent;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled
        }

        void CollisionCallName(GameEvent CollisionEvent, GameObject other, string othername)
        {

            //			bool tempBool = false;
            CollisionEvent.on = true;
            switch (CollisionEvent.gameAction)
            {
                case GameFunctions.DESTROY:

                    switch (CollisionEvent.relation)
                    {
                        case ObjectRelation.ME://This one
                            Destroy(gameObject);
                            break;
                        case ObjectRelation.IT://The other gameobject
                            Destroy(other);
                            break;
                        case ObjectRelation.ALL:
                            while (other)
                            {
                                Destroy(other);
                                other = GameObject.Find(othername);
                            }
                            break;
                        case ObjectRelation.ALL_EXCEPT:
                            while (other)
                            {
                                Destroy(other);
                                other = GameObject.Find(othername);
                            }
                            break;
                        case ObjectRelation.BOTH:
                        default://BOTH
                            Destroy(other);
                            Destroy(gameObject);
                            break;
                    }
                    break;
                case GameFunctions._NO_OP:
                    customEventList[CollisionEvent.eventName].Invoke();
                    break;
                case GameFunctions.CALL_EVENT:
                    customEventList[CollisionEvent.eventName].Invoke();
                    break;
                case GameFunctions.RANDOMIZE:
                case GameFunctions.SET:
                case GameFunctions.REGISTER:
                case GameFunctions.IF_EVENT_ON:
                case GameFunctions.LOAD_SCENE:
                case GameFunctions.PLAY:

                    customEventList[CollisionEvent.eventName].Invoke();
                    break;
                case GameFunctions.GET_MOVE_DATA:
                case GameFunctions.BEHAVIOR:
                    customEventList[CollisionEvent.eventName].Invoke();
                    break;


                case GameFunctions.PAUSE:
                default:
                    print("DON'T USE THIS GAME ACTION WITH COLLISION. DETECT COLLISION AND USE CALL_EVENT TO TRIGGER THE DESIRED & DISCRETE EVENTS");
                    break;
            }

        }

        void CollisionCallTag(GameEvent CollisionEvent, GameObject other, string othername)
        {

            //			bool tempBool = false;
            CollisionEvent.on = true;
            //			Debug.Log("collision is on");
            switch (CollisionEvent.gameAction)
            {
                case GameFunctions.DESTROY:

                    switch (CollisionEvent.relation)
                    {
                        case ObjectRelation.ME://This one
                            Destroy(gameObject);
                            break;
                        case ObjectRelation.IT://The other gameobject
                            Destroy(other.gameObject);
                            break;
                        case ObjectRelation.ALL:
                            foreach (GameObject go in GameObject.FindGameObjectsWithTag(othername))
                            {
                                Destroy(go);
                            }
                            break;
                        case ObjectRelation.ALL_EXCEPT:
                            other = GameObject.FindWithTag(othername);
                            while (other)
                            {
                                Destroy(other);
                                other = GameObject.FindWithTag(othername);
                            }
                            break;
                        case ObjectRelation.BOTH:
                        default://BOTH
                            Destroy(other);
                            Destroy(gameObject);
                            break;
                    }
                    break;
                case GameFunctions._NO_OP:
                case GameFunctions.RANDOMIZE:
                    customEventList[CollisionEvent.eventName].Invoke();
                    break;

                case GameFunctions.SET:
                case GameFunctions.CALL_EVENT:
                    //					collided = 1;
                    //						collidedGO = other;
                    customEventList[CollisionEvent.eventName].Invoke();
                    //					collidedGO = null;
                    //					collided = 0;
                    break;
                case GameFunctions.REGISTER:
                case GameFunctions.IF_EVENT_ON:
                case GameFunctions.LOAD_SCENE:
                case GameFunctions.PLAY:
                    customEventList[CollisionEvent.eventName].Invoke();
                    break;
                case GameFunctions.GET_MOVE_DATA:
                case GameFunctions.BEHAVIOR:

                    customEventList[CollisionEvent.eventName].Invoke();
                    break;
                case GameFunctions.PAUSE:
                default:
                    print("DON'T USE THIS GAME ACTION WITH COLLISION. DETECT COLLISION AND USE CALL_EVENT GAMEACTION TO TRIGGER THE DESIRED & DISCRETE EVENTS");
                    break;
            }

        }

        void FixedUpdate()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {
                GameEvent tEvent = currentEvent;

                if (objectPause)
                {
                    if (hasRigidbody)
                    {
                        storedForces = Vector3.zero;
                        storedRB.Sleep();
                        storedRB.velocity = Vector3.zero;
                    }
                    currentVelocity = storedRB.velocity;

                    gfForce.allZero();
                    gfRForce.allZero();
                    gfTorque.allZero();
                    gfRTorque.allZero();

                    storedForces = Vector3.zero;
                    storedRotation = Vector3.zero;
                    storedRelativeRotation = Vector3.zero;
                    return;
                }

                gameObject.transform.position += vectorModifier;
                //gfForce.force += storedForces;

                if (hasRigidbody == true)
                {

                    //gfForce.velocityChange += storedForces;
                    gfForce.force += storedForces;

                    storedRB.AddForce(gfForce.force, ForceMode.Force);
                    storedRB.AddForce(gfForce.impulse, ForceMode.Impulse);
                    storedRB.AddForce(gfForce.acceleration, ForceMode.Acceleration);
                    storedRB.AddForce(gfForce.velocityChange, ForceMode.VelocityChange);

                    float tSign = 1.0f;
                    tSign = (gfRForce.force.x + gfRForce.force.y + gfRForce.force.z >= 0 ? 1 : -1);
                    storedRB.AddRelativeForce(forwardVector * gfRForce.force.magnitude * tSign, ForceMode.Force);

                    tSign = (gfRForce.impulse.x + gfRForce.impulse.y + gfRForce.impulse.z >= 0 ? 1 : -1);
                    storedRB.AddRelativeForce(forwardVector * gfRForce.impulse.magnitude * tSign, ForceMode.Impulse);

                    tSign = (gfRForce.acceleration.x + gfRForce.acceleration.y + gfRForce.acceleration.z >= 0 ? 1 : -1);
                    storedRB.AddRelativeForce(forwardVector * gfRForce.acceleration.magnitude * tSign, ForceMode.Acceleration);

                    tSign = (gfRForce.velocityChange.x + gfRForce.velocityChange.y + gfRForce.velocityChange.z >= 0 ? 1 : -1);
                    storedRB.AddRelativeForce(forwardVector * gfRForce.velocityChange.magnitude * tSign, ForceMode.VelocityChange);

                    storedRB.AddTorque(gfTorque.force, ForceMode.Force);
                    storedRB.AddTorque(gfTorque.impulse, ForceMode.Impulse);
                    storedRB.AddTorque(gfTorque.acceleration, ForceMode.Acceleration);
                    storedRB.AddTorque(gfTorque.velocityChange, ForceMode.VelocityChange);

                    storedRB.AddRelativeTorque(gfRTorque.force, ForceMode.Force);
                    storedRB.AddRelativeTorque(gfRTorque.impulse, ForceMode.Impulse);
                    storedRB.AddRelativeTorque(gfRTorque.acceleration, ForceMode.Acceleration);
                    storedRB.AddRelativeTorque(gfRTorque.velocityChange, ForceMode.VelocityChange);

                    if (maxMagnitude > -1)
                    {

                        Vector3 tV = storedRB.velocity;
                        float tM = tV.magnitude;
                        if (tM > maxMagnitude)
                        {
                            storedRB.velocity = Vector3.ClampMagnitude(storedRB.velocity, maxMagnitude);
                        }
                    }
                    if (minMagnitude > -1)
                    {
                        if (storedRB.velocity.magnitude < minMagnitude)
                        {
                            Vector3 tV = storedRB.velocity.normalized;

                            Vector3 minVec;
                            //minVec.x = minVec.y = minVec.z = minMagnitude;
                            tV.Scale(new Vector3(minMagnitude, minMagnitude, minMagnitude));
                            storedRB.velocity = tV;
                        }
                    }
                    if (useFakeGravity)
                    {

                        if (storedRB.useGravity == true)
                        {

                        }
                        else if (storedRB.useGravity == false)
                        {
                            storedRB.AddForce(globalGravity, ForceMode.Acceleration);
                        }

                    }

                    currentVelocity = storedRB.velocity;

                    gfForce.allZero();
                    gfRForce.allZero();
                    gfTorque.allZero();
                    gfRTorque.allZero();

                    storedForces = Vector3.zero;
                    storedRotation = Vector3.zero;
                    storedRelativeRotation = Vector3.zero;
                }
                else
                {
                    gameObject.transform.position += gfForce.velocityChange;
                    if (useFakeGravity)
                    {
                        gameObject.transform.position += globalGravity.normalized * (Time.time - timeCounter);//, ForceMode.Acceleration);    
                    }
                }

                foreach (GameEvent kList in eventList)
                {

                    if (kList.playerAction == PlayerFunctions.UPDATEFIXED)
                    {
                        if (!kList.alive)
                        {
                            continue;
                        }

                        currentEvent = kList;
                        customEventList[currentEvent.eventName].Invoke();
                        continue;
                    }
                }
                currentEvent = tEvent;

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled

        }

        // Update is called once per frame
        void Update()
        {

            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                foreach (GameEvent kList in eventList)
                {

                    if (kList.group.Length > 0)
                    {
                        string _str = kList.group;
                        int tInt = _str.IndexOf('#') + 1;

                        if (tInt > 0)
                        {
                            _str = _str.Substring(tInt);
                            //currentEvent = null;
                            messageList.TryGetValue(_str, out currentEvent);//messageList[_str];
                        }
                        //bool activeGroup = currentEvent.alive;
                        if (currentEvent != null && !currentEvent.alive)//activeGroup)
                        {
                            //Debug.Log(currentEvent.eventName + ":continued");
                            continue;
                        }
                    }

                    currentEvent = kList;

                    msgString = currentEvent.objFilter;


                    if (kList.alive)
                    {


                        switch (kList.playerAction)
                        {
                            case PlayerFunctions.INPUTPRESS:
                                keypress();
                                break;
                            case PlayerFunctions.INPUTANY:
                                if (canInteract == false)
                                    continue;
                                if (Input.anyKeyDown)
                                {
                                    customEventList[kList.eventName].Invoke();
                                    kList.on = true;
                                }
                                break;
                            case PlayerFunctions.INPUTUP:
                                unKeypress();
                                break;
                            case PlayerFunctions.INPUTDOWN:
                                keydown();
                                break;
                            case PlayerFunctions.MESSAGE_EVENT:
                                break;
                            //						case PlayerFunctions.SYSTEM:
                            //							if ()
                            //							systemEventList.Add(kList);
                            //						break;
                            case PlayerFunctions.UPDATE:
                                customEventList[kList.eventName].Invoke();

                                break;
                            default:
                                break;

                        }
                    }
                    //				else if (kList.playerAction == PlayerFunctions.SYSTEM)
                    //				{
                    //					if (kList.msgFilter != "mouseevents")
                    //					customEventList[kList.eventName].Invoke();
                    //				}
                    if (kList.playerAction == PlayerFunctions.SYSTEM)
                    {

                        switch (kList.objFilter)
                        {
                            case "mouseevents":
                                if (!systemEventList.Contains(kList))
                                {
                                    systemEventList.Add(kList);
                                }
                                break;
                            default:
                                customEventList[kList.eventName].Invoke();
                                break;
                        }
                    }

                }

                //	TO REMOVE WARNINGS
                if (valueStorage > 0 || storedPosition.x > 0 || storedRotation.x == 0)
                {
                    //globalGravity = Physics.gravity;
                    //storedRB.useGravity = 
                    return;
                }

            }//!GetComponent<GenericBehavior>().isActiveAndEnabled

        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (GetComponent<GenericBehavior>().isActiveAndEnabled)
            {

                foreach (GameEvent kList in eventList)
                {
                    currentEvent = kList;

                    msgString = currentEvent.objFilter;
                    if (kList.alive)
                    {

                        switch (kList.playerAction)
                        {
                            case PlayerFunctions.UPDATELATE:
                                customEventList[kList.eventName].Invoke();
                                break;
                            default:
                                break;

                        }

                    }

                }
            }//!GetComponent<GenericBehavior>().isActiveAndEnabled

        }


    }
}
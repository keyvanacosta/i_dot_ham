﻿using UnityEngine;
using System.Collections;


/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        SCORE COMPONENT FOR Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

//[ExecuteInEditMode]
public class ScoreMechanics : MonoBehaviour
{

    [SerializeField]
    private GameObject[] digits;

    [SerializeField]
    private float digitWidth = 1;
    //	[SerializeField]
    private float digitHeight = 1;
    //	[SerializeField]
    private float digitSpacing = 1;
    [SerializeField]
    private GameObject digitObject;

    [SerializeField]
    private bool overflow = false;
    [SerializeField]
    private bool underflow = false;

    public Material matColor = null;

    //	[SerializeField]
    //	private bool showAllDigits = false;

    [SerializeField]
    private int digitNumber = 1;

    [SerializeField]
    private int maxNumber = 0;

    [SerializeField]
    private int minNumber = 0;



    [SerializeField]
    private int amount = 0;



    int pastAmount = -1;

    public float accessPrivateFloats(string whichMember, float value = 0, bool setGet = false)
    {
        /*
			variables to modify:
			^valueStorage, ^mainColor, ^maxMagnitude,
			^globalTimeScale, ^mass, ^force;
			//*/
        float tfloat = 0;
        /// GET ANY OF THESE VARIABLES RETURNED
        if (!setGet)
        {

            switch (whichMember)
            {
                case "score":
                    tfloat = (float)amount;
                    break;

            }
        }
        else
        { //Set
            tfloat = value;
            switch (whichMember)
            {
                case "score":
                    amount = (int)tfloat;
                    break;
            }
        }
        return tfloat;
    }

    public int getAmount()
    {
        return amount;
    }

    public int addOneAmount()
    {
        //Debug.Log("llegó");
        amount++;
        return amount;
    }

    public void setAmount(int _amt)
    {
        amount = _amt;
    }

    public void setMaxAmount(int _max)
    {
        maxNumber = _max;
    }

    public void setMinAmount(int _min)
    {
        minNumber = _min;
    }

    public int addAmount(int _amount)
    {
        amount += _amount;
        return amount;
    }

    public int subOneAmount()
    {
        amount--;
        return amount;
    }

    void Awake()
    {

        //int digitNumber = 0;
        //		int temp = amount;
        //		if (temp <= 1) digitNumber = 1; 
        //		while (temp >= 1 ) 
        //		{
        //			temp /= 10;
        //			digitNumber++;
        //		}
        string strNumber = (maxNumber).ToString();
        string period = ".";
        string strNumber2 = strNumber.Replace(period, "");

        //digitNumber = strNumber2.Length;

        //digitWidth = digitObject.GetComponent<DigitPrint>().getWidth() + digitSpacing;
        digitWidth = digitObject.GetComponent<DigitPrint>().getWidth() + digitWidth;
        digitHeight = digitObject.GetComponent<DigitPrint>().getHeight() + digitSpacing;

        Vector3 positioningVector = gameObject.transform.position;

        digits = new GameObject[digitNumber];
        //float digitsWidth = 10;
        for (int i = 0; i < digitNumber; i++)
        {
            digits[i] = Instantiate(digitObject, positioningVector, Quaternion.identity) as GameObject;
            if (matColor != null)
                for (int j = 0; j < digits[i].GetComponent<DigitPrint>().GetComponentsInChildren<Renderer>().Length; j++)
                {
                    digits[i].GetComponent<DigitPrint>().GetComponentsInChildren<Renderer>()[j].material = matColor;
                }
            digits[i].gameObject.GetComponent<Transform>().SetParent(gameObject.transform);
            digits[i].gameObject.transform.localScale = gameObject.transform.localScale;
            //digits [i].gameObject.transform.localRotation = gameObject.transform.parent.rotation;
            digits[i].gameObject.transform.rotation = gameObject.transform.rotation;

            //digits[i].gameObject.transform.Translate(new Vector3(digitWidth * gameObject.transform.localScale.x,0,0),digits[i].gameObject.transform);
            if (i > 0)
                //	digits[i].gameObject.transform.Translate(new Vector3(digitWidth * gameObject.transform.localScale.x,0,0),digits[i-1].gameObject.transform);
                digits[i].gameObject.transform.Translate(new Vector3(digitWidth * i, 0, 0), digits[i].gameObject.transform);
            //			//positioningVector.x += digitWidth*(i+1) * gameObject.transform.localScale.x;// * .6f;

            //			//	TO REMOVE WARNING: digitHeight not being used
            digitHeight = digitHeight * 1;
        }

        //Debug.Log("Digits:"+digitNumber);

    }

    // Use this for initialization
    void Start()
    {

        //pastAmount = amount;

    }

    void Play(int _value)
    {
        SendMessage("addAmount", _value);
    }

    void play(int _value)
    {
        SendMessage("addAmount", _value);
    }

    void change()
    {

        string strNumber = amount.ToString().PadLeft(digitNumber, '0');

        int unit = amount;
        unit = amount % (1);
        unit = digitNumber - 1;
        for (int i = 0; i < digitNumber; i++)
        {
            int tnum = (int)char.GetNumericValue(strNumber[i]);
            digits[i].GetComponent<DigitPrint>().setNumber(tnum);
            digits[i].GetComponent<DigitPrint>().changeDigitTo(unit);

        }

        //		int unit = amount;
        //		unit = amount%(1);
        //		for(int i = 0; i < digitNumber; i++)
        //		{
        //			if (i == 0)
        //			{
        //				unit = amount%(1);
        //			}
        //			else
        //			{
        //				unit = amount/(10^i);
        //				unit = amount%(1);
        //			}
        //			
        //			digits[i].GetComponent<DigitPrint>().setNumber(amount+i);
        //			digits[i].GetComponent<DigitPrint>().changeDigitTo(unit);
        //			
        //		}
        pastAmount = amount;
    }

    // Update is called once per frame
    void Update()
    {


        if (amount >= maxNumber)
        {
            if (overflow)
            {
                amount = minNumber;
                //pastAmount = maxNumber;
            }
            else
            {
                //pastAmount = maxNumber-1;
                amount = maxNumber;
            }
        }

        if (amount < minNumber)
        {
            if (underflow)
            {
                amount = maxNumber;
                //pastAmount = maxNumber;
            }
            else
            {
                //pastAmount = maxNumber-1;
                amount = minNumber;
            }
        }

        if (amount != pastAmount)
        {
            change();
        }


    }
}

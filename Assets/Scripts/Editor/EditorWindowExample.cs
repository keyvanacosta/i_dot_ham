﻿using UnityEngine;
using UnityEditor;

public class MyWindow : EditorWindow {

	static string[] inputHigh = new string[] 
	{
		"HIGH","Game has multiple objects that each \n acts with its own unique input controls",
		"Some objects react on their own \n(timer, keyboard manipulation: move ball), \n some react because of interaction with them (i.e. collision)",
		"Keyboard input is recognized in a \nconsistent basis",
	};

	static string[] inputMedium = new string[] 
	{
		"MEDIUM","Game has one object that reacts to \ninput from player or one input command",
		"One but not both types of keyboard \ninputs are entered and reacted to accordingly",

	};

	static string[] inputLow = new string[] 
	{
		"LOW","GameObjects i.e. Environment, have no useful, \ncomprehensive, interactions",
//		"Neither of the keyboard states \nhave been recognized",
//		"None of the input devices are used",
	};
	static string[] prefabHigh = new string[] 
	{
		"HIGH","(2+) GameObjects w/multiple non-default component; \nnamely, GenericBehavior.cs",
		"Has 1+ Nested children objects,",
		"Has Non Generic Name, well labeled",
	};

	static string[] prefabMedium = new string[] 
	{
		"MEDIUM","Only One GameObject with children",
		"Game Objects have accurate labels, but unnecessary prefabs",

	};

	static string[] prefabLow = new string[] 
	{
		"LOW","Scripts and parenting aren't functional towards the scene",
		//		"Neither of the keyboard states \nhave been recognized",
		//		"None of the input devices are used",
	};
	static string[] timedHigh = new string[] 
	{
		"HIGH","(2+) GameObjects w/multiple non-default component; namely, GenericBehavior.cs",
		"Has 1+ Nested children objects,",
		"Has Non Generic Name, well labeled",
	};

	static string[] timedMedium = new string[] 
	{
		"MEDIUM","Only One GameObject with children",
		"Game Objects have accurate labels, but unnecessary prefabs",

	};

	static string[] timedLow = new string[] 
	{
		"LOW","Scripts and parenting aren't functional towards the scene",
	};

	static string[] cohesionHigh = new string[] 
	{
		"HIGH","Scenes and themes enhance one another",
	};

	static string[] cohesionMedium = new string[] 
	{
		"MEDIUM","Scenes are disjointed, but within theme",

	};

	static string[] cohesionLow = new string[] 
	{
		"LOW","Scenes are disparate",
	};

	string myString = "Hello World";
	bool groupEnabled;
	bool myBool = true;
	float myFloat = 1.23f;

	int inputSelectHigh = 0;
	int inputSelectMedium = 0;
	int inputSelectLow = 0;
	int prefabSelectHigh = 0;
	int prefabSelectMedium = 0;
	int prefabSelectLow = 0;
	int timedSelectHigh = 0;
	int timedSelectMedium = 0;
	int timedSelectLow = 0;
	int cohesionSelectHigh = 0;
	int cohesionSelectMedium = 0;
	int cohesionSelectLow = 0;

	// Add menu named "My Window" to the Window menu
	[MenuItem ("Window/Rubrics/Pong")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		MyWindow window = (MyWindow)EditorWindow.GetWindow (typeof (MyWindow));
		window.Show();
	}

	void OnGUI () {
		GUILayout.Label ("Beginnings Rubric", EditorStyles.boldLabel);

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));

		GUILayout.Label ("Input", EditorStyles.boldLabel, GUILayout.Width(100));
		EditorGUILayout.BeginVertical("Box");
		inputSelectHigh = GUILayout.SelectionGrid(inputSelectHigh,inputHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		inputSelectMedium = GUILayout.SelectionGrid(inputSelectMedium,inputMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		inputSelectLow = GUILayout.SelectionGrid(inputSelectLow,inputLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		GUILayout.Label ("Prefabs", EditorStyles.boldLabel, GUILayout.Width(100));
		EditorGUILayout.BeginVertical("Box");
		prefabSelectHigh = GUILayout.SelectionGrid(prefabSelectHigh,prefabHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		prefabSelectMedium = GUILayout.SelectionGrid(prefabSelectMedium,prefabMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		prefabSelectLow = GUILayout.SelectionGrid(prefabSelectLow,prefabLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		GUILayout.Label ("Timers", EditorStyles.boldLabel, GUILayout.Width(100));
		EditorGUILayout.BeginVertical("Box");
		timedSelectHigh = GUILayout.SelectionGrid(timedSelectHigh,timedHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		timedSelectMedium = GUILayout.SelectionGrid(timedSelectMedium,timedMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		timedSelectLow = GUILayout.SelectionGrid(timedSelectLow,timedLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		GUILayout.Label ("Cohesion", EditorStyles.boldLabel, GUILayout.Width(100));
		EditorGUILayout.BeginVertical("Box");

		cohesionSelectHigh = GUILayout.SelectionGrid(cohesionSelectHigh,cohesionHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		cohesionSelectMedium = GUILayout.SelectionGrid(cohesionSelectMedium,cohesionMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		cohesionSelectLow = GUILayout.SelectionGrid(cohesionSelectLow,cohesionLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;

		if (inputSelectLow == 1)
		{
			inputSelectHigh = 0;
			inputSelectMedium = 0;
		}
		if (prefabSelectLow == 1)
		{
			prefabSelectHigh = 0;
			prefabSelectMedium = 0;
		}
		if (timedSelectLow == 1)
		{
			timedSelectHigh = 0;
			timedSelectMedium = 0;
		}
		if (cohesionSelectLow == 1)
		{
			cohesionSelectHigh = 0;
			cohesionSelectMedium = 0;
		}

		myString = EditorGUILayout.TextField ("Actual Selections", inputSelectHigh.ToString()+inputSelectMedium.ToString()+inputSelectLow.ToString());

		groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
		myBool = EditorGUILayout.Toggle ("Toggle", myBool);
		myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
		EditorGUILayout.EndToggleGroup ();
	}
}
﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

namespace MuninuM
{
    [CustomEditor(typeof(GenericBehavior))]
    public class GenericBehaviorEditor : Editor
    {
        //static bool globalGBSTOP = false;
        static string[] setStrings = new string[]
        {
            "Select any to add as filter:", "",
            "amount",
            //          "caninteract",
            "deltatime",
            "dechild",
            "deparent",
            "enablecaninteract",
            "enablecollider",
            "enablegameobject",
            "enablegravity",
            "enableistrigger",
            "enablekinematic",
            "enablerenderer",
            "exit",
            "forwardvector",
            "freezepositions",
            "freezerotations",
            "forceparticle",
            "globalgravity",
            "gravity",
            //          "kinematic",
            "layer",
            "lookat",
            "maincolor",
            "mass",
            "maxangmagnitude",
            "maxvelmagnitude",
            "minangmagnitude",
            "minvelmagnitude",
            "name",
            "objectpause",
            "on",
            "originalcolor",
            "othercolor",
            "parent",
            "parentit",
            "parentme",
            "pastamount",
            "position",
            "realgravity",
            "resetrotation",
            "resetangular",
            "rightvector",
            "rotate",
            "rotation",
            "rotationidentity",
            "scale",
            "scaleby",
            "specificcolor",
            "stop",
            "storedposition",
            "storedvector",
            "tag",
            "toposition",
            "torotation",
            "toscale",
            "tostoredposition",
            "translate",
            "unchild",
            "unparent",
            "upvector",
            "use_gb_fakegravity",
            "use_gb_realgravity",
            "valuestorage",
            "velocity",
        };
        static string[] getHelp = new string[]
        {
            "Select \"get\" Filter:",
            "time.full",
            "time.hour",
            "time.minute",
            "time.second",
            "date.full",
            "date.date",
            "date.day",
            "date.weekday",
            "date.dayofyear",
            "input",
            "findgameobject",
            "data",
        };

        static string[] inputOptions = new string[]
        {
            "Input Type",
            "detectOnMouseDown:",
            "detectOnMouseDrag:",
            "detectOnMouseUp:",
            "inputvariable_acceleration",
            "inputvariable_compass",
            "inputvariable_gyro",
            "inputvariable_location",
            "inputvariable_mouseposition",
            "inputvariable_mousepresent",
            "inputvariable_scrolldelta",
            "inputvariable_multitouchenabled",
            "inputvariable_touches",
            "inputvariable_touchcount",
            "joypad.axis:",
            "keyboard:",
            "mouse.axis:",
            "other:",
            "mousebutton:",

        };

        static string[] inputManagerNames = new string[]
        {
            "InputManager Variables",
            "Horizontal",
            "Vertical",
            "Fire1",
            "Fire2",
            "Fire3",
            "Jump",
            "Mouse X",
            "Mouse Y",
            "0",
            "1",
            "2",
            "replacethistext"
        };

        static string[] no_op_Options = new string[]
        {
            "Type",
            "turnoffall:",
            "turnoffon:",
            "turnoffalive:",
            "group:",
        };

        static string[] mouseOptions = new string[]
        {
            "Buttons",
            "0",
            "1",
            "2",
            "x",
        };
        static string[] joypadOptions = new string[]
        {
            "Buttons",
            "button0",
            "button1",
            "button2",
            "button3",
            "button4",
            "button5",
            "other",
        };

        static string[] compareHelp = new string[]
        {
            "Comparison -VectorStorage:Z", "Equals", "NotEquals",
            "GreaterThan", "GreaterThanOrEqual", "LesserThan", "LesserThanOrEqual",
            //      "6 = \"LesserThanOrEqual\"",
        };

        static string[] comparePrivateEvent = new string[]
        {
            "Access",
            "private",
            "event",
            //      "infoindex",
            "floatindex",
            "stringindex",
            "vectors",
        };
        static string[] getEvents = new string[]
        {
            "Data Access",
            "private",
            "event",
            "floatindex",
            "stringindex",
            "vectors",
            "axis",

            //      "PlayerInput",
        };

        static string[] toggleFunctionsList = new string[]
        {
            "Toggle attribute",
            "canInteract",
            "enablecollider",
            "enableistrigger",
            "enablegameobject",
            "enablerenderer",
            "freezepositions",
            "freezerotations",
            "forceparticle",
            "globalgravity",
            "gravity",
            "kinematic",
            "objectpause",
            "on",
            "realgravity",
            "usegravity",
        };

        static string[] loadFunctionsList = new string[]
        {
            "LOAD Details",
            "scene_default",
            "animationint",
            "animationtrigger",
            "animationbool",
            "animationfloat",
            "combinescene",
            "unload",

        };

        static string[] playFunctionsList = new string[]
        {
            "PLAY Details",
            "sound_default",
            "play",
            "sound",
            "playanimation",
            "animation:on",
            "animation:off",
            "animation:toggle",
            "animation:trigger",
            "animation:amount",
            "animation:otheramount",
            "sendmsgfunction",
        };

        static string[] SYSTEMList = new string[]
        {
            "SYSTEM Events",
            "yeild",
            "yeild_startcoroutine",
            "yeild_waitforseconds",
            "yeild_waitforfixedupdate",
            "yeild_www",
            "yeild_waitforseconds",
            "ondestroy",
        };

        static string[] transformPropertyList = new string[]
        {
            "Vectors & Transforms",
            "transform_position",
            "transform_localposition",
            "transform_rotation",
            "transform_localrotation",
            "transform_scale",
            "transform_localposition",
            "gb_vectormodifier",
            "gb_storedforce",
            "gb_storedrotation",
            "gb_storedpsition",
            "event_vectorstorage",
        };

        static string[] behaviorEvents = new string[]
        {
            "Choose from the following premade behaviors",
            "bounce",
            "custom_behavior",
            "follow",
            "lookat",
            "gravitational",
            "hover",
            "inertia",
            "sleep",
            "magnetic",
            "reverse",
            "vectorreflect",
        };

        static string[] timerEvents = new string[]
        {
            "Choose from the following premade behaviors",
            "inifinite_up",
            "inifinite_down",
            "deltatime_up",
            "deltatime_down",
            "unscaleddeltatime_up",
            "unscaleddeltatime_down",
            "custom_up",
            "custom_down",
            "fixeddeltatime_up",
            "fixeddeltatime_down",
            "fixedtime_up",
            "fixedtime_down",
            "framecount_up",
            "framecount_down",
            "maximumdeltatime_up",
            "maximumdeltatime_down",
            "time_up",
            "time_down",
        };

        static string[] comparePrivateVariables = new string[]
        {
            "Available Variables",
            "mass",
            "forceparticle",
            "valuestorage",
            "stringstorage",
            "maxvelmagnitude",
            "maxangmagnitude",
            "minvelmagnitude",
            "minangmagnitude",
            "globaltimescale",
            "colorR",
            "colorG",
            "colorB",
            "storedvelocityx",
            "storedvelocityy",
            "storedvelocityz",
            "floatindex",
            "stringindex",
        };
        static string[] compareInfoVariables = new string[]
        {
            "Available Variables",
            "mass",
            "forceparticle",
            "valuestorage",
            "stringstorage",
            "maxvelmagnitude",
            "maxangmagnitude",
            "minvelmagnitude",
            "minangmagnitude",
            "globaltimescale",
            "colorR",
            "colorG",
            "colorB",
            "storedvelocityx",
            "storedvelocityy",
            "storedvelocityz",
            "floatindex",
            "stringindex",
            "amount",
            "otheramount",
            "vectorstoragex",
            "vectorstoragey",
            "vectorstoragez",
            "word",
            "glyph",
        };
        static string[] compareEventVariables = new string[]
        {
            "Event Variables",
            "amount",
            "otheramount",
            "vectorstoragex",
            "vectorstoragey",
            "vectorstoragez",
            "word",
            "glyph",
            "msgfilter",
            "objfilter",
            "floatindex",
            "stringindex",
        };

        static string[] registerEventVariables = new string[]
        {
            "Register change to",
            "vectorstoragex",
            "vectorstoragey",
            "vectorstoragez",
            "otheramount",
            "animation.bool",
            "animation.float",
            "animation.int",
            "animation.trigger",
            "score",
            "scoremechanic",
            "valuestorage",
        };

        static string[] instantiateHelp = new string[]
        {
            "GameObject details", "aschild", "position", "uniquename",
        };

        static string[] modifyHelp = new string[]
        {
            "Variables listed available for manipulation $ (also for stringstorage)", "0 = \"Add\"", "1 = \"Multiply\"",
            "2 = \"round\"", "3 = \"AbsoluteValue\"", "4 = \"PowerOf\"", "5 = \"Square Root\"",
            "6 = \"PingPong\"", "7 = \"Lerp\"", "8 = \"Modulus\"", "9 = \"SetTo\"",
        };

        static string[] modifyHelp2 = new string[]
        {
            "OtherAmount Sets math operations;$ (also for stringstorage)", "$0 = \"Add\"", "1 = \"Multiply\"",
            "2 = \"round\"", "3 = \"AbsoluteValue\"", "4 = \"PowerOf\"", "5 = \"Square Root\"",
            "6 = \"PingPong\"", "7 = \"Lerp\"", "8 = \"Modulus\"", "$9 = \"SetTo\"",
        };


        static string[] computeHelp = new string[]
        {
            "Possible Computations:", "count", "distance", "orientation", "raycasttouch", "valuestorages", "velmagnitude",
        };

        static string[] destroyHelp = new string[]
        {
            "Destroy uses the Relation selector",
            "If by NAME, it must be able to be found in the Object Access",
            "Default or ME, destroys this calling GameObject, object filter can remain empty",
            "IT, needs the NAME of the game object written in the object filter",
            "BOTH, needs the NAME of the game object written in the object filter, destroying it first and then this game object",
            "ALL, needs the TAG of the game object written in the object filter",
            "ALL-EXCEPT, needs the TAG of the game object written in the message filter and the NAME in the object Filter",
        };

        static string[] accessFloatsHelp = new string[]
        {
            "Select to insert into Message Filter:", "mass", "force", "valuestorage", "maxvelmagnitude", "minvelmagnitude",
            "globaltimescale", "colorR", "colorG", "colorB", "storedvelocityx", "storedvelocityy", "storedvelocityz",
            "stringstorageword", "stringstorageletters", "eventsNumber.#NAMEoFeVENT.amount", "eventsNumber.#NAMEoFeVENT.otheramount",
            "eventsNumber.#NAMEoFeVENT.vectorstoragex", "eventsNumber.#NAMEoFeVENT.vectorstoragey", "eventsNumber.#NAMEoFeVENT.vectorstoragez",
        };

        static string[] sendHelp = new string[]
        {
            "Send(Console||SendMessage)",
            "debug.currentvelocity",
            "debug.forward",
            "debug.log",
            "debug.name",
            "debug.position",
            "debug.rotation",
            "debug.scale",
            "debug.stringstorage",
            "debug.tag",
            "debug.up",
            "debug.valuestorage",
            "debug.floatindex",
            "word.amount",
            "word.replaceWithFunctionName",
        };

        static string[] initHelp = new string[]
        {
            "Select to insert into Message Filter:",
            "readfile",
            "clearfloatindex",
            "clearstringindex",
            "searchstringindex",
            "searchfloatindex",
            "setstringindex",
            "setfloatindex",
            "setfloatindexrange",
            "setstringindexsize",
            "setfloatindexsize",
        };

        static string[] randomizeHelp = new string[]
        {
            "Select to insert into Message Filter:", "valuestorage", "seed", "diceroll", "vectormodifier", "storedposition",
            "velocity", "amount", "sendvaluestorage", "setvaluestoragetoamount",
            "sendamount",
        };



        static string[] imitateHelp = new string[]
        {
            "Select to insert into Message Filter:", "color", "position", "movement", "scale", "rotation",
        };
        //  static string[] forceHelp = new string[]
        //  {
        //      "FORCE" = 0,
        //      TORQUE,
        //      INERTIA,
        //      RELATIVEFORCE,
        //      RELATIVETORQUE,
        //      GRAVITATIONAL,
        //  };

        static string[] rigidbodyHelp = new string[]
        {
            "ForceMode:", "acceleration", "force", "impulse", "velocitychange", "stop", "set",
        };

        static string[] vectorHelp = new string[]
        {
            "Vector tricks:", "backwards", "movetowards", "reverse",
        };

        bool turnOnDelete = false;
        bool turnOnHelp = true;

        bool objNeed = false;
        bool msgNeed = false;
        bool amtNeed = false;
        bool oamtNeed = false;
        bool vectorNeed = false;
        bool wordNeed = false;

        bool objDisplay = true;
        bool msgDisplay = true;
        bool amtDisplay = true;
        bool oamtDisplay = true;
        bool vectorDisplay = true;
        bool wordDisplay = true;
        bool glyphDisplay = true;
        bool keyDisplay = true;
        bool filterDisplay = true;
        bool relationDisplay = true;
        bool forceTypeDisplay = true;
        bool eventListDisplay = false;
        bool objListDisplay = false;
        bool groupDisplay = false;
        bool onDisplay = false;

        //bool eventPositionDisplay = false;
        static bool globalMessage = false;

        int detectChange = 0;

        int comparePhrase0 = 0;
        int comparePhrase1 = 0;
        int comparePhrase2 = 0;
        int comparePhrase3 = 0;
        int comparePhraseInto0 = 0;
        int comparePhraseInto1 = 0;
        int comparePhraseInto2 = 0;
        int comparePhraseInto3 = 0;

        string compareSentence = string.Empty;
        string compareSentenceInto = string.Empty;
        string messageLabel = "#Message/Filter";
        string wordLabel = "Word";
        string onLabel = "On";
        string messageLabelTooltip = "#Message/Filter";
        string objectLabel = "GameObject:";
        string objectLabelTooltip = "#Message/Filter";
        string windowLabelTooltip = "(GenericBehaviorEvent)";
        string eventSearchString = "";
        string windowLabel = "";
        string documentationLink = "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub";
        string tStr = "";
        char[] delimiterComma = { ',' };
        char[] delimiterHash = { '#' };
        char[] delimiterPipes = { '\n', '\t' };
        string[] eventSplits = { "" };

        int selectedPopup = 0;
        int iOptionsSelection = 0;
        int iManagerSelection = 0;

        SerializedProperty eventListProp;

        SerializedProperty behaviorNotesProp;
        SerializedProperty showPrivatesProp;
        SerializedProperty globalStopGBProp;

        SerializedProperty forceParticleProp;
        SerializedProperty dontDestroyProp;
        SerializedProperty valueStorageProp;
        SerializedProperty stringStorageProp;
        SerializedProperty storedForcesProp;
        SerializedProperty storedRotationProp;
        SerializedProperty storedPositionProp;
        SerializedProperty objectPauseProp;
        SerializedProperty canInteractBoolProp;
        SerializedProperty vectorModifierProp;
        SerializedProperty mainColorProp;
        SerializedProperty otherColorProp;

        SerializedProperty useRealGravityBoolProp;
        SerializedProperty realGravityProp;

        GUIStyle _mine;

        SerializedProperty showEventProp;
        SerializedProperty showPositionProp;

        SerializedProperty objectAccessProp;

        private GenericBehavior gb = null;

        protected List<bool>[] eventFoldouts;

        public List<bool> toggles;
        // = new bool[list.arraySize];

        //      private void GenericBehaviorGlobalsList(SerializedProperty list, bool showListSize = true)
        //      {
        //          EditorGUILayout.PropertyField(list);
        //          if (list.isExpanded)
        //          {
        //              EditorGUILayout.BeginVertical("Box");
        //              if (showListSize)
        //              {
        //                  EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
        //              }
        //              for (int i = 0; i < list.arraySize; i++)
        //              {
        //                  EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
        //              }
        //              EditorGUILayout.EndVertical();
        //          }
        //      }
        //
        void nothing(int i)
        {
        }

        private void initStyles()
        {
            _mine = new GUIStyle();
            _mine.wordWrap = true;
            _mine.stretchHeight = true;
            _mine.fontSize = 10;
        }

        void OnEnable()
        {

            //initStyles();

            if (globalMessage == false)
            {
                //    EditorUtility.DisplayDialog("Thanks for using the Generic Behavior", "Known Bugs:ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint... please ignore as it will be eliminated in future versions of the Generic Behavior", "Ok");
                Debug.Log("Known Bugs:ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint... please ignore as it will be eliminated in future versions of the Generic Behavior");
                globalMessage = true;
            }
            eventListProp = serializedObject.FindProperty("eventList");

            behaviorNotesProp = serializedObject.FindProperty("behaviorNotes");

            objectPauseProp = serializedObject.FindProperty("objectPause");
            canInteractBoolProp = serializedObject.FindProperty("canInteract");

            //      realGravityProp = serializedObject.FindProperty("realGravity");
            //      useRealGravityBoolProp = serializedObject.FindProperty("useRealGravity");

            valueStorageProp = serializedObject.FindProperty("valueStorage");
            forceParticleProp = serializedObject.FindProperty("forceParticle");
            showPrivatesProp = serializedObject.FindProperty("expandPrivates");
            globalStopGBProp = serializedObject.FindProperty("globalStopGB");

            dontDestroyProp = serializedObject.FindProperty("dontDestroy");
            stringStorageProp = serializedObject.FindProperty("stringStorage");
            vectorModifierProp = serializedObject.FindProperty("vectorModifier");
            storedForcesProp = serializedObject.FindProperty("storedForces");
            storedRotationProp = serializedObject.FindProperty("storedRotation");
            storedPositionProp = serializedObject.FindProperty("storedPosition");

            mainColorProp = serializedObject.FindProperty("mainColor");
            otherColorProp = serializedObject.FindProperty("otherColor");
            objectAccessProp = serializedObject.FindProperty("objectAccess");


            gb = (GenericBehavior)target;
        }

        public override void OnInspectorGUI()
        {

            serializedObject.Update();
            GUI.color = new Color(1, 1, 1, 1f);//, Color.white,Time.deltaTime);
            EditorGUILayout.Separator();
            int id = 10;
            //1_VB'



            GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(false));
            {
                id = 0;
                //EditorGUILayout.BeginHorizontal();


                EditorGUILayout.PropertyField(showPrivatesProp);
                EditorGUILayout.LabelField("Behavior Notes");
                //EditorGUILayout.EndHorizontal();


                EditorStyles.textArea.wordWrap = true;
                EditorStyles.textField.wordWrap = true;

                behaviorNotesProp.stringValue = EditorGUILayout.TextArea(behaviorNotesProp.stringValue, GUILayout.ExpandWidth(true));
                GUI.color = Color.white;
                if (behaviorNotesProp.stringValue == string.Empty)
                {
                    behaviorNotesProp.stringValue = "EMPTY BEHAVIOR DESCRIPTION!";
                    serializedObject.ApplyModifiedProperties();
                    GUI.color = Color.red;
                }
                EditorGUILayout.PropertyField(canInteractBoolProp);

                if (showPrivatesProp.boolValue == true)
                {

                    //                    EditorGUILayout.PropertyField(globalStopGBProp);
                    //
                    //                    EditorStyles.textField.normal.textColor = new Color(0f, 0, 0, 1f);
                    //                    //gb.globalStopGB = globalGBSTOP; 
                    //                    globalGBSTOP = globalStopGBProp.boolValue & globalGBSTOP;
                    //                    if (globalStopGBProp.boolValue == true)
                    //                    {
                    //                        
                    //                        //GUI.color = new Color(1f, 0, 0, 1f);
                    //                        EditorStyles.textField.normal.textColor = new Color(1f, 0, 0, 1f);
                    //                    }
                    EditorGUILayout.PropertyField(dontDestroyProp);


                    EditorGUILayout.PropertyField(valueStorageProp);
                    EditorGUILayout.PropertyField(stringStorageProp);
                    //1a_HB
                    //EditorGUILayout.BeginHorizontal();
                    //EditorGUILayout.LabelField("Vector Modifier", GUILayout.ExpandWidth(false), GUILayout.Width(120));
                    EditorGUILayout.PropertyField(vectorModifierProp);//, GUIContent.none, GUILayout.ExpandWidth(true));
                    //EditorGUILayout.Space();
                    //EditorGUILayout.EndHorizontal();
                    //1a_HE
                    //1a_HB
                    //EditorGUILayout.BeginHorizontal();
                    //EditorGUILayout.LabelField("Stored Force", GUILayout.ExpandWidth(false), GUILayout.Width(120));
                    EditorGUILayout.PropertyField(storedForcesProp);//, GUIContent.none, GUILayout.ExpandWidth(true));
                    //EditorGUILayout.EndHorizontal();
                    //1a_HE
                    //1a_HB
                    //EditorGUILayout.BeginHorizontal();
                    //EditorGUILayout.LabelField("Stored Rotation", GUILayout.ExpandWidth(false), GUILayout.Width(120));
                    EditorGUILayout.PropertyField(storedRotationProp);//, GUIContent.none, GUILayout.ExpandWidth(true));
                    //EditorGUILayout.EndHorizontal();
                    //1a_HE
                    //1a_HB
                    //EditorGUILayout.BeginHorizontal();
                    //EditorGUILayout.LabelField("Stored Position", GUILayout.ExpandWidth(false), GUILayout.Width(120));
                    EditorGUILayout.PropertyField(storedPositionProp);//, GUIContent.none, GUILayout.ExpandWidth(true));
                    //EditorGUILayout.EndHorizontal();
                    //1a_He
                    EditorGUILayout.PropertyField(forceParticleProp);
                    EditorGUILayout.PropertyField(mainColorProp);
                    EditorGUILayout.PropertyField(otherColorProp);
                }
            }


            EditorGUILayout.EndVertical();
            //1_VE

            GUI.color = new Color(.85f, .85f, .85f, 1f);

            //1_VB
            EditorGUILayout.BeginVertical("Box");
            ObjectAccesList(objectAccessProp, true);
            EditorGUILayout.EndVertical();
            //1_VE
            GUI.color = new Color(.7f, .7f, .7f, 1f);
            //1_VB
            EditorGUILayout.BeginVertical("Box");
            EventList(eventListProp, true);
            EditorGUILayout.EndVertical();
            //1_VE

            serializedObject.ApplyModifiedProperties();
        }

        int selId = 0;
        int selEventId = 0;

        private void EventList(SerializedProperty list, bool showListSize = true)
        {

            EditorGUILayout.PropertyField(list);

            if (list.isExpanded)
            {

                string[] popOptions = null;
                string[] popOptions1 = null;
                if (gb.objectAccess.Length > 0)
                {

                    popOptions = new string[gb.objectAccess.Length + 2];


                    popOptions1 = new string[gb.objectAccess.Length];
                    popOptions[0] = "Select GameObject";
                    popOptions[1] = "this.gameObject";
                    for (int i = 0; i < popOptions1.Length; i++)
                    {

                        if (gb.objectAccess[i] == null)
                        {
                            popOptions1[i] = "None (Game Object)";
                        }
                        else
                        {
                            popOptions1[i] = gb.objectAccess[i].name;
                        }
                    }
                    for (int i = 2; i < popOptions.Length; i++)
                    {
                        popOptions[i] = popOptions1[i - 2];
                    }


                }
                else
                {
                    popOptions = new string[gb.objectAccess.Length + 2];

                    popOptions[0] = "Empty:Object Access List";
                    popOptions[1] = "gameObject";

                }

                string[] popEventOptions = null;
                if (gb.eventList.Count > 0)
                {
                    popEventOptions = new string[gb.eventList.Count + 1];
                    popEventOptions[0] = "Choose message as a #Event";
                    for (int i = 1; i < popEventOptions.Length; i++)
                    {
                        if (gb.eventList[i - 1] == null)
                        {
                            popEventOptions[i] = "EMPTY";
                        }
                        else
                        {
                            popEventOptions[i] = "#" + gb.eventList[i - 1].eventName;
                        }
                    }
                }

                List<string> popNothingList = null;
                popNothingList = new List<string>();
                //popNothingList.Add("Group to NOTHING&_NO_OP #Events");
                popNothingList.Add("Group in N&N #Event ");
                popNothingList.Add("Clear Group");
                if (gb.eventList.Count > 0)
                {
                    for (int i = 1; i < popEventOptions.Length; i++)
                    {
                        if (gb.eventList[i - 1].playerAction == PlayerFunctions._NOTHING && gb.eventList[i - 1].gameAction == GameFunctions._NO_OP)
                            //popNothingOptions[i] = "#" + gb.eventList[i - 1].eventName;
                            popNothingList.Add("#" + gb.eventList[i - 1].eventName);
                    }
                }


                string[] popNothingOptions = null;
                if (gb.eventList.Count > 0)
                {
                    popNothingOptions = new string[gb.eventList.Count + 1];
                    popNothingOptions[0] = "Group to NOTHING+_NO_OP #Event";
                    popNothingOptions[1] = "Clear Group Belonging";
                    for (int i = 1; i < popNothingOptions.Length; i++)
                    {
                        if (gb.eventList[i - 1] == null)
                        {
                            popNothingOptions[i] = "EMPTY";
                        }
                        else
                        {
                            if (gb.eventList[i - 1].playerAction == PlayerFunctions._NOTHING && gb.eventList[i - 1].gameAction == GameFunctions._NO_OP)
                                popNothingOptions[i] = "#" + gb.eventList[i - 1].eventName;
                        }
                    }
                }

                documentationLink = "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub";

                //1_VB

                EditorGUILayout.BeginVertical("Box");
                {
                    GUI.color = Color.white;
                    EditorGUILayout.LabelField("Filter Events");
                    EditorGUILayout.BeginHorizontal();
                    {
                        eventSearchString = EditorGUILayout.TextArea(eventSearchString, GUILayout.ExpandWidth(true));
                        if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false)))
                        {
                            eventSearchString = string.Empty;
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginVertical("Box");
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUI.color = new Color(.8f, .8f, .8f, 1f);
                            if (eventListProp.arraySize > 0)
                            {
                                GUI.color = Color.cyan;
                                if (GUILayout.Button(new GUIContent("@Help", "Go to the online Documentation"), GUILayout.ExpandWidth(false)))
                                {
                                    Application.OpenURL(documentationLink);
                                    serializedObject.ApplyModifiedProperties();
                                }
                                GUI.color = new Color(.8f, .8f, .8f, 1f);
                                if (turnOnDelete)
                                {
                                    GUI.color = Color.red;
                                }
                                if (GUILayout.Button(new GUIContent("Delete", "Enable the Delete Button to delete 1 Event [X]. "), GUILayout.ExpandWidth(false)))
                                {
                                    turnOnDelete = !turnOnDelete;
                                    serializedObject.ApplyModifiedProperties();
                                }
                                GUI.color = new Color(.8f, .8f, .8f, 1f);
                                //                  if (GUILayout.Button(new GUIContent("Full", "Expand and show all Events' Properties"), GUILayout.ExpandWidth(false)))
                                //                  {
                                //                      for (int i = 0; i < list.arraySize; i++)
                                //                      {
                                //                          list.GetArrayElementAtIndex(i).FindPropertyRelative("showEvent").boolValue = true;
                                //                          turnOnHelp = !turnOnHelp;
                                //                      }
                                //                      serializedObject.ApplyModifiedProperties();
                                //                  }                   
                                if (GUILayout.Button(new GUIContent("Mini", "Collapse all Events' Properties to glance at the entire set of behaviors"), GUILayout.ExpandWidth(false)))
                                {
                                    for (int i = 0; i < list.arraySize; i++)
                                    {
                                        if (eventSearchString != string.Empty && !gb.eventList[i].eventName.ToLower().Contains(eventSearchString.ToLower()))
                                        {
                                            continue;
                                        }
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("showEvent").boolValue = false;
                                    }
                                    serializedObject.ApplyModifiedProperties();
                                }

                                //                                if (GUILayout.Button(new GUIContent("->", "Collapse all Events' Properties to glance at the entire set of behaviors"), GUILayout.ExpandWidth(false)))
                                //                                {
                                //                                    GBNodeEditor editor = EditorWindow.GetWindow<GBNodeEditor>();
                                //
                                //                                }
                                if (GUILayout.Button(new GUIContent("Toggle", "Toggle Active status of each event"), GUILayout.ExpandWidth(false)))
                                {
                                    for (int i = 0; i < list.arraySize; i++)
                                    {
                                        if (eventSearchString != string.Empty && !gb.eventList[i].eventName.ToLower().Contains(eventSearchString.ToLower()))
                                        {
                                            continue;
                                        }
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue = !list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue;
                                    }
                                    serializedObject.ApplyModifiedProperties();
                                }
                                if (GUILayout.Button(new GUIContent((turnOnHelp ? "¿" : "?"), "Expand and show all Events' Properties"), GUILayout.ExpandWidth(false)))
                                {
                                    turnOnHelp = !turnOnHelp;
                                    serializedObject.ApplyModifiedProperties();
                                }
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        {
                            if (GUILayout.Button(new GUIContent("New Event", "Add a new, blank, event to this Generic Behavior"), GUILayout.ExpandWidth(true)))
                            {
                                GameEvent tEvent = new GameEvent();
                                tEvent.eventName += eventListProp.arraySize.ToString();
                                eventListProp.InsertArrayElementAtIndex(0);
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("eventName").stringValue = tEvent.eventName;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("amount").floatValue = tEvent.amount;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("otherAmount").floatValue = tEvent.otherAmount;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("vectorStorage").vector3Value = tEvent.vectorStorage;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("showEvent").boolValue = tEvent.showEvent;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("alive").boolValue = tEvent.alive;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("on").boolValue = tEvent.on;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("letters").stringValue = tEvent.letters;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("word").stringValue = tEvent.word;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("msgFilter").stringValue = tEvent.msgFilter;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("objFilter").stringValue = tEvent.objFilter;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("group").stringValue = tEvent.group;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("force").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("filterType").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("gameAction").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("playerAction").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("relation").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("key").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("inputType").enumValueIndex = 0;
                                eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("inputString").stringValue = tEvent.inputString;
                                serializedObject.ApplyModifiedProperties();
                            }


                            tStr = EditorGUIUtility.systemCopyBuffer;
                            //delimiterComma = { ',' };
                            //delimiterHash = { '#' };
                            eventSplits = tStr.Split(delimiterComma);

                            if (tStr.Contains("C0P1_BUFF3R_G3N3R1C_B3H4V10R") && eventSplits[0] == "C0P1_BUFF3R_G3N3R1C_B3H4V10R")
                            {
                                if (GUILayout.Button(new GUIContent("Paste", "Paste an event in the clipboard into this Generic Behavior Component"), GUILayout.ExpandWidth(false)))
                                {
                                    GameEvent tEvent = new GameEvent();
                                    tEvent.amount = float.Parse(eventSplits[1]);
                                    tEvent.otherAmount = float.Parse(eventSplits[2]);
                                    Vector3 tVec = new Vector3(float.Parse(eventSplits[3]), float.Parse(eventSplits[4]), float.Parse(eventSplits[5]));
                                    tEvent.vectorStorage = tVec;
                                    tEvent.filterType = (ObjectNameType)System.Enum.Parse(typeof(ObjectNameType), eventSplits[6]);
                                    tEvent.gameAction = (GameFunctions)System.Enum.Parse(typeof(GameFunctions), eventSplits[7]);
                                    tEvent.playerAction = (PlayerFunctions)System.Enum.Parse(typeof(PlayerFunctions), eventSplits[8]);
                                    tEvent.relation = (ObjectRelation)System.Enum.Parse(typeof(ObjectRelation), eventSplits[9]);
                                    tEvent.inputType = (InteractionType)System.Enum.Parse(typeof(InteractionType), eventSplits[10]);
                                    tEvent.key = (KeyCode)System.Enum.Parse(typeof(KeyCode), eventSplits[11]);
                                    tEvent.inputString = eventSplits[12];
                                    tEvent.group = eventSplits[13];
                                    tEvent.force = (ObjectForce)System.Enum.Parse(typeof(ObjectForce), eventSplits[14]);
                                    tEvent.on = (eventSplits[15] == "True");
                                    tEvent.alive = (eventSplits[16] == "True");
                                    tEvent.showEvent = (eventSplits[17] == "True");
                                    tEvent.letters = eventSplits[18];
                                    tEvent.eventName = eventSplits[19] + (eventListProp.arraySize + 1).ToString();
                                    tEvent.word = eventSplits[20];
                                    tEvent.msgFilter = eventSplits[21];
                                    tEvent.msgFilter = tEvent.msgFilter.Replace("|", ",");
                                    tEvent.objFilter = eventSplits[22];
                                    tEvent.objFilter = tEvent.objFilter.Replace("|", ",");

                                    eventListProp.InsertArrayElementAtIndex(0);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("amount").floatValue = tEvent.amount;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("otherAmount").floatValue = tEvent.otherAmount;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("vectorStorage").vector3Value = tEvent.vectorStorage;
                                    int tInt = 0;
                                    tInt = (int)System.Enum.Parse(typeof(ObjectNameType), eventSplits[6]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("filterType").enumValueIndex = tInt;//enumNames[prop.FindPropertyRelative("fieldType").enumValueIndex].ToString()
                                    tInt = (int)System.Enum.Parse(typeof(GameFunctions), eventSplits[7]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("gameAction").enumValueIndex = tInt;
                                    tInt = (int)System.Enum.Parse(typeof(PlayerFunctions), eventSplits[8]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("playerAction").enumValueIndex = tInt;
                                    tInt = (int)System.Enum.Parse(typeof(ObjectRelation), eventSplits[9]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("relation").enumValueIndex = tInt;

                                    tInt = (int)System.Enum.Parse(typeof(InteractionType), eventSplits[10]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("inputType").enumValueIndex = tInt;//System.Convert.ToInt32(eventSplits[10]);

                                    tInt = (int)System.Enum.Parse(typeof(KeyCode), eventSplits[11]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("key").enumValueIndex = tInt;//System.Convert.ToInt32(eventSplits[11]);

                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("inputString").stringValue = tEvent.inputString;

                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("group").stringValue = tEvent.group;
                                    tInt = (int)System.Enum.Parse(typeof(ObjectForce), eventSplits[14]);
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("force").enumValueIndex = tInt;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("on").boolValue = tEvent.on;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("alive").boolValue = tEvent.alive;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("showEvent").boolValue = tEvent.showEvent;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("letters").stringValue = tEvent.letters;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("eventName").stringValue = tEvent.eventName;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("objFilter").stringValue = tEvent.objFilter;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("msgFilter").stringValue = tEvent.msgFilter;
                                    eventListProp.GetArrayElementAtIndex(0).FindPropertyRelative("word").stringValue = tEvent.word;
                                    detectChange++;
                                    serializedObject.ApplyModifiedProperties();
                                }
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndVertical();


                for (int i = 0; i < list.arraySize; i++)
                {

                    if (eventSearchString != string.Empty && !gb.eventList[i].eventName.ToLower().Contains(eventSearchString.ToLower()))
                    {
                        continue;
                    }

                    SerializedProperty prop = list.GetArrayElementAtIndex(i);
                    SerializedProperty playerActionProp = list.GetArrayElementAtIndex(i).FindPropertyRelative("playerAction");
                    GUI.color = Color.gray;// + Color.black;
                    //Initialize all flags to false before running the details on each switch 
                    //  This is needed so that each event has its initial values independent from each other in the GB
                    objNeed = false;
                    msgNeed = false;
                    amtNeed = false;
                    oamtNeed = false;
                    vectorNeed = false;
                    objDisplay = false;
                    msgDisplay = false;
                    amtDisplay = false;
                    oamtDisplay = false;
                    vectorDisplay = false;
                    wordDisplay = true;
                    glyphDisplay = false;
                    keyDisplay = false;
                    filterDisplay = false;
                    relationDisplay = false;
                    objListDisplay = false;
                    eventListDisplay = false;
                    groupDisplay = false;
                    onDisplay = false;
                    //eventPositionDisplay = false;

                    Color colorStore = GUI.color;
                    Color colorStoreDeactivated = new Color(0, 0, 0, .6f);
                    onLabel = "On";
                    wordLabel = "string";
                    switch ((PlayerFunctions)playerActionProp.enumValueIndex)
                    {
                        case PlayerFunctions._NOTHING:
                            GUI.color = Color.grey;
                            onLabel = "Show Group";
                            break;
                        case PlayerFunctions.INPUTPRESS:
                        case PlayerFunctions.INPUTDOWN:
                        case PlayerFunctions.INPUTANY:
                        case PlayerFunctions.INPUTUP:
                            GUI.color = new Color(.345f, .827f, .471f, 1);

                            break;
                        case PlayerFunctions.COLLISIONENTER:
                        case PlayerFunctions.COLLISIONEXIT:
                        case PlayerFunctions.COLLISIONSTAY:
                            GUI.color = new Color(1, .486f, .42f, 1);
                            objNeed = true;
                            break;

                        case PlayerFunctions.TRIGGERENTER:
                        case PlayerFunctions.TRIGGEREXIT:
                        case PlayerFunctions.TRIGGERSTAY:
                            GUI.color = new Color(0, .675f, 1, 1);
                            objNeed = true;
                            break;
                        case PlayerFunctions.MESSAGE_EVENT:
                            GUI.color = new Color(1, .831f, .345f, 1);
                            break;
                        case PlayerFunctions.AWAKE:
                        case PlayerFunctions.START:
                            GUI.color = new Color(.984f, 1, .341f, 1);
                            break;
                        case PlayerFunctions.SYSTEM:
                            GUI.color = new Color(1.1f, 1.1f, 1.1f, 1.1f);
                            break;
                        case PlayerFunctions.UPDATE:
                            GUI.color = new Color(.663f, .376f, .953f, 1);
                            break;
                        case PlayerFunctions.UPDATEFIXED:
                            GUI.color = new Color(.863f, .476f, .953f, 1);
                            break;
                        case PlayerFunctions.UPDATELATE:
                            GUI.color = new Color(.763f, .576f, .953f, 1);
                            break;
                    }

                    string groupAssignment = gb.eventList[i].group;
                    string groupWOHash = string.Empty;//gb.eventList[i].group;
                    bool showGrouped = true;
                    if (groupAssignment != string.Empty)
                    {
                        string _str = groupAssignment;
                        int tInt = _str.IndexOf('#') + 1;

                        if (tInt > 0)
                        {
                            _str = _str.Substring(tInt);
                            groupWOHash = _str;
                            //                          currentEvent = messageList[_str];
                        }
                        for (int l = 0; l < gb.eventList.Count; l++)
                        {

                            if (gb.eventList[l].eventName == _str)
                            {

                                if (gb.eventList[l].on == false)
                                    showGrouped = false;
                            }
                        }
                    }

                    if (!showGrouped)
                        continue;

                    colorStore = GUI.color;
                    if (!gb.eventList[i].alive)
                    {
                        GUI.color -= colorStoreDeactivated;
                    }

                    switch ((GameFunctions)gb.eventList[i].gameAction)
                    {
                        case GameFunctions.COMPARE_DATA:
                            objNeed = true;
                            msgNeed = true;
                            amtNeed = true;
                            forceTypeDisplay = false;
                            relationDisplay = false;
                            filterDisplay = false;
                            msgDisplay = true;
                            eventListDisplay = true;
                            objDisplay = true;
                            amtDisplay = true;
                            vectorDisplay = true;
                            oamtDisplay = true;
                            wordDisplay = true;
                            messageLabel = "#Event to call";
                            objectLabel = "Compare script";
                            windowLabelTooltip = "(" + "COMPARE: " + "" + ")";
                            break;
                        case GameFunctions.GET_MOVE_DATA:
                            objNeed = true;
                            msgNeed = true;
                            amtNeed = false;
                            oamtNeed = false;
                            vectorNeed = false;
                            objDisplay = true;
                            wordDisplay = false;
                            glyphDisplay = false;
                            objListDisplay = false;
                            filterDisplay = false;
                            msgDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            vectorDisplay = true;
                            messageLabel = "Select";
                            forceTypeDisplay = false;
                            eventListDisplay = true;
                            objectLabel = "Get Statement";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";

                            break;
                        case GameFunctions._NO_OP:
                            objNeed = false;
                            msgNeed = false;
                            amtNeed = false;
                            oamtNeed = false;
                            vectorNeed = false;
                            keyDisplay = false;
                            objDisplay = false;
                            msgDisplay = false;
                            amtDisplay = false;
                            oamtDisplay = false;
                            wordDisplay = false;
                            glyphDisplay = false;
                            filterDisplay = false;
                            vectorDisplay = false;
                            objListDisplay = false;
                            relationDisplay = false;
                            forceTypeDisplay = false;
                            eventListDisplay = false;
                            if (gb.eventList[i].on || gb.eventList[i].group != string.Empty)
                            {
                                msgDisplay = true;
                                //eventListDisplay = true;
                            }
                            //eventListDisplay = msgDisplay = gb.eventList[i].showEvent;
                            messageLabel = "list #events in group";
                            //prop.FindPropertyRelative("on").boolValue;
                            windowLabelTooltip =
                                "(NO OPERATION:" + "Does nothing other than turn on the event based on the PLAYER ACTION." + ")";
                            break;
                        case GameFunctions.TIMER:
                            objNeed = false;
                            msgNeed = true;
                            amtNeed = true;
                            oamtNeed = true;
                            amtDisplay = true;
                            msgDisplay = true;
                            vectorNeed = true;
                            oamtDisplay = true;
                            objDisplay = false;
                            wordDisplay = true;
                            vectorDisplay = true;
                            glyphDisplay = false;
                            filterDisplay = false;
                            relationDisplay = false;
                            eventListDisplay = true;
                            forceTypeDisplay = false;
                            //objListDisplay = false;
                            messageLabel = "#Event to call";
                            wordLabel = "word";

                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + " " +
                            "Runs a countdown timer that when its vectorstorage.x reaches 0 calls #event(s) listed in the messageFilter." + ")";
                            break;
                        case GameFunctions.SET:
                            objNeed = false;
                            msgNeed = true;
                            amtNeed = true;
                            oamtNeed = true;
                            vectorNeed = true;
                            objDisplay = true;
                            wordDisplay = true;
                            glyphDisplay = true;
                            objListDisplay = true;
                            msgDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            vectorDisplay = true;
                            filterDisplay = true;
                            relationDisplay = true;
                            messageLabel = "Which Filter";
                            objectLabel = "Which Object:";
                            //eventListDisplay = false;
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.COMPUTE:
                            objNeed = true;
                            msgNeed = true;
                            objDisplay = true;
                            wordDisplay = false;
                            glyphDisplay = true;
                            objListDisplay = true;
                            eventListDisplay = false;
                            msgDisplay = true;
                            amtDisplay = true;
                            glyphDisplay = false;
                            messageLabel = "Compute What?";
                            objectLabel = "Which Object/Tag";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.IF_EVENT_ON:
                            objNeed = false;
                            msgNeed = true;
                            objDisplay = true;
                            vectorNeed = false;
                            vectorDisplay = false;
                            wordDisplay = false;
                            glyphDisplay = false;
                            msgDisplay = true;
                            objListDisplay = true;
                            eventListDisplay = true;
                            messageLabel = "#Events=#ResultEvent";
                            break;
                        case GameFunctions.INFO_IO:
                            objNeed = false;
                            msgNeed = true;
                            amtNeed = true;
                            oamtNeed = true;
                            vectorNeed = true;
                            objDisplay = true;
                            wordDisplay = true;
                            glyphDisplay = true;
                            objListDisplay = true;
                            msgDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            vectorDisplay = true;
                            messageLabel = "Which Filter";
                            objectLabel = "Which Object:";
                            //eventListDisplay = false;
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.CALL_EVENT:
                            msgNeed = true;
                            amtDisplay = true;
                            oamtDisplay = false;
                            vectorNeed = false;
                            vectorDisplay = false;
                            wordDisplay = false;
                            glyphDisplay = false;
                            filterDisplay = false;
                            relationDisplay = true;
                            keyDisplay = false;
                            forceTypeDisplay = false;
                            objDisplay = true;
                            objListDisplay = true;
                            msgDisplay = true;
                            eventListDisplay = true;
                            messageLabel = "#Event to call:";
                            objectLabel = "Relevant Object";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.TOGGLE_EVENT:
                            msgNeed = true;
                            amtDisplay = true;
                            amtNeed = true;
                            oamtNeed = true;
                            oamtDisplay = true;
                            vectorNeed = false;
                            vectorDisplay = false;
                            wordDisplay = false;
                            glyphDisplay = false;
                            filterDisplay = true;
                            relationDisplay = true;
                            keyDisplay = false;
                            forceTypeDisplay = false;
                            objDisplay = true;
                            objListDisplay = true;
                            msgDisplay = true;
                            eventListDisplay = true;
                            messageLabel = "#Event to Toggle:";
                            objectLabel = "Relevant Object";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;

                        case GameFunctions.SEND_MSG:
                            msgNeed = true;
                            amtDisplay = false;
                            oamtDisplay = false;
                            vectorNeed = false;
                            vectorDisplay = false;
                            wordDisplay = true;
                            glyphDisplay = false;
                            filterDisplay = false;
                            relationDisplay = false;
                            amtDisplay = true;
                            keyDisplay = false;
                            forceTypeDisplay = false;
                            objDisplay = true;
                            objListDisplay = true;
                            msgDisplay = true;
                            eventListDisplay = true;
                            messageLabel = "Send output:";
                            objectLabel = "Obj.SendMessage(func,amt)";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.LOAD_SCENE:
                            msgNeed = true;
                            amtDisplay = false;
                            oamtDisplay = false;
                            vectorNeed = false;
                            wordDisplay = false;
                            glyphDisplay = false;
                            objListDisplay = false;
                            eventListDisplay = false;
                            vectorDisplay = false;
                            objDisplay = false;
                            msgDisplay = true;
                            forceTypeDisplay = false;
                            filterDisplay = false;
                            relationDisplay = false;
                            messageLabel = "Load parameter:";
                            break;
                        case GameFunctions.DESTROY:
                            objNeed = true;
                            relationDisplay = true;
                            filterDisplay = true;
                            wordDisplay = false;
                            glyphDisplay = false;
                            objDisplay = true;
                            objListDisplay = true;
                            forceTypeDisplay = false;
                            msgDisplay = false;
                            messageLabel = "Filter:";
                            objectLabel = "Destroy Object:";
                            //eventListDisplay = false;
                            break;
                        case GameFunctions.MODIFY_VALUES:
                            msgNeed = true;
                            oamtNeed = true;
                            objListDisplay = true;
                            objDisplay = true;
                            eventListDisplay = false;
                            oamtDisplay = true;
                            amtDisplay = true;
                            msgDisplay = true;
                            forceTypeDisplay = false;
                            messageLabel = "Which Variable";
                            objectLabel = "Object:";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.RANDOMIZE:
                            msgNeed = true;
                            amtNeed = true;
                            oamtNeed = true;
                            objListDisplay = false;
                            eventListDisplay = false;
                            forceTypeDisplay = false;
                            msgDisplay = true;
                            wordDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            objDisplay = true;
                            objListDisplay = true;
                            //list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = "";
                            break;
                        case GameFunctions.BEHAVIOR:
                            objNeed = true;
                            msgNeed = false;
                            amtNeed = true;
                            oamtNeed = true;
                            vectorNeed = true;
                            objDisplay = true;
                            wordDisplay = true;
                            glyphDisplay = false;
                            objListDisplay = true;
                            forceTypeDisplay = true;
                            msgDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            vectorDisplay = true;
                            messageLabel = "Perform:";
                            eventListDisplay = false;
                            objectLabel = "Against:";
                            wordLabel = "Forcemode";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;

                        case GameFunctions.IMITATE:
                            objNeed = true;
                            msgNeed = true;
                            relationDisplay = false;
                            filterDisplay = false;
                            msgDisplay = true;
                            objDisplay = true;
                            objListDisplay = true;
                            forceTypeDisplay = false;
                            vectorNeed = true;
                            vectorDisplay = true;
                            amtNeed = true;
                            amtDisplay = true;
                            wordDisplay = false;
                            glyphDisplay = false;
                            messageLabel = "Imitate What";
                            objectLabel = "From Object(Name):";
                            break;
                        case GameFunctions.INSTANTIATE:
                            msgNeed = false;
                            amtDisplay = false;
                            oamtDisplay = false;
                            vectorNeed = false;
                            vectorDisplay = true;
                            wordDisplay = true;
                            glyphDisplay = false;
                            filterDisplay = false;
                            relationDisplay = true;
                            keyDisplay = false;
                            forceTypeDisplay = false;
                            objDisplay = true;
                            objListDisplay = true;
                            msgDisplay = true;
                            eventListDisplay = true;
                            amtDisplay = true;

                            messageLabel = "Instantiate flags";
                            objectLabel = "Object to Create";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";
                            break;
                        case GameFunctions.PLAY:
                            objNeed = true;
                            msgNeed = true;
                            amtNeed = true;
                            oamtNeed = true;
                            vectorNeed = true;
                            objDisplay = true;
                            msgDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            wordDisplay = true;
                            glyphDisplay = false;
                            vectorDisplay = false;
                            objListDisplay = true;
                            forceTypeDisplay = false;
                            eventListDisplay = false;
                            wordLabel = "Mode:";
                            messageLabel = "Which Asset";
                            objectLabel = "GameObject/Component:";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";

                            break;
                        case GameFunctions.PAUSE:
                            objNeed = true;
                            //msgNeed = true;
                            amtNeed = true;
                            amtDisplay = true;
                            wordDisplay = false;
                            glyphDisplay = false;
                            filterDisplay = false;
                            relationDisplay = true;
                            forceTypeDisplay = false;
                            msgDisplay = true;
                            eventListDisplay = true;
                            //wordLabel = "Mode:";
                            messageLabel = "Pause Type";
                            objectLabel = "GameObject/Component:";
                            break;
                        case GameFunctions.MOTION:
                            msgNeed = true;
                            amtNeed = true;
                            vectorNeed = true;
                            amtDisplay = true;
                            msgDisplay = true;
                            objDisplay = false;
                            wordDisplay = false;
                            wordDisplay = false;
                            glyphDisplay = false;
                            vectorDisplay = true;
                            glyphDisplay = false;
                            filterDisplay = false;
                            objListDisplay = false;
                            forceTypeDisplay = true;
                            relationDisplay = false;
                            eventListDisplay = false;
                            messageLabel = "ForceMode:";
                            objectLabel = "GameObject/Component:";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";

                            break;
                        case GameFunctions.REGISTER:
                            msgNeed = true;
                            amtNeed = true;
                            msgDisplay = true;
                            objDisplay = true;
                            amtDisplay = true;
                            oamtDisplay = true;
                            vectorNeed = false;
                            wordDisplay = true;
                            //wordDisplay = false;
                            glyphDisplay = false;
                            glyphDisplay = false;
                            vectorDisplay = true;
                            filterDisplay = false;
                            objListDisplay = true;
                            relationDisplay = false;
                            forceTypeDisplay = false;
                            eventListDisplay = false;
                            wordLabel = "Label:";
                            messageLabel = "Register What:";
                            objectLabel = "Object:";
                            windowLabelTooltip =
                                "(" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                            prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ": " +
                            "https://docs.google.com/document/d/1nn2DCi_aSq38N9QSL-lvT9cmNZIyE0UC13ZLEq-lUl4/pub#id.ygp8bypjkexb" + ")";

                            break;
                        default:
                            break;
                    }

                    if (!turnOnHelp)
                    {
                        objDisplay = true;
                        msgDisplay = true;
                        amtDisplay = true;
                        keyDisplay = true;
                        oamtDisplay = true;
                        wordDisplay = true;
                        glyphDisplay = true;
                        groupDisplay = true;
                        filterDisplay = true;
                        vectorDisplay = true;
                        relationDisplay = true;
                        onDisplay = true;
                        forceTypeDisplay = true;
                        //eventPositionDisplay = true;
                    }

                    if (gb.eventList[i].playerAction == PlayerFunctions.COLLISIONENTER ||
                        gb.eventList[i].playerAction == PlayerFunctions.COLLISIONEXIT ||
                        gb.eventList[i].playerAction == PlayerFunctions.COLLISIONSTAY ||
                        gb.eventList[i].playerAction == PlayerFunctions.TRIGGERENTER ||
                        gb.eventList[i].playerAction == PlayerFunctions.TRIGGERSTAY ||
                        gb.eventList[i].playerAction == PlayerFunctions.TRIGGEREXIT)
                    {
                        keyDisplay = false;
                        //relationDisplay = true;
                        //                      objDisplay = true;
                        objDisplay = true;
                        objNeed = true;
                        filterDisplay = true;
                        relationDisplay = true;
                        msgDisplay = true;
                        eventListDisplay = true;
                        messageLabel = "Event to call";
                        //messageLabel = "Which #event";
                        objectLabel = ":" + prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString();
                        objListDisplay = true;
                    }

                    if (gb.eventList[i].playerAction == PlayerFunctions.INPUTDOWN ||
                        gb.eventList[i].playerAction == PlayerFunctions.INPUTPRESS ||
                        gb.eventList[i].playerAction == PlayerFunctions.INPUTANY ||
                        gb.eventList[i].playerAction == PlayerFunctions.INPUTUP)
                    {
                        keyDisplay = true;
                        wordDisplay = true;
                        //wordLabel = "Word";
                        glyphDisplay = true;
                    }

                    if (gb.eventList[i].playerAction == PlayerFunctions.SYSTEM)
                    {
                        objDisplay = true;
                        msgDisplay = true;
                        amtDisplay = true;
                        keyDisplay = true;
                        oamtDisplay = true;
                        wordDisplay = true;
                        glyphDisplay = true;
                        groupDisplay = true;
                        filterDisplay = true;
                        vectorDisplay = true;
                        relationDisplay = true;
                        onDisplay = true;
                    }

                    GUIStyle gsTest = new GUIStyle();
                    gsTest.normal.textColor = Color.white;

                    windowLabel =
                        " (" + //prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "-" +
                    prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + ")";

                    //2_HB
                    //                  if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING && gb.eventList[i].gameAction == GameFunctions._NO_OP)
                    //                  {
                    //                      GUILayout.BeginHorizontal(new GUIContent("#" + list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue + windowLabel), "Box", GUILayout.MaxWidth(500));
                    //                  }
                    //                  else
                    //                  {

                    if (gb.eventList[i].group != string.Empty)
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.Separator();
                    }


                    GUILayout.BeginHorizontal(new GUIContent("#" + list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue + windowLabel), "Window", GUILayout.MaxWidth(500));
                    {
                        GUI.color = colorStore + Color.gray;
                        if (!gb.eventList[i].alive)
                        {
                            GUI.color -= colorStoreDeactivated;
                        }
                        //GUI.color += new Color(.1f,1f,1f,-.1f);

                        //3_VB
                        if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING && gb.eventList[i].msgFilter != string.Empty)
                        {
                            GUI.color = colorStore + new Color(.5f, 0, 0, 1);
                            onDisplay = true;
                        }
                        #region Vertical
                        EditorGUILayout.BeginVertical("Box");

                        #region Horizontal
                        EditorGUILayout.BeginHorizontal();


                        //EditorGUI.BeginChangeCheck();
                        list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue =
                        GUILayout.Toggle(list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue,
                            GUIContent.none, GUILayout.Width(15), GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
                        //                        if (EditorGUI.EndChangeCheck())
                        //                        {
                        //                            //                      Debug.Log("changed");
                        //                            if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING &&
                        //                                gb.eventList[i].gameAction == GameFunctions._NO_OP &&
                        //                                gb.eventList[i].msgFilter != string.Empty)
                        //                            {
                        //                                //char[] delimiterComma = { ',', ':', '>' };
                        //
                        //                                string[] msgSplits = gb.eventList[i].msgFilter.Split(delimiterComma);
                        //
                        //                                for (int w = 0; w < msgSplits.Length; w++)
                        //                                {
                        //                                    for (int l = 0; l < list.arraySize; l++)
                        //                                    {
                        //                                        string strTemp = "#" + gb.eventList[l].eventName;
                        //                                        if (strTemp == msgSplits[w])
                        //                                        {
                        //                                            list.GetArrayElementAtIndex(l).FindPropertyRelative("alive").boolValue = list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue;
                        //
                        //                                        }
                        //                                    }
                        //                                }
                        //
                        //                            }
                        //                        }
                        if (GUILayout.Button(new GUIContent("C", "Copy"), GUILayout.Width(17), GUILayout.ExpandWidth(false)))
                        {
                            string replacementCommaMsg = prop.FindPropertyRelative("msgFilter").stringValue.ToString();
                            replacementCommaMsg = replacementCommaMsg.Replace(",", "|");
                            string replacementCommaObj = prop.FindPropertyRelative("objFilter").stringValue.ToString();
                            replacementCommaObj = replacementCommaObj.Replace(",", "|");
                            string tStr1 =
                                "C0P1_BUFF3R_G3N3R1C_B3H4V10R," +
                                prop.FindPropertyRelative("amount").floatValue.ToString() + "," + //gb.eventList[i].amount.ToString() + "," +
                                prop.FindPropertyRelative("otherAmount").floatValue.ToString() + "," + //gb.eventList[i].otherAmount.ToString()  + "," +
                                prop.FindPropertyRelative("vectorStorage.x").floatValue.ToString() + "," + //gb.eventList[i].vectorStorage.x.ToString()  + "," +
                                prop.FindPropertyRelative("vectorStorage.y").floatValue.ToString() + "," + //gb.eventList[i].vectorStorage.y.ToString()  + "," +
                                prop.FindPropertyRelative("vectorStorage.z").floatValue.ToString() + "," + //gb.eventList[i].vectorStorage.z.ToString()  + "," +
                                prop.FindPropertyRelative("filterType").enumNames[prop.FindPropertyRelative("filterType").enumValueIndex].ToString() + "," + //gb.eventList[i].fieldType.ToString()  + "," +
                                prop.FindPropertyRelative("gameAction").enumNames[prop.FindPropertyRelative("gameAction").enumValueIndex].ToString() + "," + //gb.eventList[i].gameAction.ToString()  + "," +
                                prop.FindPropertyRelative("playerAction").enumNames[prop.FindPropertyRelative("playerAction").enumValueIndex].ToString() + "," + //gb.eventList[i].playerAction.ToString()  + "," +
                                prop.FindPropertyRelative("relation").enumNames[prop.FindPropertyRelative("relation").enumValueIndex].ToString() + "," + //gb.eventList[i].relation.ToString()  + "," +                 
                                prop.FindPropertyRelative("inputType").enumNames[prop.FindPropertyRelative("inputType").enumValueIndex].ToString() + "," + //gb.eventList[i].relation.ToString()  + "," +                   
                                prop.FindPropertyRelative("key").enumValueIndex.ToString() + "," + //gb.eventList[i].key.ToString()  + "," +
                                prop.FindPropertyRelative("inputString").stringValue.ToString() + "," + //gb.eventList[i].key.ToString()  + "," +
                                prop.FindPropertyRelative("group").stringValue.ToString() + "," + //gb.eventList[i].playerAction.ToString()  + "," +
                                prop.FindPropertyRelative("force").enumNames[prop.FindPropertyRelative("force").enumValueIndex].ToString() + "," + //gb.eventList[i].relation.ToString()  + "," +                   
                                prop.FindPropertyRelative("on").boolValue.ToString() + "," + //gb.eventList[i].on.ToString()  + "," +
                                prop.FindPropertyRelative("alive").boolValue.ToString() + "," + //gb.eventList[i].alive.ToString()  + "," +
                                prop.FindPropertyRelative("showEvent").boolValue.ToString() + "," + //gb.eventList[i].showEvent.ToString()  + "," +
                                prop.FindPropertyRelative("letters").stringValue.ToString() + "," + //gb.eventList[i].letters.ToString()  + "," +
                                prop.FindPropertyRelative("eventName").stringValue.ToString() + "," + //gb.eventList[i].eventName.ToString()  + "," +
                                prop.FindPropertyRelative("word").stringValue.ToString() + "," + //gb.eventList[i].word.ToString()  + "," +
                                replacementCommaMsg + "," +
                                replacementCommaObj;

                            Debug.Log("EVENT:" + prop.FindPropertyRelative("eventName").stringValue + "COPIED TO CLIPBOARD");

                            EditorGUIUtility.systemCopyBuffer = tStr1;
                        }

                        GUI.color = Color.cyan;
                        if (GUILayout.Button(new GUIContent("P", "Paste: " + windowLabel), GUILayout.Width(18), GUILayout.ExpandWidth(false)))
                        {
                            //turnOnHelp = !turnOnHelp;
                            //Application.OpenURL(documentationLink);
                            GameEvent tEvent = new GameEvent();
                            tEvent.amount = float.Parse(eventSplits[1]);
                            tEvent.otherAmount = float.Parse(eventSplits[2]);
                            Vector3 tVec = new Vector3(float.Parse(eventSplits[3]), float.Parse(eventSplits[4]), float.Parse(eventSplits[5]));
                            tEvent.vectorStorage = tVec;
                            tEvent.filterType = (ObjectNameType)System.Enum.Parse(typeof(ObjectNameType), eventSplits[6]);
                            tEvent.gameAction = (GameFunctions)System.Enum.Parse(typeof(GameFunctions), eventSplits[7]);
                            tEvent.playerAction = (PlayerFunctions)System.Enum.Parse(typeof(PlayerFunctions), eventSplits[8]);
                            tEvent.relation = (ObjectRelation)System.Enum.Parse(typeof(ObjectRelation), eventSplits[9]);
                            tEvent.inputType = (InteractionType)System.Enum.Parse(typeof(InteractionType), eventSplits[10]);
                            tEvent.key = (KeyCode)System.Enum.Parse(typeof(KeyCode), eventSplits[11]);
                            tEvent.inputString = eventSplits[12];
                            tEvent.group = eventSplits[13];
                            tEvent.force = (ObjectForce)System.Enum.Parse(typeof(ObjectForce), eventSplits[14]);
                            tEvent.on = (eventSplits[15] == "True");
                            tEvent.alive = (eventSplits[16] == "True");
                            tEvent.showEvent = (eventSplits[17] == "True");
                            tEvent.letters = eventSplits[18];
                            tEvent.eventName = eventSplits[19] + (eventListProp.arraySize + 1).ToString();
                            tEvent.word = eventSplits[20];
                            tEvent.msgFilter = eventSplits[21];
                            tEvent.msgFilter = tEvent.msgFilter.Replace("|", ",");
                            tEvent.objFilter = eventSplits[22];
                            tEvent.objFilter = tEvent.objFilter.Replace("|", ",");

                            //eventListProp.InsertArrayElementAtIndex(0);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("amount").floatValue = tEvent.amount;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = tEvent.otherAmount;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage").vector3Value = tEvent.vectorStorage;
                            int tInt = 0;
                            tInt = (int)System.Enum.Parse(typeof(ObjectNameType), eventSplits[6]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("filterType").enumValueIndex = tInt;//enumNames[prop.FindPropertyRelative("fieldType").enumValueIndex].ToString()
                            tInt = (int)System.Enum.Parse(typeof(GameFunctions), eventSplits[7]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("gameAction").enumValueIndex = tInt;
                            tInt = (int)System.Enum.Parse(typeof(PlayerFunctions), eventSplits[8]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("playerAction").enumValueIndex = tInt;
                            tInt = (int)System.Enum.Parse(typeof(ObjectRelation), eventSplits[9]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("relation").enumValueIndex = tInt;

                            tInt = (int)System.Enum.Parse(typeof(InteractionType), eventSplits[10]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("inputType").enumValueIndex = tInt;//System.Convert.ToInt32(eventSplits[10]);

                            tInt = (int)System.Enum.Parse(typeof(KeyCode), eventSplits[11]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("key").enumValueIndex = tInt;//System.Convert.ToInt32(eventSplits[11]);

                            list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue = tEvent.inputString;

                            list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue = tEvent.group;
                            tInt = (int)System.Enum.Parse(typeof(ObjectForce), eventSplits[14]);
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("force").enumValueIndex = tInt;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("on").boolValue = tEvent.on;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue = tEvent.alive;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("showEvent").boolValue = tEvent.showEvent;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("letters").stringValue = tEvent.letters;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue = tEvent.eventName;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = tEvent.objFilter;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = tEvent.msgFilter;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = tEvent.word;
                            serializedObject.ApplyModifiedProperties();
                        }
                        GUI.color = Color.white;
                        //selId = EditorGUILayout.Popup(selId, popNothingOptions, GUILayout.Width(18));



                        EditorGUI.BeginChangeCheck();

                        selId = EditorGUILayout.Popup(selId, popNothingList.ToArray(), GUILayout.Width(18));

                        //Debug.Log(selId);
                        if (selId > 1)
                        {
                            if (gb.eventList[i].playerAction != PlayerFunctions._NOTHING)
                            {
                                //list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue = popNothingOptions[selId];
                                list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue = popNothingList.ToArray()[selId];
                            }
                            selId = 0;
                        }
                        else if (selId == 1)
                        {

                            if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING && gb.eventList[i].gameAction == GameFunctions._NO_OP)
                            {
                                if (gb.eventList[i].msgFilter.Length > 0)
                                {
                                    string[] msgsplits = gb.eventList[i].msgFilter.Split(delimiterComma);
                                    //Debug.Log(msgsplits);
                                    for (int k = 0; k < msgsplits.Length; k++)
                                    {
                                        string _str = msgsplits[k].Trim(delimiterHash);

                                        for (int j = 0; j < list.arraySize; j++)
                                        {
                                            if (gb.eventList[j].eventName == _str)
                                            {
                                                list.GetArrayElementAtIndex(j).FindPropertyRelative("group").stringValue = string.Empty;
                                            }
                                        }
                                    }
                                }
                            }
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue = string.Empty;

                            selId = 0;
                            //Debug.Log(list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue);
                        }

                        if (EditorGUI.EndChangeCheck())
                        {
                            detectChange++;
                        }


                        GUILayout.Label(new GUIContent("Show", "Expand the event details"), GUILayout.ExpandWidth(false), GUILayout.Width(32));
                        list.GetArrayElementAtIndex(i).FindPropertyRelative("showEvent").boolValue = GUILayout.Toggle(list.GetArrayElementAtIndex(i).FindPropertyRelative("showEvent").boolValue, GUIContent.none, GUILayout.Width(14), GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));

                        //colorStore = GUI.color
                        if (!turnOnDelete)
                        {
                            if (i > 0)
                            {
                                GUI.color = colorStore + Color.grey;
                                if (GUILayout.Button(new GUIContent("[", "Move to top of the list"), GUILayout.ExpandWidth(false), GUILayout.Width(15)))
                                {
                                    list.MoveArrayElement(i, 0);//DeleteArrayElementAtIndex(i);
                                    serializedObject.ApplyModifiedProperties();
                                }
                            }

                            GUI.color = colorStore + Color.grey;
                            if (i < list.arraySize - 1)
                            {
                                if (GUILayout.Button(new GUIContent("]", "Move to Bottom of the list"), GUILayout.ExpandWidth(false), GUILayout.Width(15)))
                                {
                                    list.MoveArrayElement(i, list.arraySize - 1);//DeleteArrayElementAtIndex(i);
                                    serializedObject.ApplyModifiedProperties();
                                }
                            }
                        }

                        bool testDelete = false;
                        if (turnOnDelete)
                        {

                            GUI.color = Color.red;
                            if (GUILayout.Button(new GUIContent("X", "Remove #Event"), GUILayout.Width(20), GUILayout.ExpandWidth(false)))
                            {
                                if (EditorUtility.DisplayDialog("Delete #Event",
                                        "Undo may restore deleting:" + list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue,
                                        "Yes, Delete!", "NO, Cancel!"))
                                {

                                    if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING && gb.eventList[i].gameAction == GameFunctions._NO_OP)
                                    {
                                        if (gb.eventList[i].msgFilter.Length > 0)
                                        {
                                            string[] msgsplits = gb.eventList[i].msgFilter.Split(delimiterComma);
                                            Debug.Log(msgsplits);
                                            for (int k = 0; k < msgsplits.Length; k++)
                                            {
                                                string _str = msgsplits[k].Trim(delimiterHash);

                                                for (int j = 0; j < list.arraySize; j++)
                                                {
                                                    if (gb.eventList[j].eventName == _str)
                                                    {
                                                        list.GetArrayElementAtIndex(j).FindPropertyRelative("group").stringValue = string.Empty;
                                                    }
                                                }
                                            }
                                        }


                                    }
                                    list.DeleteArrayElementAtIndex(i);

                                    //turnOnDelete = false;
                                    testDelete = true;
                                    detectChange++;
                                    serializedObject.ApplyModifiedProperties();

                                }
                                else
                                {
                                    turnOnDelete = false;
                                }

                            }
                        }

                        EditorGUILayout.EndHorizontal();
                        #endregion

                        //                  EditorGUILayout.EndHorizontal();
                        if (gb.eventList[i].on)
                        {
                            GUI.color = new Color(1, 0, 0, 1);
                        }

                        #region Vertical
                        EditorGUILayout.BeginVertical("Box");
                        #region Horizontal
                        EditorGUILayout.BeginHorizontal();
                        GUI.color = new Color(1, .9f, .9f, .25f);
                        if (gb.eventList[i].alive)
                        {
                            GUI.color = new Color(1, 1, 1, 1);
                        }
                        colorStore = GUI.color;
                        EditorGUILayout.LabelField("Name", GUILayout.ExpandWidth(false), GUILayout.Width(50), GUILayout.ExpandWidth(false));

                        EditorGUI.BeginChangeCheck();
                        string text = list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue;
                        text = EditorGUILayout.TextField(text);
                        //EditorGUILayout.PropertyField(EditorGUILayout.GetControlRect(), list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName"));//, GUILayout.Width(100), GUILayout.ExpandWidth(true));//,GUILayout.ExpandWidth(false));
                        if (EditorGUI.EndChangeCheck() && text != null && text != string.Empty)
                        {
                            detectChange++;
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue = text;
                            if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING && gb.eventList[i].gameAction == GameFunctions._NO_OP)
                            {
                                if (gb.eventList[i].msgFilter.Length > 0)
                                {
                                    string[] msgsplits = gb.eventList[i].msgFilter.Split(delimiterComma);
                                    Debug.Log(msgsplits);
                                    for (int k = 0; k < msgsplits.Length; k++)
                                    {
                                        string _str = msgsplits[k].Trim(delimiterHash);

                                        for (int j = 0; j < list.arraySize; j++)
                                        {
                                            if (gb.eventList[j].eventName == _str)
                                            {
                                                list.GetArrayElementAtIndex(j).FindPropertyRelative("group").stringValue = list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName").stringValue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (detectChange > 0)
                        {
                            //Debug.ClearDeveloperConsole();
                            //Debug.Log(gb.eventList[i].eventName + list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue);
                            //                      if (list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue != string.Empty)
                            //                      {
                            groupAssignment = list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue;
                            string oldGroup = list.GetArrayElementAtIndex(i).FindPropertyRelative("group").stringValue;
                            groupWOHash = string.Empty;//gb.eventList[i].group;

                            string _str = groupAssignment;
                            int tInt = _str.IndexOf('#') + 1;

                            if (tInt > 0)
                            {
                                _str = _str.Substring(tInt);
                                groupWOHash = _str;
                            }
                            int spottedGrouper = -1;
                            for (int l = 0; l < list.arraySize; l++)
                            {

                                if (gb.eventList[l].playerAction == PlayerFunctions._NOTHING && gb.eventList[l].gameAction == GameFunctions._NO_OP)
                                {
                                    list.GetArrayElementAtIndex(l).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                    spottedGrouper = l;
                                }
                                else
                                {
                                    continue;
                                }

                                for (int k = 0; k < list.arraySize; k++)
                                {
                                    if (k == spottedGrouper)
                                    {
                                        continue;
                                    }
                                    string groupAndHash = "#" + list.GetArrayElementAtIndex(spottedGrouper).FindPropertyRelative("eventName").stringValue;
                                    if (list.GetArrayElementAtIndex(k).FindPropertyRelative("group").stringValue == groupAndHash)
                                    {
                                        list.GetArrayElementAtIndex(spottedGrouper).FindPropertyRelative("msgFilter").stringValue +=
                                        (list.GetArrayElementAtIndex(spottedGrouper).FindPropertyRelative("msgFilter").stringValue != string.Empty ? "," : "") +
                                        "#" + list.GetArrayElementAtIndex(k).FindPropertyRelative("eventName").stringValue;
                                    }
                                }
                            }

                            detectChange = 0;
                            //                      EditorGUILayout.EndVertical();      
                            //                      EditorGUILayout.EndHorizontal();        
                            //                      EditorGUILayout.EndVertical();      
                            //                      EditorGUILayout.EndHorizontal();        
                            //                      break;

                        }


                        if (testDelete)
                        {
                            EditorGUILayout.EndVertical();
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.EndVertical();
                            EditorGUILayout.EndHorizontal();
                            break;
                        }
                        EditorGUILayout.EndHorizontal();
                        #endregion

                        #region Horizontal
                        //EditorGUILayout.BeginHorizontal();
                        if (groupDisplay || gb.eventList[i].group != string.Empty)
                        {
                            GUI.color = new Color(1, 0, 0, 1);

                            EditorGUILayout.LabelField("Group", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("group"), GUIContent.none);
                            //                      if (eventPositionDisplay)
                            //                      {
                            //                          EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("editorPosition"), GUIContent.none);
                            //                      }
                            GUI.color = colorStore;
                        }
                        //EditorGUILayout.EndHorizontal();
                        #endregion

                        //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("eventName"));

                        //list.GetArrayElementAtIndex(i).FindPropertyRelative("showEvent").boolValue = 

                        #region Horizontal
                        EditorGUILayout.BeginHorizontal();


                        EditorGUILayout.EndHorizontal();
                        #endregion

                        if (gb.eventList[i].showEvent)
                        {
                            if (!gb.eventList[i].alive)
                            {
                                //list.GetArrayElementAtIndex(i).FindPropertyRelative("alive").boolValue = 
                                EditorGUILayout.BeginToggleGroup("INActive/UnEditable", gb.eventList[i].alive);

                            }
                            colorStore = GUI.color;

                            //EditorGUI.BeginChangeCheck ();

                            EditorGUILayout.LabelField("System Event", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                            EditorGUILayout.PropertyField(playerActionProp, GUIContent.none);

                            //                  if (EditorGUI.EndChangeCheck ()) {
                            //                      // Code to execute if GUI.changed
                            //                      // was set to true inside the block of code above.
                            //                      detectChange++;
                            //                  }

                            if (keyDisplay)
                            {
                                #region Horizontal

                                //EditorGUI.indentLevel += 1;
                                EditorGUILayout.BeginHorizontal();

                                EditorGUILayout.LabelField("InputInfo", GUILayout.ExpandWidth(false), GUILayout.Width(55));
                                EditorGUILayout.BeginVertical();
                                //                          iOptionsSelection = EditorGUILayout.Popup(iOptionsSelection, inputOptions);
                                //                          iManagerSelection = EditorGUILayout.Popup(iManagerSelection, inputManagerNames);
                                EditorGUI.BeginChangeCheck();
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("inputType"), GUIContent.none, GUILayout.Width(50), GUILayout.ExpandWidth(true));
                                if (EditorGUI.EndChangeCheck())
                                {
                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue = "";
                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("key").enumValueIndex = 0;
                                }
                                switch (list.GetArrayElementAtIndex(i).FindPropertyRelative("inputType").enumValueIndex)
                                {
                                    case 1://Joypad
                                        EditorGUI.indentLevel += 1;
                                        EditorGUILayout.BeginHorizontal();
                                        EditorGUI.BeginChangeCheck();
                                        //EditorGUILayout.Space();
                                        selId = EditorGUILayout.Popup(selId, joypadOptions, GUILayout.ExpandWidth(false));
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            if (selId > 0)
                                            {
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue = joypadOptions[selId];
                                            }
                                            else
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue = "";

                                        }
                                        selId = 0;
                                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString"), GUIContent.none, GUILayout.ExpandWidth(false));
                                        EditorGUILayout.EndHorizontal();
                                        EditorGUI.indentLevel -= 1;
                                        break;
                                    case 2://Mouse
                                        EditorGUILayout.BeginHorizontal();
                                        EditorGUI.BeginChangeCheck();
                                        selId = EditorGUILayout.Popup(selId, mouseOptions, GUILayout.Width(50), GUILayout.ExpandWidth(false));
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            if (selId > 0)
                                            {
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue = mouseOptions[selId];
                                            }
                                            else
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue = "";

                                        }
                                        selId = 0;
                                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString"), GUIContent.none, GUILayout.Width(30), GUILayout.ExpandWidth(false));
                                        EditorGUILayout.EndHorizontal();
                                        break;
                                    case 0://KEYBOARD
                                    default:
                                        EditorGUI.BeginChangeCheck();
                                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("key"), GUIContent.none);
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("inputString").stringValue =
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("key").enumNames[list.GetArrayElementAtIndex(i).FindPropertyRelative("key").enumValueIndex];
                                        }
                                        break;
                                }


                                EditorGUILayout.EndVertical();
                                EditorGUILayout.EndHorizontal();
                                //EditorGUI.indentLevel -= 1;
                                #endregion
                                serializedObject.ApplyModifiedProperties();
                            }
                            if (filterDisplay)
                            {
                                #region Horizontal
                                EditorGUILayout.BeginHorizontal();
                                EditorGUI.BeginChangeCheck();
                                EditorGUILayout.LabelField("ByType", GUILayout.ExpandWidth(false), GUILayout.Width(80));
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("filterType"), GUIContent.none);
                                if (EditorGUI.EndChangeCheck())
                                {
                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = string.Empty;
                                }
                                EditorGUILayout.EndHorizontal();
                                #endregion
                                serializedObject.ApplyModifiedProperties();
                            }
                            if (relationDisplay)
                            {
                                #region Horizontal
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField("Relation", GUILayout.ExpandWidth(false), GUILayout.Width(80));
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("relation"), GUIContent.none);
                                EditorGUILayout.EndHorizontal();
                                #endregion
                                serializedObject.ApplyModifiedProperties();
                            }

                            //                  if (gb.eventList[i].gameAction == GameFunctions.DESTROY)
                            //                  {
                            //                      //EditorGUILayout.Popup(selectedPopup,accessFloatsHelp,GUILayout.ExpandWidth(false));
                            //                      EditorGUILayout.LabelField("DESTROY Filter",GUILayout.ExpandWidth(false), GUILayout.Width(130));
                            //                      GUI.color = Color.cyan;
                            //                      EditorGUILayout.BeginHorizontal();
                            //                      selectedPopup = EditorGUILayout.Popup(selectedPopup,destroyHelp,GUILayout.ExpandWidth(false));
                            //
                            //                      selectedPopup = 0;
                            //
                            //                      GUI.color = colorStore;
                            //                      EditorGUILayout.EndHorizontal();
                            //                      serializedObject.ApplyModifiedProperties();
                            //                  }
                            EditorGUI.BeginChangeCheck();

                            GUI.color = colorStore;
                            EditorGUILayout.LabelField("Game Action", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("gameAction"), GUIContent.none);//,GUILayout.ExpandWidth(true));

                            if (EditorGUI.EndChangeCheck())
                            {
                                // Code to execute if GUI.changed
                                // was set to true inside the block of code above.
                                detectChange++;
                            }

                            if (detectChange > 0)
                            {
                                Debug.Log("CLEARING OUT " + messageLabel + " and " + objectLabel + " FIELDS");
                                list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = "";
                                list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = "";
                                detectChange = 0;
                                break;
                            }

                            GUI.color = colorStore;

                            if (msgDisplay)
                            {

                                if (msgNeed)
                                {
                                    GUI.color = Color.cyan;
                                }
                                EditorGUILayout.LabelField(new GUIContent((msgNeed ? "*" : "") + messageLabel, messageLabelTooltip), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false), GUILayout.Width(150));
                                if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING && gb.eventList[i].gameAction == GameFunctions._NO_OP)
                                {
                                    EditorStyles.label.wordWrap = true;
                                    EditorGUILayout.LabelField(list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue);
                                }
                                else
                                {
                                    //                              EditorStyles.textArea.wordWrap = true; 
                                    //                              EditorStyles.textField.wordWrap = true; 
                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = EditorGUILayout.TextArea(list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue, GUILayout.ExpandWidth(true));
                                    //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter"), GUIContent.none, GUILayout.ExpandHeight(true));
                                }

                                switch ((GameFunctions)gb.eventList[i].gameAction)
                                {
                                    case GameFunctions.BEHAVIOR:
                                        EditorGUILayout.LabelField("Behavior Filter", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                        GUI.color = Color.cyan;
                                        #region Horizontal
                                        EditorGUILayout.BeginHorizontal();
                                        {
                                            selectedPopup = EditorGUILayout.Popup(selectedPopup, behaviorEvents, GUILayout.ExpandWidth(false));
                                            if (selectedPopup > 0)
                                            {
                                                if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + behaviorEvents[selectedPopup];
                                                }
                                                else
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = behaviorEvents[selectedPopup];
                                                }

                                                selectedPopup = 0;
                                            }
                                            else
                                            {
                                                if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue == string.Empty)
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = "";
                                                }
                                            }
                                            GUI.color = colorStore;
                                            if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                            {
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                            }
                                            if (GUILayout.Button("Enter", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                            {
                                                GUIUtility.keyboardControl = 0;
                                            }
                                        }
                                        EditorGUILayout.EndHorizontal();
                                        #endregion
                                        break;
                                    case GameFunctions.GET_MOVE_DATA:
                                        #region Horizontal
                                        EditorGUILayout.BeginHorizontal();
                                        {
                                            if (popEventOptions != null)
                                            {
                                                if (eventListDisplay == true)
                                                {
                                                    selEventId = EditorGUILayout.Popup(selEventId, getHelp);
                                                }
                                            }
                                            if (selEventId > 0)
                                            {
                                                if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = getHelp[selEventId];
                                                }
                                                else
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = getHelp[selEventId];
                                                }
                                                selEventId = 0;
                                            }
                                            if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                            {
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                            }
                                            if (GUILayout.Button("Enter", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                            {
                                                GUIUtility.keyboardControl = 0;
                                            }
                                        }
                                        EditorGUILayout.EndHorizontal();
                                        #endregion
                                        break;
                                    case GameFunctions._NO_OP:
                                        #region Horizontal
                                        EditorGUILayout.BeginHorizontal();
                                        //                                  if (gb.eventList[i].on)
                                        //                                  {
                                        //                                      if (eventListDisplay == true)
                                        //                                      {
                                        //                                          if (popEventOptions != null)
                                        //                                          {
                                        //                                              selEventId = EditorGUILayout.Popup(selEventId, popEventOptions);
                                        //                                              //selEventId = EditorGUILayout.Popup(selEventId, no_op_Options);
                                        //                                          }
                                        //                                          if (selEventId > 0)
                                        //                                          {
                                        //                                              if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        //                                              {
                                        //                                                  list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + popEventOptions[selEventId];
                                        //                                                  //list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + no_op_Options[selEventId];
                                        //                                              }
                                        //                                              else
                                        //                                              {
                                        //                                                  list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = popEventOptions[selEventId];
                                        //                                                  //list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = no_op_Options[selEventId];
                                        //                                              }
                                        //                                              selEventId = 0;
                                        //                                          }
                                        //                                          if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                        //                                          {
                                        //                                              list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        //                                          }
                                        //                                      }
                                        //                                  }
                                        EditorGUILayout.EndHorizontal();
                                        #endregion
                                        //5_HE
                                        break;
                                    case GameFunctions.CALL_EVENT:
                                    case GameFunctions.COMPARE_DATA:
                                    case GameFunctions.IF_EVENT_ON:
                                    case GameFunctions.TOGGLE_EVENT:
                                    case GameFunctions.TIMER:
                                        #region Horizontal
                                        EditorGUILayout.BeginHorizontal();
                                        if (popEventOptions != null)
                                        {
                                            if (eventListDisplay == true)
                                            {
                                                selEventId = EditorGUILayout.Popup(selEventId, popEventOptions);
                                            }
                                        }
                                        if (selEventId > 0)
                                        {
                                            if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                            {
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + popEventOptions[selEventId];
                                            }
                                            else
                                            {
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = popEventOptions[selEventId];
                                            }
                                            selEventId = 0;
                                        }
                                        if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        }
                                        if (GUILayout.Button("Enter", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                        {
                                            GUIUtility.keyboardControl = 0;
                                        }
                                        EditorGUILayout.EndHorizontal();
                                        #endregion
                                        break;
                                    default:
                                        break;
                                }//Switch
                                //5_HE


                            }


                            GUI.color = colorStore;

                            switch ((GameFunctions)gb.eventList[i].gameAction)
                            {
                                case GameFunctions.SET:

                                    EditorGUILayout.LabelField("SET Filters", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, setStrings, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + setStrings[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = setStrings[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    if (GUILayout.Button("Enter", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        GUIUtility.keyboardControl = 0;
                                    }
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;
                                case GameFunctions.INSTANTIATE:

                                    EditorGUILayout.LabelField("INSTANTIATE:how", GUILayout.ExpandWidth(false), GUILayout.Width(150));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, instantiateHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + instantiateHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = instantiateHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;
                                case GameFunctions.INFO_IO:

                                    EditorGUILayout.LabelField("Init vars before Start:", GUILayout.ExpandWidth(false), GUILayout.Width(150));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, initHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + initHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = initHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;
                                case GameFunctions.REGISTER:

                                    EditorGUILayout.LabelField("Register to:", GUILayout.ExpandWidth(false), GUILayout.Width(150));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, registerEventVariables, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + registerEventVariables[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = registerEventVariables[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;

                                case GameFunctions.SEND_MSG:

                                    EditorGUILayout.LabelField("SEND Filters", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, sendHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + sendHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = sendHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;
                                case GameFunctions.RANDOMIZE:

                                    EditorGUILayout.LabelField("RANDOMIZE Filters", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, randomizeHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("amount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = 0;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + randomizeHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = randomizeHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;

                                case GameFunctions.MODIFY_VALUES:

                                    EditorGUILayout.LabelField("Variables", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, accessFloatsHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("amount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = 0;
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + accessFloatsHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = accessFloatsHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion

                                    break;

                                case GameFunctions.MOTION:

                                    EditorGUILayout.LabelField("Physics", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    GUI.color = Color.cyan;

                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, rigidbodyHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("amount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage").vector3Value = new Vector3(0, 0, 0);
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = rigidbodyHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = rigidbodyHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion

                                    break;
                                case GameFunctions.COMPUTE:

                                    //EditorGUILayout.Popup(selectedPopup,accessFloatsHelp,GUILayout.ExpandWidth(false));

                                    EditorGUILayout.LabelField(" COMPUTE Filter", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    GUI.color = Color.cyan;

                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, computeHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("amount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage").vector3Value = new Vector3(1, 1, 1);
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + computeHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = computeHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion

                                    break;
                                case GameFunctions.IMITATE:

                                    EditorGUILayout.LabelField(" IMITATE Filter", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    GUI.color = Color.cyan;
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup(selectedPopup, imitateHelp, GUILayout.ExpandWidth(false));
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("amount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = 0;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage").vector3Value = new Vector3(1, 1, 1);
                                    }
                                    if (selectedPopup > 0)
                                    {
                                        if (list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue != string.Empty)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue += "," + imitateHelp[selectedPopup];
                                        }
                                        else
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("msgFilter").stringValue = imitateHelp[selectedPopup];
                                        }
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    break;
                            }

                            if (objDisplay)
                            {
                                if (objNeed)
                                {
                                    GUI.color = Color.cyan;
                                    if ((GameFunctions)gb.eventList[i].gameAction == GameFunctions.COMPARE_DATA)
                                    {
                                        GUI.color = Color.green + Color.grey + Color.blue;
                                    }
                                    if ((GameFunctions)gb.eventList[i].gameAction == GameFunctions.GET_MOVE_DATA)
                                    {
                                        GUI.color = Color.yellow + Color.gray;
                                    }
                                }
                                #region IFGET_MOVE_DATA
                                if ((GameFunctions)gb.eventList[i].gameAction == GameFunctions.GET_MOVE_DATA)
                                {
                                    if (gb.eventList[i].msgFilter == "data")
                                    {
                                        EditorGUILayout.LabelField("FetchField:", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                        compareSentence = "";
                                        compareSentenceInto = "";
                                        comparePhrase0 = EditorGUILayout.Popup(comparePhrase0, comparePrivateEvent);

                                        switch (comparePhrase0)
                                        {
                                            case 1:
                                                comparePhrase1 = 1;
                                                if (comparePhrase2 > comparePrivateVariables.Length)
                                                {
                                                    comparePhrase2 = 0;
                                                }

                                                comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, comparePrivateVariables);
                                                break;
                                            case 2:
                                                comparePhrase1 = EditorGUILayout.Popup(comparePhrase1, popEventOptions);
                                                if (comparePhrase2 > comparePrivateVariables.Length)
                                                {
                                                    comparePhrase2 = 0;
                                                }
                                                comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, compareEventVariables);
                                                break;
                                            case 3:
                                            case 4:
                                                comparePhrase1 = 1;
                                                comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, compareInfoVariables);
                                                vectorDisplay = true;
                                                oamtDisplay = true;
                                                wordDisplay = true;
                                                amtDisplay = true;
                                                break;
                                            case 5:
                                            case 6:
                                                //comparePhrase1 = 1;
                                                comparePhrase1 = EditorGUILayout.Popup(comparePhrase1, transformPropertyList);
                                                //comparePhrase2 = 1;
                                                if (comparePhrase1 > 10)
                                                {
                                                    //comparePhrase2 = 0;
                                                    comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, popEventOptions);

                                                }
                                                break;
                                            default:
                                                break;
                                        }

                                        comparePhrase3 = EditorGUILayout.Popup(comparePhrase3, popOptions);
                                    }
                                    EditorGUILayout.LabelField("Place Into:", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    if ((GameFunctions)gb.eventList[i].gameAction == GameFunctions.GET_MOVE_DATA)
                                    {
                                        GUI.color = Color.green + Color.gray;
                                    }
                                    comparePhraseInto0 = EditorGUILayout.Popup(comparePhraseInto0, comparePrivateEvent);

                                    //                              switch (comparePrivateEvent[comparePhraseInto0])
                                    //                              {
                                    //                                  case 1:
                                    //                                      comparePhraseInto1 = 1;
                                    //                                      if (comparePhraseInto2 > comparePrivateVariables.Length)
                                    //                                      {
                                    //                                          comparePhraseInto2 = 0;
                                    //                                      }
                                    //
                                    //                                      comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, comparePrivateVariables);
                                    //                                      break;
                                    //                                  case 2:
                                    //                              //comparePhrase2 = 1;
                                    //                                      comparePhraseInto1 = EditorGUILayout.Popup(comparePhraseInto1, popEventOptions);
                                    //                                      if (comparePhraseInto2 > comparePrivateVariables.Length)
                                    //                                      {
                                    //                                          comparePhraseInto2 = 0;
                                    //                                      }
                                    //                                      comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, compareEventVariables);
                                    //                                      break;
                                    //                                  case 3:
                                    //                                  case 4:
                                    //                                      comparePhraseInto1 = 1;
                                    //                                  //comparePhraseInto1 = EditorGUILayout.Popup(comparePhraseInto1, popEventOptions);
                                    //                                      comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, compareInfoVariables);
                                    //                                      vectorDisplay = true;
                                    //                                      oamtDisplay = true;
                                    //                                      wordDisplay = true;
                                    //                                      amtDisplay = true;
                                    //                                      break;
                                    //                                  case 5:
                                    //                                      comparePhraseInto1 = EditorGUILayout.Popup(comparePhraseInto1, transformPropertyList);
                                    //                                      comparePhraseInto2 = 1;
                                    //                                      if (comparePhraseInto1 > 10)
                                    //                                      {
                                    //                                          //comparePhrase2 = 0;
                                    //                                          comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, popEventOptions);    
                                    //                                          //comparePhrase3 = 0;
                                    //                                      }
                                    //                                      break;
                                    //                                  default:
                                    //                                      break;
                                    //                              }
                                    switch (comparePhraseInto0)
                                    {
                                        case 1:
                                            comparePhraseInto1 = 1;
                                            if (comparePhraseInto2 > comparePrivateVariables.Length)
                                            {
                                                comparePhraseInto2 = 0;
                                            }

                                            comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, comparePrivateVariables);
                                            break;
                                        case 2:
                                            //comparePhrase2 = 1;
                                            comparePhraseInto1 = EditorGUILayout.Popup(comparePhraseInto1, popEventOptions);
                                            if (comparePhraseInto2 > comparePrivateVariables.Length)
                                            {
                                                comparePhraseInto2 = 0;
                                            }
                                            comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, compareEventVariables);
                                            break;
                                        case 3:
                                        case 4:
                                            comparePhraseInto1 = 1;
                                            //comparePhraseInto1 = EditorGUILayout.Popup(comparePhraseInto1, popEventOptions);
                                            comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, compareInfoVariables);
                                            vectorDisplay = true;
                                            oamtDisplay = true;
                                            wordDisplay = true;
                                            amtDisplay = true;
                                            break;
                                        case 5:
                                            comparePhraseInto1 = EditorGUILayout.Popup(comparePhraseInto1, transformPropertyList);
                                            comparePhraseInto2 = 1;
                                            if (comparePhraseInto1 > 10)
                                            {
                                                //comparePhrase2 = 0;
                                                comparePhraseInto2 = EditorGUILayout.Popup(comparePhraseInto2, popEventOptions);
                                                //comparePhrase3 = 0;
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                    comparePhraseInto3 = EditorGUILayout.Popup(comparePhraseInto3, popOptions);

                                    if (comparePhrase0 == 1)
                                    {
                                        comparePhrase1 = 1;
                                        compareSentence += "private:";
                                        if (comparePrivateVariables[comparePhrase2] == "stringindex" || comparePrivateVariables[comparePhrase2] == "stringstorage")
                                        {
                                            wordNeed = true;
                                            amtNeed = false;
                                        }
                                        compareSentence += comparePrivateVariables[comparePhrase2];
                                        compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 2)
                                    {
                                        compareSentence += "event:";
                                        compareSentence += popEventOptions[comparePhrase1];
                                        if (comparePrivateVariables[comparePhrase2] == "stringindex" || comparePrivateVariables[comparePhrase2] == "word" || comparePrivateVariables[comparePhrase2] == "glyph")
                                        {
                                            wordNeed = true;
                                            amtNeed = false;
                                        }
                                        compareSentence += ":" + compareEventVariables[comparePhrase2];
                                        compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 3)
                                    {
                                        compareSentence += "floatindex:";
                                        //compareSentence += popEventOptions [comparePhrase1];
                                        compareSentence += compareInfoVariables[comparePhrase2];
                                        compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 4)
                                    {
                                        compareSentence += "stringindex:";
                                        //compareSentence += popEventOptions [comparePhrase1];
                                        compareSentence += compareInfoVariables[comparePhrase2];
                                        compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 5)
                                    {
                                        compareSentence += "transform:";
                                        if (comparePhrase1 > 6)
                                        {
                                            compareSentence += popEventOptions[comparePhrase2];
                                            compareSentence += ":";
                                        }
                                        compareSentence += transformPropertyList[comparePhrase1];
                                        compareSentence += "@" + popOptions[comparePhrase3];
                                        //                                  Debug.Log(compareSentence);
                                    }

                                    compareSentence += ">";
                                    if (comparePhraseInto0 == 1)
                                    {
                                        comparePhraseInto1 = 1;
                                        compareSentenceInto += "private:";
                                        if (comparePrivateVariables[comparePhraseInto2] == "stringindex" || comparePrivateVariables[comparePhraseInto2] == "stringstorage")
                                        {
                                            wordNeed = false;
                                            amtNeed = true;
                                            oamtNeed = true;
                                        }
                                        compareSentenceInto += comparePrivateVariables[comparePhraseInto2];
                                        compareSentenceInto += "@" + popOptions[comparePhraseInto3];
                                    }
                                    else if (comparePhraseInto0 == 2)
                                    {
                                        compareSentenceInto += "event:";
                                        compareSentenceInto += popEventOptions[comparePhraseInto1];
                                        if (comparePrivateVariables[comparePhraseInto2] == "stringindex" || comparePrivateVariables[comparePhraseInto2] == "word" || comparePrivateVariables[comparePhraseInto2] == "glyph")
                                        {
                                            wordNeed = false;
                                            amtNeed = true;
                                            oamtNeed = true;
                                        }
                                        compareSentenceInto += ":" + compareEventVariables[comparePhraseInto2];
                                        compareSentenceInto += "@" + popOptions[comparePhraseInto3];
                                    }
                                    else if (comparePhraseInto0 == 3)
                                    {
                                        compareSentenceInto += "floatindex:";
                                        compareSentenceInto += compareInfoVariables[comparePhraseInto2];
                                        compareSentenceInto += "@" + popOptions[comparePhraseInto3];
                                    }
                                    else if (comparePhraseInto0 == 4)
                                    {
                                        compareSentenceInto += "stringindex:";
                                        compareSentenceInto += compareInfoVariables[comparePhraseInto2];
                                        compareSentenceInto += "@" + popOptions[comparePhraseInto3];
                                    }
                                    else if (comparePhraseInto0 == 5)
                                    {
                                        compareSentence += "transform:";
                                        if (comparePhraseInto1 > 6)
                                        {
                                            compareSentenceInto += popEventOptions[comparePhraseInto2];
                                            compareSentenceInto += ":";
                                        }
                                        compareSentenceInto += transformPropertyList[comparePhraseInto1];
                                        compareSentenceInto += "@" + popOptions[comparePhraseInto3];
                                        //                                  Debug.Log(compareSentence);
                                    }
                                    //
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    if (GUILayout.Button("Create", GUILayout.ExpandWidth(false), GUILayout.Width(50)))
                                    {
                                        switch (gb.eventList[i].msgFilter)
                                        {
                                            case "axis:":
                                                if (comparePhraseInto0 > 0 && comparePhraseInto1 > 0 && comparePhraseInto2 > 0 && comparePhraseInto3 > 0)
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = compareSentenceInto;
                                                }
                                                break;
                                            case "data":
                                                if (comparePhrase0 > 0 && comparePhrase1 > 0 && comparePhrase2 > 0 && comparePhrase3 > 0 &&
                                                    comparePhraseInto0 > 0 && comparePhraseInto1 > 0 && comparePhraseInto2 > 0 && comparePhraseInto3 > 0)
                                                {
                                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = compareSentence + compareSentenceInto;
                                                }
                                                break;
                                            default:

                                                //                                          if (comparePhraseInto0 > 0 && comparePhraseInto1 > 0 && comparePhraseInto2 > 0 && comparePhraseInto3 > 0)
                                                //                                          {
                                                //                                              list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = compareSentenceInto;
                                                //                                          }
                                                //

                                                break;
                                        }
                                        compareSentence = string.Empty;
                                        compareSentenceInto = string.Empty;

                                    }
                                    if (GUILayout.Button("Clear", GUILayout.ExpandWidth(false), GUILayout.Width(50)))
                                    {
                                        comparePhrase0 = 0;
                                        comparePhrase1 = 0;
                                        comparePhrase2 = 0;
                                        comparePhrase3 = 0;
                                        comparePhraseInto0 = 0;
                                        comparePhraseInto1 = 0;
                                        comparePhraseInto2 = 0;
                                        comparePhraseInto3 = 0;
                                        compareSentence = string.Empty;
                                        compareSentenceInto = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = compareSentence;
                                    }
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                }
                                #endregion

                                #region IFCOMPARE
                                if ((GameFunctions)gb.eventList[i].gameAction == GameFunctions.COMPARE_DATA)
                                {
                                    EditorGUILayout.LabelField("Comparison Field:", GUILayout.ExpandWidth(false), GUILayout.Width(130));
                                    compareSentence = "";
                                    comparePhrase0 = EditorGUILayout.Popup(comparePhrase0, comparePrivateEvent);

                                    switch (comparePhrase0)
                                    {
                                        case 1:
                                            comparePhrase1 = 1;
                                            if (comparePhrase2 > comparePrivateVariables.Length)
                                            {
                                                comparePhrase2 = 0;
                                            }
                                            comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, comparePrivateVariables);
                                            break;
                                        case 2:
                                            comparePhrase1 = EditorGUILayout.Popup(comparePhrase1, popEventOptions);
                                            if (comparePhrase2 > comparePrivateVariables.Length)
                                            {
                                                comparePhrase2 = 0;
                                            }
                                            comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, compareEventVariables);
                                            break;
                                        case 3:
                                        case 4:
                                            comparePhrase1 = 1;
                                            comparePhrase2 = EditorGUILayout.Popup(comparePhrase2, compareInfoVariables);
                                            vectorDisplay = true;
                                            oamtDisplay = true;
                                            wordDisplay = true;
                                            amtDisplay = true;
                                            break;
                                        default:
                                            break;
                                    }

                                    comparePhrase3 = EditorGUILayout.Popup(comparePhrase3, popOptions);

                                    if (comparePhrase0 == 1)
                                    {
                                        comparePhrase1 = 1;
                                        compareSentence += "private:";
                                        if (comparePrivateVariables[comparePhrase2] == "stringindex" || comparePrivateVariables[comparePhrase2] == "stringstorage")
                                        {
                                            wordNeed = true;
                                            amtNeed = false;
                                        }
                                        compareSentence += comparePrivateVariables[comparePhrase2];
                                        //compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 2)
                                    {
                                        compareSentence += "event:";
                                        compareSentence += popEventOptions[comparePhrase1];
                                        if (comparePrivateVariables[comparePhrase2] == "stringindex" || comparePrivateVariables[comparePhrase2] == "word" || comparePrivateVariables[comparePhrase2] == "glyph")
                                        {
                                            wordNeed = true;
                                            amtNeed = false;
                                        }
                                        compareSentence += ":" + compareEventVariables[comparePhrase2];
                                        //compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 3)
                                    {
                                        compareSentence += "floatindex:";
                                        compareSentence += compareInfoVariables[comparePhrase2];
                                        //compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    else if (comparePhrase0 == 4)
                                    {
                                        compareSentence += "stringindex:";
                                        compareSentence += compareInfoVariables[comparePhrase2];
                                        //compareSentence += "@" + popOptions[comparePhrase3];
                                    }
                                    //                          else if (comparePhrase0 == 3 ) {
                                    //                              compareSentence += "stringinfo:";                           
                                    //                              compareSentence += popEventOptions [comparePhrase1];
                                    //                              compareSentence += ":" + compareInfoVariables [comparePhrase2];
                                    //                              compareSentence += "@" + popOptions [comparePhrase3];
                                    //                          }
                                    //
                                    //5_HB
                                    EditorGUILayout.BeginHorizontal();
                                    if (GUILayout.Button("Create", GUILayout.ExpandWidth(false), GUILayout.Width(50)))
                                    {
                                        Debug.Log(compareSentence);

                                        if (comparePhrase0 > 0 && comparePhrase1 > 0 && comparePhrase2 > 0 && comparePhrase3 > 0)
                                        {
                                            compareSentence += "@" + popOptions[comparePhrase3];
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = compareSentence;
                                        }
                                        compareSentence = string.Empty;

                                    }
                                    if (GUILayout.Button("Clear", GUILayout.ExpandWidth(false), GUILayout.Width(50)))
                                    {
                                        comparePhrase0 = 0;
                                        comparePhrase1 = 0;
                                        comparePhrase2 = 0;
                                        comparePhrase3 = 0;
                                        compareSentence = string.Empty;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = compareSentence;
                                    }
                                    EditorGUILayout.EndHorizontal();
                                    //5_HE
                                }
                                else
                                {
                                    //                          if (selId > 0)
                                    //                          {
                                    //                              if (popOptions[selId] != "None (Game Object)")
                                    //                              {
                                    //                                  list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = (selId>0?popOptions[selId]:selId.ToString());
                                    //                                  selId = 0;
                                    //                              }
                                    //
                                    //                          }
                                }
                                #endregion

                                EditorGUILayout.LabelField(new GUIContent((objNeed ? "*" : "") + objectLabel, objectLabelTooltip), GUILayout.ExpandWidth(false), GUILayout.Width(170));

                                #region Horizontal
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter"), GUIContent.none);//,GUILayout.ExpandWidth(true));
                                if (GUILayout.Button("Clear", GUILayout.MaxWidth(40), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                {
                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = string.Empty;
                                }
                                EditorGUILayout.EndHorizontal();

                                switch (list.GetArrayElementAtIndex(i).FindPropertyRelative("filterType").enumValueIndex)
                                {
                                    default:
                                    case 2:
                                        if (popEventOptions != null)
                                        {
                                            if (objListDisplay == true)
                                            {
                                                selId = EditorGUILayout.Popup(selId, popOptions);
                                            }
                                        }

                                        if (selId > 0)
                                        {
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = popOptions[selId];
                                            if (list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue == "None (Game Object)")
                                                list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = "";
                                            selId = 0;
                                        }
                                        break;
                                    case 1:
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = EditorGUILayout.TagField("SelectTag",
                                            list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue);

                                        break;

                                        //list.GetArrayElementAtIndex(i).FindPropertyRelative("objFilter").stringValue = EditorGUILayout.TextField("");
                                        //                                        break;
                                }


                                #endregion
                            }

                            GUI.color = colorStore;

                            if (forceTypeDisplay)
                            {
                                EditorGUILayout.LabelField("ForceType", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("force"), GUIContent.none);//,GUILayout.ExpandWidth(true));
                            }



                            if (vectorDisplay)
                            {
                                if (vectorNeed)
                                {
                                    GUI.color = Color.cyan;
                                }
                                //EditorGUILayout.LabelField("Vector Storage", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage"));//, GUIContent.none);//,GUILayout.ExpandWidth(true));
                            }
                            GUI.color = colorStore;

                            #region Horizontal
                            //5_HB
                            EditorGUILayout.BeginHorizontal();
                            switch (gb.eventList[i].gameAction)
                            {
                                case GameFunctions.COMPARE_DATA:

                                    EditorGUILayout.LabelField("COMPARISON:" + compareHelp[(int)list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage.z").floatValue + 1], GUILayout.ExpandWidth(false), GUILayout.Width(160));
                                    GUI.color = Color.cyan;
                                    //5_HB
                                    #region Horizontal
                                    EditorGUILayout.BeginHorizontal();
                                    selectedPopup = EditorGUILayout.Popup((int)list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage.z").floatValue + 1, compareHelp, GUILayout.ExpandWidth(true));

                                    if (selectedPopup > 0)
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage.z").floatValue = (float)selectedPopup - 1;
                                        selectedPopup = 0;
                                    }
                                    GUI.color = colorStore;
                                    EditorGUILayout.EndHorizontal();
                                    #endregion
                                    //5_HE
                                    break;
                                case GameFunctions.INSTANTIATE:
                                case GameFunctions.SET:
                                    GUI.color = Color.cyan;

                                    if (GUILayout.Button("Spot", GUILayout.MaxWidth(50), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage.x").floatValue = gb.gameObject.transform.position.x;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage.y").floatValue = gb.gameObject.transform.position.y;
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("vectorStorage.z").floatValue = gb.gameObject.transform.position.z;
                                    }
                                    GUI.color = colorStore;
                                    break;
                                default:
                                    break;
                            }//SWITCH GAMEACTION
                            EditorGUILayout.EndHorizontal();
                            #endregion

                            #region Horizontal
                            EditorGUILayout.BeginHorizontal();
                            if (amtDisplay)
                            {
                                EditorGUILayout.LabelField("Amount", GUILayout.ExpandWidth(false), GUILayout.Width(100));

                                if (amtNeed)
                                {
                                    GUI.color = Color.cyan;
                                }
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("amount"), GUIContent.none);
                            }
                            GUI.color = colorStore;
                            EditorGUILayout.EndHorizontal();
                            #endregion

                            #region Horizontal
                            EditorGUILayout.BeginHorizontal();
                            if (oamtDisplay)
                            {
                                EditorGUILayout.LabelField("Other Amount", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                if (oamtNeed)
                                {
                                    GUI.color = Color.cyan;
                                }
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount"), GUIContent.none);
                            }
                            EditorGUILayout.EndHorizontal();
                            #endregion

                            if (gb.eventList[i].gameAction == GameFunctions.MODIFY_VALUES)
                            {
                                #region Horizontal
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField("Operation", GUILayout.ExpandWidth(false), GUILayout.Width(100));

                                selectedPopup = EditorGUILayout.Popup(selectedPopup, modifyHelp2, GUILayout.ExpandWidth(true));
                                if (selectedPopup > 0)
                                {
                                    list.GetArrayElementAtIndex(i).FindPropertyRelative("otherAmount").floatValue = (float)selectedPopup - 1;
                                    selectedPopup = 0;
                                }
                                GUI.color = colorStore;
                                EditorGUILayout.EndHorizontal();
                                #endregion
                            }//if

                            GUI.color = colorStore;
                            #region Horizontal
                            if (wordDisplay)
                            {
                                EditorGUILayout.BeginHorizontal();
                                if (wordNeed)
                                {
                                    GUI.color = Color.cyan;
                                }
                                EditorGUILayout.EndHorizontal();
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField(wordLabel, GUILayout.ExpandWidth(false), GUILayout.Width(70));
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("word"), GUIContent.none, GUILayout.ExpandWidth(false));
                                EditorGUILayout.EndHorizontal();
                                EditorGUILayout.BeginHorizontal();
                                if (gb.eventList[i].gameAction == GameFunctions.PLAY)
                                {
                                    iManagerSelection = EditorGUILayout.Popup(iManagerSelection, playFunctionsList);
                                    if (iManagerSelection > 0)
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = playFunctionsList[iManagerSelection];
                                        iManagerSelection = 0;
                                    }
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(60), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = string.Empty;
                                    }


                                }
                                if (gb.eventList[i].gameAction == GameFunctions.BEHAVIOR)
                                {
                                    iManagerSelection = EditorGUILayout.Popup(iManagerSelection, rigidbodyHelp);
                                    if (iManagerSelection > 0)
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = rigidbodyHelp[iManagerSelection];
                                        iManagerSelection = 0;
                                    }
                                    if (GUILayout.Button("Clear", GUILayout.MaxWidth(60), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    {
                                        list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = string.Empty;
                                    }


                                }

                                //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("word"), GUIContent.none);
                                EditorGUILayout.EndHorizontal();
                                if (keyDisplay)
                                {




                                    //                              EditorGUILayout.BeginHorizontal();
                                    //                              //EditorGUILayout.LabelField("Input", GUILayout.ExpandWidth(false), GUILayout.Width(50));
                                    //                              iOptionsSelection = EditorGUILayout.Popup(iOptionsSelection, inputOptions);
                                    //                              if (inputOptions[iOptionsSelection] == "joypad.axis:"
                                    //                                  || inputOptions[iOptionsSelection] == "mouse.axis:"
                                    //                                  || inputOptions[iOptionsSelection] == "touch:"
                                    //                                  || inputOptions[iOptionsSelection] == "other:")
                                    //                              {
                                    //                                  iManagerSelection = EditorGUILayout.Popup(iManagerSelection, inputManagerNames);
                                    //                              }
                                    //                              //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("key"), GUIContent.none);
                                    //                              EditorGUILayout.EndHorizontal();
                                    //                              EditorGUILayout.BeginHorizontal();
                                    //                              if (GUILayout.Button("Create Input Filter", GUILayout.ExpandWidth(true)))//, GUILayout.Width(50)))
                                    //                              {                               
                                    //                                  if (iOptionsSelection > 0 && iManagerSelection > 0)
                                    //                                  {
                                    //                                      switch (inputOptions[iOptionsSelection])
                                    //                                      {
                                    //                                          case "keyboard:":
                                    //                                              list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = 
                                    //                                                  inputOptions[iOptionsSelection] + gb.eventList[i].key.ToString();
                                    //                                              break;
                                    //                                          case "mousebutton:":
                                    //                                              list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = 
                                    //                                                  inputOptions[iOptionsSelection] + inputManagerNames[iManagerSelection];
                                    //                                              break;
                                    //                                          default:
                                    //                                              list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = 
                                    //                                                  inputOptions[iOptionsSelection] + inputManagerNames[iManagerSelection];
                                    //                                              break;
                                    //                                      }
                                    //                                  }
                                    //
                                    //                                  iOptionsSelection = iManagerSelection = 0;
                                    //                              
                                    //                              }
                                    //                              if (GUILayout.Button("Clear", GUILayout.MaxWidth(20), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                                    //                              {
                                    //                                  list.GetArrayElementAtIndex(i).FindPropertyRelative("word").stringValue = string.Empty;
                                    //                              }
                                    //                              EditorGUILayout.EndHorizontal();
                                }
                            }
                            #endregion

                            GUI.color = colorStore;
                            #region Horizontal
                            EditorGUILayout.BeginHorizontal();
                            if (glyphDisplay)
                            {
                                EditorGUILayout.LabelField("glyph", GUILayout.ExpandWidth(false), GUILayout.Width(100));
                                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("letters"), GUIContent.none);
                            }
                            EditorGUILayout.EndHorizontal();
                            #endregion      

                            if (list.GetArrayElementAtIndex(i).FindPropertyRelative("letters").stringValue == string.Empty)
                            {
                                list.GetArrayElementAtIndex(i).FindPropertyRelative("letters").stringValue = "_";
                                serializedObject.ApplyModifiedProperties();
                            }
                            //                      else if (list.GetArrayElementAtIndex(i).FindPropertyRelative("letters").stringValue.Length > 1)
                            //                      {
                            //                          list.GetArrayElementAtIndex(i).FindPropertyRelative("letters").stringValue = 
                            //                                  list.GetArrayElementAtIndex(i).FindPropertyRelative("letters").stringValue.ToCharArray(0, 1)[0].ToString();
                            //                      }

                            #region Horizontal
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField(onLabel, GUILayout.ExpandWidth(false), GUILayout.Width(90));
                            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("on"), GUIContent.none, GUILayout.Width(30), GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
                            EditorGUILayout.EndHorizontal();
                            #endregion

                            if (!gb.eventList[i].alive)
                            {
                                EditorGUILayout.EndToggleGroup();
                            }

                        }//if SHOWEVENT
                        else if (gb.eventList[i].playerAction == PlayerFunctions._NOTHING &&
                                 gb.eventList[i].gameAction == GameFunctions._NO_OP &&
                                 gb.eventList[i].msgFilter != string.Empty &&
                                 onDisplay == true)
                        {
                            #region Horizontal
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField(onLabel, GUILayout.ExpandWidth(false), GUILayout.Width(90));
                            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("on"), GUIContent.none, GUILayout.Width(30), GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
                            EditorGUILayout.EndHorizontal();
                            #endregion
                        }

                        EditorGUILayout.EndVertical();
                        #endregion

                        //GUI.color = new Color(1, 1, 1, 1);

                        EditorGUILayout.EndVertical();
                        #endregion
                        //3_VE

                        //3_VB
                        #region Vertical
                        EditorGUILayout.BeginVertical();
                        if (i > 0)
                        {
                            if (GUILayout.Button("^", GUILayout.Height(15), GUILayout.Width(17), GUILayout.ExpandWidth(false)))
                            {

                                list.MoveArrayElement(i, i - 1);//DeleteArrayElementAtIndex(i);
                                serializedObject.ApplyModifiedProperties();
                            }
                        }
                        //GUI.color = colorStore + Color.gray;


                        //                  if (GUILayout.Button(new GUIContent((turnOnHelp ? "¿" : "?"), "Expand and show all Events' Properties"), GUILayout.Width(15), GUILayout.Height(15), GUILayout.ExpandWidth(false)))
                        //                  {                       
                        //                      turnOnHelp = !turnOnHelp;
                        //                      serializedObject.ApplyModifiedProperties();
                        //                  }
                        //GUI.color = Color.white;
                        if (i < list.arraySize - 1)
                        {

                            if (GUILayout.Button("v", GUILayout.Height(14), GUILayout.Width(17), GUILayout.ExpandWidth(false)))
                            {
                                list.MoveArrayElement(i, i + 1);//DeleteArrayElementAtIndex(i);
                                serializedObject.ApplyModifiedProperties();
                            }
                        }


                        EditorGUILayout.EndVertical();
                        #endregion
                        //3_VE


                    }
                    GUILayout.EndHorizontal();

                    //2_HE
                    if (gb.eventList[i].group != string.Empty)
                    {
                        EditorGUILayout.EndHorizontal();
                    }

                    EditorGUILayout.Separator();

                }



            }
            EditorStyles.textField.normal.textColor = new Color(0f, 0, 0, 1f);

        }

        private void ObjectAccesList(SerializedProperty list, bool showListSize = true)
        {
            //DragAndDrop.AcceptDrag();
            EditorGUILayout.PropertyField(list, new GUIContent("Object Access List", "To add an object, increase the size by one, and drag an object from the Hierarchy or by pressing the circle button next to where it says \"None (Game Object)\""));
            if (list.isExpanded)
            {
                GUI.color = Color.white;

                EditorGUILayout.BeginVertical("Box", GUILayout.ExpandWidth(false));
                {
                    if (showListSize)
                    {
                        EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                    }
                    for (int i = 0; i < list.arraySize; i++)
                    {
                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
                    }
                }
                EditorGUILayout.EndVertical();
                //          if (gb.objectAccess.Length < 1)
                //          {
                //              GUILayout.Label("EMPTY",GUILayout.Width(45));
                //          }           
            }


            GUI.color = new Color(.8f, .8f, .8f, 1f);
        }

        private void drawGBGroup(GameEvent ge)
        {
            GUILayout.BeginHorizontal(new GUIContent("#" + ge.eventName), "Window", GUILayout.MaxWidth(500));
            EditorGUILayout.Separator();

            ge.eventName = EditorGUILayout.TextField("eventName", ge.eventName);

            GUILayout.EndHorizontal();
            EditorUtility.SetDirty(gb);

        }

        private void drawGB_Set(GameEvent ge)
        {
            GUILayout.BeginHorizontal(new GUIContent("#" + ge.eventName), "Window", GUILayout.MaxWidth(500));
            EditorGUILayout.Separator();

            ge.eventName = EditorGUILayout.TextField("eventName", ge.eventName);

            GUILayout.EndHorizontal();
            EditorUtility.SetDirty(gb);

        }

    }





}
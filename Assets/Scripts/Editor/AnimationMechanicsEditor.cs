﻿using UnityEngine;
using UnityEditor;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

[CustomEditor(typeof(AnimationMechanics))]
public class GenericBehaviorEditor : Editor
{

    SerializedProperty varsListProp;

    SerializedProperty aniTimeProp;
    SerializedProperty animatorProp;
    string text = string.Empty;

    private AnimationMechanics am = null;

    void OnEnable()
    {


        //initStyles();


        aniTimeProp = serializedObject.FindProperty("aniTime");
        animatorProp = serializedObject.FindProperty("animator");
        varsListProp = serializedObject.FindProperty("varsList");


        am = (AnimationMechanics)target;
    }

    public override void OnInspectorGUI()
    {

        serializedObject.Update();
        GUI.color = new Color(1, 1, 1, 1f);//, Color.white,Time.deltaTime);
        EditorGUILayout.Separator();

        //1_VB
        GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(false));

        aniTimeProp.floatValue = EditorGUILayout.FloatField("Animation Speed", aniTimeProp.floatValue, GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(animatorProp);
        GUI.color = Color.white;
        /*if (aniTimeProp.stringValue == string.Empty)
		{
			aniTimeProp.stringValue = "EMPTY BEHAVIOR DESCRIPTION!";
			serializedObject.ApplyModifiedProperties();
			GUI.color = Color.red;
		}*/

        EditorGUILayout.EndVertical();
        //1_VE

        //1_VB
        EditorGUILayout.BeginVertical("Box");
        VarsList(varsListProp, true);
        EditorGUILayout.EndVertical();
        //1_VE

        serializedObject.ApplyModifiedProperties();
    }

    private void VarsList(SerializedProperty list, bool showListSize = true)
    {
        if (GUILayout.Button(new GUIContent("aniVariable", "Add a new, blank, event to this animation"), GUILayout.ExpandWidth(true)))
        {
            animGroup aGroup = new animGroup();
            aGroup.name += varsListProp.arraySize.ToString();
            varsListProp.InsertArrayElementAtIndex(0);
            varsListProp.GetArrayElementAtIndex(0).FindPropertyRelative("name").stringValue = aGroup.name;
            varsListProp.GetArrayElementAtIndex(0).FindPropertyRelative("variableType").enumValueIndex = 0;
            //varsListProp.GetArrayElementAtIndex(0).FindPropertyRelative("values").floatValue = aGroup.values;
            serializedObject.ApplyModifiedProperties();
        }
        //EditorGUILayout.Space();	
        //Debug.Log(list.arraySize + "num");
        EditorGUILayout.PropertyField(list);
        if (list.isExpanded)
        {

            //Debug.Log("asdf");
            //EditorGUILayout.LabelField(list.GetArrayElementAtIndex(0).FindPropertyRelative("name").stringValue + "Hi");
            for (int i = 0; i < list.arraySize; i++)
            {
                EditorGUILayout.BeginVertical("Box");


                SerializedProperty animProp = list.GetArrayElementAtIndex(i).FindPropertyRelative("variableType");
                //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("name"), GUIContent.none);//,GUILayout.ExpandWidth(true));	
                EditorGUILayout.BeginHorizontal();
                text = EditorGUILayout.TextField(list.GetArrayElementAtIndex(i).FindPropertyRelative("name").stringValue, GUILayout.Width(150));
                //EditorGUI.indentLevel += 1;
                list.GetArrayElementAtIndex(i).FindPropertyRelative("name").stringValue = text;
                //list.GetArrayElementAtIndex(i).FindPropertyRelative("name").stringValue = list.GetArrayElementAtIndex(i).FindPropertyRelative("name").stringValue;
                EditorGUILayout.PropertyField(animProp, GUIContent.none);
                float tValues = list.GetArrayElementAtIndex(i).FindPropertyRelative("values").floatValue;
                bool tBool = false;
                switch ((VariableType)animProp.enumValueIndex)
                {
                    case VariableType.UNDEFINED:
                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("values"), GUIContent.none);
                        break;
                    case VariableType.BOOL:
                        EditorGUI.BeginChangeCheck();
                        tBool = EditorGUILayout.Toggle((tValues <= 0 ? false : true));//(new Vector2(30, 30), 5, 0, 10, "rads", Color.cyan, Color.red, true);
                        if (EditorGUI.EndChangeCheck())
                        {
                            list.GetArrayElementAtIndex(i).FindPropertyRelative("values").floatValue = (tBool ? 1 : 0);
                        }
                        //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("values"), GUIContent.none);
                        break;
                    case VariableType.FLOAT:
                        list.GetArrayElementAtIndex(i).FindPropertyRelative("values").floatValue = EditorGUILayout.FloatField(tValues);
                        break;
                    case VariableType.INT:
                        list.GetArrayElementAtIndex(i).FindPropertyRelative("values").floatValue = (float)EditorGUILayout.IntField((int)tValues);
                        //list.GetArrayElementAtIndex(i).FindPropertyRelative("values") 
                        break;
                    case VariableType.TRIGGER:
                    default:
                        break;
                }

                if (GUILayout.Button(new GUIContent("X", "Remove #Event"), GUILayout.Width(20), GUILayout.ExpandWidth(false)))
                {
                    if (EditorUtility.DisplayDialog("Delete #Event",
                            "Undo may restore deleting:" + list.GetArrayElementAtIndex(i).FindPropertyRelative("name").stringValue,
                            "Yes, Delete!", "NO, Cancel!"))
                    {

                        list.DeleteArrayElementAtIndex(i);

                        //turnOnDelete = false;

                        serializedObject.ApplyModifiedProperties();

                    }

                }
                EditorGUILayout.EndHorizontal();
                //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("values"), GUIContent.none);//,GUILayout.ExpandWidth(true));	
                //				EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("values"), GUIContent.none);//,GUILayout.ExpandWidth(true));	
                //EditorGUI.indentLevel -= 1;
                //EditorGUILayout.Separator();
                EditorGUILayout.EndVertical();

            }
        }

    }

}

//protein 20gms every 3 hours
//creatin away
//nitric oxide
//creatin
//ethyl
//micronized
//monohydrate
//hcl

//astaxanthin
//oxidation
//immune system


//colagen/biotin
//musclebased

//folic acid - creates more circulation

//mango extract -- no need
//cayenne pepper extract

//gaba (hgh) - 2 hours before sleep

//lcarnitine = gplc
//reduces oxidation

//bcaa (aminoacids)
//20

//q10
//for absorbtion

//whey protein

//workoutdiary

//fenugreek extract
//increase testosterone
//inhibits conversion to DHT
//treats


//	Leo
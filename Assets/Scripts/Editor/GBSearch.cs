﻿using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
        Keyvan Acosta, Technical Design, Implementation
        Lee Wood Search functions
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Collaboration - MuninuM & ByTheTale
//*/

namespace MuninuM
{
    public class GBSearch : ScriptableWizard
    {
        public float range = 500;
        public Color color = Color.red;
        public string[] genericBehaviours;
        public List<GenericBehavior> genericBehavioursList;
        public string[] searchOptions;
        public string[] searchQueryPopup;
        public string searchQuery;
        public int selectedObject = 0;
        public int selectedSearch = 0;
        public bool drawNow = false;
        public bool selectAll = false;
        public GenericBehavior[] gbs;


        [MenuItem("GameObject/Find...")]
        [MenuItem("GenericBehavior/Find...")]
        static void CreateWizard()
        {
            GBSearch window = ScriptableWizard.DisplayWizard<GBSearch>("Choose Which Object", "Select", "Find");

            window.searchOptions = new string[] { "all", "byGameObjectName", "byGameObjectTag", "byEventName", "bySystemEvent", "byGameAction", };
            //window.genericBehaviours = new string[]{"stringTest","stringTest2","stringTest3",};

            window.gbs = GameObject.FindObjectsOfType<GenericBehavior>();

            //window.genericBehavioursList = new List<GenericBehavior>();
            window.genericBehaviours = window.gbs.Select<GenericBehavior, string>(GB => GB.name).ToArray();
            //window.searchQueryPopup = new string[]{"Empty",};
            window.searchQueryPopup = window.gbs.Select<GenericBehavior, string>(GB => GB.name).ToArray();

            window.genericBehavioursList = window.gbs.Select<GenericBehavior, GenericBehavior>(GB => GB).ToList();


            window.drawNow = true;
            //If you don't want to use the secondary button simply leave it out:
            //ScriptableWizard.DisplayWizard<GBSearch>("Create Light", "Create");
        }


        protected override bool DrawWizardGUI()
        {
            string str = "";
            selectedSearch = EditorGUILayout.Popup("Search By:", selectedSearch, searchOptions);
            EditorGUILayout.Space();
            EditorGUILayout.Separator();
            switch (selectedSearch)
            {
                case 0://all
                    selectedObject = EditorGUILayout.Popup("Found Objects", selectedObject, genericBehaviours);
                    foreach (GenericBehavior gb in genericBehavioursList)
                    {
                        str += gb.gameObject + "\n";

                    }
                    break;
                case 1://byName
                case 2://byTag
                case 3://byEventName
                case 4://byEventName
                    searchQuery = EditorGUILayout.TextField(searchQuery);
                    selectedObject = EditorGUILayout.Popup("Found Objects", selectedObject, searchQueryPopup);
                    foreach (string gb in searchQueryPopup)
                    {
                        str += gb + "\n";

                    }
                    break;
                default:

                    break;
            }
            selectAll = EditorGUILayout.Toggle(new GUIContent("selectAll"), selectAll);

            //EditorGUILayout.TextArea (str);
            //return base.DrawWizardGUI ();
            return true;
        }

        void OnWizardCreate()
        {

            if (gbs.Length > selectedObject)
            {
                //Selection.activeGameObject = gbs[selectedObject].gameObject;
                //Selection.activeGameObject = new GameObject[] { gbs[0].gameObject,gbs[1].gameObject};
                //Selection.objects = new GameObject[] { gbs[0].gameObject,gbs[1].gameObject};
                if (selectAll)
                {
                    Selection.objects = gbs.Select<GenericBehavior, GameObject>(GB => GB.gameObject).ToArray();
                }
                else
                {
                    Selection.activeGameObject = gbs[selectedObject].gameObject;
                }
            }
        }

        void OnWizardUpdate()
        {
            if (drawNow)
            {
                switch (selectedSearch)
                {
                    case 0://all
                        gbs = GameObject.FindObjectsOfType<GenericBehavior>();
                        //Debug.Log("0:gbsLength"+gbs.Length.ToString());
                        break;
                    case 1://byName
                           //Debug.Log("1:gbsLength"+gbs.Length.ToString());
                        break;
                    default:

                        break;
                }
            }
        }

        static bool hasSearchQueryTag(GenericBehavior s)
        {
            return s.gameObject.tag.ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery);
        }

        static bool hasSearchQueryName(GenericBehavior s)
        {
            return s.gameObject.name.ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery);
        }

        static bool objectHasSearchQueryName(GameObject s)
        {
            return s.name.ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery);
        }

        static bool hasBothGBSearchQuery(GenericBehavior s)
        {
            return s.gameObject.name.ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery);
        }

        static bool hasSearchQuerySystemEvent(GenericBehavior s)
        {
            bool boolSum = false;
            if (GetWindow<GBSearch>("Choose Which Object").searchQuery == string.Empty)
                return false;
            for (int j = 0; j < s.eventList.Count; j++)
            {
                //Debug.Log(GetWindow<GBSearch>("Choose Which Object").searchQuery);
                boolSum |= s.eventList[j].playerAction.ToString().ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery.ToLower());
            }
            return boolSum;
        }

        static bool objectHasSearchQuery(GenericBehavior s)
        {
            bool boolSum = false;
            if (GetWindow<GBSearch>("Choose Which Object").searchQuery == string.Empty)
                return false;
            for (int j = 0; j < s.eventList.Count; j++)
            {
                //Debug.Log(GetWindow<GBSearch>("Choose Which Object").searchQuery);
                boolSum |= s.eventList[j].eventName.ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery.ToLower());
            }
            return boolSum;
        }

        static bool objectHasGameActionQuery(GenericBehavior s)
        {
            bool boolSum = false;
            if (GetWindow<GBSearch>("Choose Which Object").searchQuery == string.Empty)
                return false;
            for (int j = 0; j < s.eventList.Count; j++)
            {
                Debug.Log(s.eventList[j].gameAction.ToString());

                //boolSum |= s.eventList[j].gameAction.ToString().ToLower().Contains(GetWindow<GBSearch>("Choose Which Object").searchQuery.ToLower());
            }
            return boolSum;
        }

        //When the user pressed the "Find" button OnWizardOtherButton is called.
        void OnWizardOtherButton()
        {

            genericBehavioursList.Clear();
            genericBehavioursList = gbs.Select<GenericBehavior, GenericBehavior>(GB => GB).ToList();

            switch (selectedSearch)
            {
                case 0://all
                    gbs = GameObject.FindObjectsOfType<GenericBehavior>();
                    //gbs = genericBehavioursList.Select<GenericBehavior,GenericBehavior>(GB => GB).ToArray();
                    break;
                case 1://byName
                    if (true)
                    {
                        //GameObject[] subArray = GameObject.FindObjectsOfType<GameObject>();

                        //gbs =  GameObject.FindObjectsOfType<GenericBehavior>();


                        List<GenericBehavior> sublist = genericBehavioursList.Select<GenericBehavior, GenericBehavior>(GB => GB).ToList();
                        sublist = sublist.FindAll(hasBothGBSearchQuery);
                        searchQueryPopup = sublist.Select<GenericBehavior, string>(GB => GB.gameObject.name).ToArray();
                        gbs = sublist.Select<GenericBehavior, GenericBehavior>(GB => GB).ToArray();
                    }
                    break;
                case 2://byTag
                    {
                        List<GenericBehavior> sublist = genericBehavioursList.FindAll(hasSearchQueryTag);
                        searchQueryPopup = sublist.Select<GenericBehavior, string>(GB => GB.name).ToArray();
                        gbs = sublist.Select<GenericBehavior, GenericBehavior>(GB => GB).ToArray();
                    }
                    break;
                case 3://byEventName
                    {
                        List<GenericBehavior> sublist = genericBehavioursList.FindAll(objectHasSearchQuery);
                        searchQueryPopup = sublist.Select<GenericBehavior, string>(GB => GB.name).ToArray();
                        gbs = sublist.Select<GenericBehavior, GenericBehavior>(GB => GB).ToArray();
                    }
                    break;
                case 4://byEventAction
                    {
                        List<GenericBehavior> sublist = genericBehavioursList.FindAll(objectHasGameActionQuery);
                        searchQueryPopup = sublist.Select<GenericBehavior, string>(GB => GB.name).ToArray();
                        gbs = sublist.Select<GenericBehavior, GenericBehavior>(GB => GB).ToArray();
                    }
                    break;
                case 5://byEventAction
                    {
                        List<GenericBehavior> sublist = genericBehavioursList.FindAll(objectHasGameActionQuery);
                        searchQueryPopup = sublist.Select<GenericBehavior, string>(GB => GB.name).ToArray();
                        gbs = sublist.Select<GenericBehavior, GenericBehavior>(GB => GB).ToArray();
                    }
                    break;
                default:
                    //gbs =  GameObject.FindObjectsOfType<GenericBehavior>();
                    break;
            }
            //selectedObject = 0;
            //Selection.gameObjects = gbs[selectedObject].gameObject;
            //		}
        }
    }
}
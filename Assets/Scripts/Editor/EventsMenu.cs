﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

//using GenericBehavior = MuninuM.GenericBehavior;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/

namespace MuninuM
{
    public class EventsMenu : MonoBehaviour
    {

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Rigidbody/addRigidbody")]
        static void AddGenericEvents_add(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            Rigidbody RB = go.GetComponent<Rigidbody>();// ?? go.AddComponent<Rigidbody> () as Rigidbody;

            if (RB == null)
                RB = go.AddComponent<Rigidbody>() as Rigidbody;

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }

        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Rigidbody/addRigidbody", true)]
        static bool ValidateAddGenericEvents_add()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }
        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Rigidbody/WASD")]
        static void AddGenericEvents_WASD(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            Rigidbody RB = go.GetComponent<Rigidbody>();// ?? go.AddComponent<Rigidbody> () as Rigidbody;

            if (RB == null)
                RB = go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent geUp = new GameEvent(false, "MoveUP_W", InteractionType.KEYBOARD, KeyCode.W, KeyCode.W.ToString(), "w", 1, 0, new Vector3(0, 1, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                 ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geDown = new GameEvent(false, "MoveDOWN_S", InteractionType.KEYBOARD, KeyCode.S, KeyCode.S.ToString(), "s", 1, 0, new Vector3(0, -1, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                   ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geRight = new GameEvent(false, "MoveRIGHT_D", InteractionType.KEYBOARD, KeyCode.D, KeyCode.D.ToString(), "d", 1, 0, new Vector3(1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                    ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geLeft = new GameEvent(false, "MoveLeft_A", InteractionType.KEYBOARD, KeyCode.A, KeyCode.A.ToString(), "a", 1, 0, new Vector3(-1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS,
                                   ObjectRelation._DEFAULT, ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);

            GB.eventList.Insert(0, geUp);
            GB.eventList.Insert(0, geDown);
            GB.eventList.Insert(0, geRight);
            GB.eventList.Insert(0, geLeft);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }

        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Rigidbody/WASD", true)]
        static bool ValidateAddGenericEvents_WASD()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Movement/WASD")]
        static void AddGenericEvents_Physics_WASD(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody> () ?? go.AddComponent<Rigidbody> () as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent geUp = new GameEvent(false, "MoveUP_W", InteractionType.KEYBOARD, KeyCode.W, KeyCode.W.ToString(), "w", 1, 0, new Vector3(0, 1, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                 ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "force", "", true);
            GameEvent geDown = new GameEvent(false, "MoveDOWN_S", InteractionType.KEYBOARD, KeyCode.S, KeyCode.S.ToString(), "s", 1, 0, new Vector3(0, -1, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                   ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "force", "", true);
            GameEvent geRight = new GameEvent(false, "MoveRIGHT_D", InteractionType.KEYBOARD, KeyCode.D, KeyCode.D.ToString(), "d", 1, 0, new Vector3(1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                    ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "force", "", true);
            GameEvent geLeft = new GameEvent(false, "MoveLeft_A", InteractionType.KEYBOARD, KeyCode.A, KeyCode.A.ToString(), "a", 1, 0, new Vector3(-1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS,
                                   ObjectRelation._DEFAULT, ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "force", "", true);

            GB.eventList.Insert(0, geUp);
            GB.eventList.Insert(0, geDown);
            GB.eventList.Insert(0, geRight);
            GB.eventList.Insert(0, geLeft);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }

        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Movement/WASD", true)]
        static bool ValidateAddGenericEvents_Physics_WASD()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Movement/DirectionKeys")]
        static void AddGenericEvents_DirectionKeys(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //		Rigidbody RB = go.GetComponent<Rigidbody> () ?? go.AddComponent<Rigidbody> () as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent geUp = new GameEvent(
                                 false, "MoveUP", InteractionType.KEYBOARD, KeyCode.UpArrow, KeyCode.UpArrow.ToString(), "^", 1, 0, new Vector3(0, 1, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                 ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geDown = new GameEvent(
                                   false, "MoveDOWN", InteractionType.KEYBOARD, KeyCode.DownArrow, KeyCode.DownArrow.ToString(), "v", 1, 0, new Vector3(0, -1, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                   ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geRight = new GameEvent(
                                    false, "MoveRIGHT", InteractionType.KEYBOARD, KeyCode.RightArrow, KeyCode.RightArrow.ToString(), ">", 1, 0, new Vector3(1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                    ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geLeft = new GameEvent(false, "MoveLeft", InteractionType.KEYBOARD, KeyCode.LeftArrow, KeyCode.LeftArrow.ToString(), "<", 1, 0, new Vector3(-1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS,
                                   ObjectRelation._DEFAULT, ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);

            GB.eventList.Insert(0, geUp);
            GB.eventList.Insert(0, geDown);
            GB.eventList.Insert(0, geRight);
            GB.eventList.Insert(0, geLeft);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }

        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Movement/DirectionKeys", true)]
        static bool ValidateAddGenericEvents_DirectionKeys()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Movement/TopDownDirectionKeys")]
        static void AddGenericEvents_TopDownDirectionKeys(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //		Rigidbody RB = go.GetComponent<Rigidbody> () ?? go.AddComponent<Rigidbody> () as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent geUp = new GameEvent(
                                 false, "MoveUP", InteractionType.KEYBOARD, KeyCode.UpArrow, KeyCode.UpArrow.ToString(), "^", 1, 0, new Vector3(0, 0, 1), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                 ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geDown = new GameEvent(
                                   false, "MoveDOWN", InteractionType.KEYBOARD, KeyCode.DownArrow, KeyCode.DownArrow.ToString(), "v", 1, 0, new Vector3(0, 0, -1), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                   ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geRight = new GameEvent(
                                    false, "MoveRIGHT", InteractionType.KEYBOARD, KeyCode.RightArrow, KeyCode.RightArrow.ToString(), ">", 1, 0, new Vector3(1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS, ObjectRelation._DEFAULT,
                                    ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);
            GameEvent geLeft = new GameEvent(false, "MoveLeft", InteractionType.KEYBOARD, KeyCode.LeftArrow, KeyCode.LeftArrow.ToString(), "<", 1, 0, new Vector3(-1, 0, 0), GameFunctions.MOTION, PlayerFunctions.INPUTPRESS,
                                   ObjectRelation._DEFAULT, ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "velocitychange", "", true);

            GB.eventList.Insert(0, geUp);
            GB.eventList.Insert(0, geDown);
            GB.eventList.Insert(0, geRight);
            GB.eventList.Insert(0, geLeft);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }

        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Movement/TopDownDirectionKeys", true)]
        static bool ValidateAddGenericEvents_TopDownDirectionKeys()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Rigidbody/MaximumVelocity")]
        static void AddGenericEvents_MaximumVelocity(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            Rigidbody RB = go.GetComponent<Rigidbody>();// ?? go.AddComponent<Rigidbody> () as Rigidbody;

            if (RB == null)
                RB = go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "SetMaximumVelocity", InteractionType.KEYBOARD, KeyCode.None, KeyCode.None.ToString(), "a", 20, 0, new Vector3(0, 0, 0), GameFunctions.SET, PlayerFunctions.START, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "maxvelmagnitude", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Rigidbody/MaximumVelocity", true)]
        static bool ValidateAddGenericEvents_MaximumVelocity()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Logic/Compute")]
        static void AddGenericEvents_Computer(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            if (go.tag == "untagged" || go.tag == "undefined")
            {
                Debug.Log("This gameObject's tag is:" + go.tag);
            }
            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "ComputeCount_untagged_Tab", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 1, new Vector3(0, 0, 0), GameFunctions.COMPUTE, PlayerFunctions.INPUTDOWN, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "count", "untagged", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Logic/Compute", true)]
        static bool ValidateAddGenericEvents_Compute()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }
        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Logic/Toggle")]
        static void AddGenericEvents_Toggle(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "ToggleWithTab", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 1, new Vector3(0, 0, 0), GameFunctions.TOGGLE_EVENT, PlayerFunctions.INPUTDOWN, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Logic/Toggle", true)]
        static bool ValidateAddGenericEvents_Toggle()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Cosmetic/Imitate/PosX")]
        static void AddEvents_ImitatePosX(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;
            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;
            GB.eventList = GB.eventList ?? new List<GameEvent>();
            GameEvent eventToInsert = new GameEvent(
                                          false, "ImitatePosX", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 1, new Vector3(1, 0, 0), GameFunctions.IMITATE, PlayerFunctions.UPDATE, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "position", "", true);
            GB.eventList.Insert(0, eventToInsert);
            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Cosmetic/ImitatePosX", true)]
        static bool ValidateAddEvents_ImitatePosX()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Cosmetic/Imitate/PosY")]
        static void AddEvents_ImitatePosY(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;
            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;
            GB.eventList = GB.eventList ?? new List<GameEvent>();
            GameEvent eventToInsert = new GameEvent(
                                          false, "ImitatePosY", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 1, new Vector3(0, 1, 0), GameFunctions.IMITATE, PlayerFunctions.UPDATE, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "position", "", true);
            GB.eventList.Insert(0, eventToInsert);
            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Cosmetic/ImitatePosY", true)]
        static bool ValidateAddEvents_ImitatePosy()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }


        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Cosmetic/Scale_x_2")]
        static void AddEvents_ChangeScale(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;
            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;
            GB.eventList = GB.eventList ?? new List<GameEvent>();
            GameEvent eventToInsert = new GameEvent(
                                          false, "ScaleToSize", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 1, new Vector3(1, 1, 1), GameFunctions.SET, PlayerFunctions.START, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "scale", "", true);
            GB.eventList.Insert(0, eventToInsert);
            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Cosmetic/Scale_x_2", true)]
        static bool ValidateAddEvents_ChangeScale()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Position/GotoPosition")]
        static void AddEvents_GotoPosition(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "GoToPosition", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 0, 0, new Vector3(0, 0, 0), GameFunctions.SET, PlayerFunctions.MESSAGE_EVENT, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "position", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Position/GotoPosition", true)]
        static bool ValidateAddEvents_GotoPosition()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Cosmetic/Color")]
        static void AddEvents_ChangeColor(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "ChangeToMainColor", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 0, 0, new Vector3(0, 0, 0), GameFunctions.SET, PlayerFunctions.START, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "maincolor", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Cosmetic/Color", true)]
        static bool ValidateAddEvents_ChangeColor()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Logic/CompareEvent")]
        static void AddGenericEvents_CompareEvent(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            GB.objectAccess = GB.objectAccess ?? new GameObject[1];

            GameObject newObj = new GameObject();
            newObj = go;
            //if (GB.objectAccess != null)

            int objSize = GB.objectAccess.Length;

            Debug.Log(objSize.ToString() + newObj.name);
            if (objSize == 0)
            {
                GB.objectAccess = new GameObject[1];
                GB.objectAccess[0] = go;

            }
            if (objSize > 0)
                GB.objectAccess[GB.objectAccess.Length - 1] = go;

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,
            Debug.Log("This is only an example of a compareEvent. Please fill out the compare's resulting #event.");
            GameEvent eventToInsert = new GameEvent(
                                          false, "CompareEventEqual", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 1, new Vector3(0, 0, 0), GameFunctions.COMPARE_DATA, PlayerFunctions.INPUTDOWN, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "", "event:#CompareEventEqual:otheramount@" + go.name, true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Logic/CompareEvent", true)]
        static bool ValidateAddGenericEvents_CompareEvent()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Logic/Compare")]
        static void AddGenericEvents_Compare(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            GB.objectAccess = GB.objectAccess ?? new GameObject[1];

            GameObject newObj = new GameObject();
            newObj = go;
            //if (GB.objectAccess != null)

            int objSize = GB.objectAccess.Length;

            Debug.Log(objSize.ToString() + newObj.name);
            if (objSize == 0)
            {
                GB.objectAccess = new GameObject[1];
                GB.objectAccess[0] = go;

            }
            if (objSize > 0)
                GB.objectAccess[GB.objectAccess.Length - 1] = go;

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "CompareEqual", InteractionType.KEYBOARD, KeyCode.Tab, KeyCode.Tab.ToString(), "a", 1, 0, new Vector3(0, 0, 0), GameFunctions.COMPARE_DATA, PlayerFunctions.INPUTDOWN, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.FORCE, true, "", "", "private:private:valuestorage@" + go.name, true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Logic/Compare", true)]
        static bool ValidateAddGenericEvents_Compare()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Rigidbody/Bounce")]
        static void AddGenericEvents_Bounce(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            Rigidbody RB = go.GetComponent<Rigidbody>();// ?? go.AddComponent<Rigidbody> () as Rigidbody;

            if (RB == null)
                RB = go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "CollideBounce", InteractionType.KEYBOARD, KeyCode.None, KeyCode.None.ToString(), "a", 1, 0, new Vector3(1, 1, 1), GameFunctions.BEHAVIOR, PlayerFunctions.COLLISIONENTER, ObjectRelation._DEFAULT,
                                          ObjectNameType.TAG, "", ObjectForce.FORCE, true, "", "bounce", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Rigidbody/Bounce", true)]
        static bool ValidateAddGenericEvents_Bounce()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }
        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Rigidbody/LaunchUp")]
        static void AddGenericEvents_LaunchUp(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            Rigidbody RB = go.GetComponent<Rigidbody>();// ?? go.AddComponent<Rigidbody> () as Rigidbody;

            if (RB == null)
                RB = go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "LaunchUp", InteractionType.KEYBOARD, KeyCode.None, KeyCode.None.ToString(), "a", 5, 0, new Vector3(0, 1, 0), GameFunctions.MOTION, PlayerFunctions.START, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.RELATIVEFORCE, true, "", "velocitychange", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Rigidbody/LaunchUp", true)]
        static bool ValidateAddGenericEvents_LaunchUp()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Rigidbody/LaunchRight")]
        static void AddGenericEvents_LaunchRight(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            Rigidbody RB = go.GetComponent<Rigidbody>();// ?? go.AddComponent<Rigidbody> () as Rigidbody;

            if (RB == null)
                RB = go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "LaunchRight", InteractionType.KEYBOARD, KeyCode.None, KeyCode.None.ToString(), "a", 5, 0, new Vector3(1, 0, 0), GameFunctions.MOTION, PlayerFunctions.START, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.RELATIVEFORCE, true, "", "velocitychange", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Rigidbody/LaunchRight", true)]
        static bool ValidateAddGenericEvents_LaunchRight()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }
        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Scene/Load")]
        static void AddGenericEvents_LoadScene(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "ResetScene_EscapeKey", InteractionType.KEYBOARD, KeyCode.Escape, KeyCode.Escape.ToString(), "Ω", 0, 0, new Vector3(0, 0, 0), GameFunctions.LOAD_SCENE, PlayerFunctions.INPUTDOWN, ObjectRelation._DEFAULT,
                                          ObjectNameType._DEFAULT, "", ObjectForce.TRANSLATION, true, "", "", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Scene/Load", true)]
        static bool ValidateAddGenericEvents_LOAD()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Camera/LookAt")]
        static void AddGenericEvents_LookAt(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "ObjectLookAt", InteractionType.KEYBOARD, KeyCode.None, KeyCode.None.ToString(), "Ω", 1, 0, new Vector3(1, 1, 1), GameFunctions.SET, PlayerFunctions.UPDATELATE, ObjectRelation._DEFAULT,
                                          ObjectNameType._DEFAULT, "", ObjectForce.TRANSLATION, true, "", "lookat", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Camera/LookAt", true)]
        static bool ValidateAddGenericEvents_LookAt()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }
        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Numeric/Modify_Setvalue")]
        static void AddGenericEvents_Modify_Setvalue(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "Modify_Setvalue", InteractionType.KEYBOARD, KeyCode.Backspace, KeyCode.Backspace.ToString(), "a", 10, 9, new Vector3(1, 1, 1), GameFunctions.MODIFY_VALUES, PlayerFunctions.INPUTDOWN, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.RELATIVEFORCE, true, "", "valuestorage", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // 939
        // 642
        // 2596



        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Numeric/Modify_Setvalue", true)]
        static bool ValidateAddGenericEvents_Modify_Setvalue()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

        // Validated menu item.
        // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
        // We use a second function to validate the menu item
        // so it will only be enabled if we have a transform selected.
        [MenuItem("GenericBehavior/Events/Time/TimerRepeat")]
        static void AddGenericEvents_TimerRepeat(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = GB.eventList ?? new List<GameEvent>();

            //Rigidbody RB = go.GetComponent<Rigidbody>()??go.AddComponent<Rigidbody>() as Rigidbody;

            //C0P1_BUFF3R_G3N3R1C_B3H4V10R,10,9,0,0,1,_DEFAULT,MOTION,KEYDOWN,_DEFAULT,1,GLOBAL,TRANSLATION,False,True,True,a,_BlankName0,,force,

            GameEvent eventToInsert = new GameEvent(
                                          false, "TimerRepeat", InteractionType.KEYBOARD, KeyCode.None, KeyCode.None.ToString(), "a", -1, 0, new Vector3(1, 1, 1), GameFunctions.TIMER, PlayerFunctions.UPDATE, ObjectRelation.ME,
                                          ObjectNameType._DEFAULT, "", ObjectForce.RELATIVEFORCE, true, "", "", "", true);

            GB.eventList.Insert(0, eventToInsert);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

        }
        // 939
        // 642
        // 2596



        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/Events/Time/TimerRepeat", true)]
        static bool ValidateAddGenericEvents_TimerRepeat()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }




        //	// Add a menu item named "Do Something with a Shortcut Key" to MyMenu in the menu bar
        //	// and give it a shortcut (ctrl-g on Windows, cmd-g on OS X).
        //	[MenuItem ("MyMenu/Do Something with a Shortcut Key %g")]
        //	static void DoSomethingWithAShortcutKey () {
        //		Debug.Log ("Doing something with a Shortcut Key...");
        //	}
        //
        //	// Add a menu item called "Double Mass" to a Rigidbody's context menu.
        //	[MenuItem ("CONTEXT/Rigidbody/Double Mass")]
        //	static void DoubleMass (MenuCommand command) {
        //		Rigidbody body = (Rigidbody)command.context;
        //		body.mass = body.mass * 2;
        //		Debug.Log ("Doubled Rigidbody's Mass to " + body.mass + " from Context Menu.");
        //	}
        //
        //	// Add a menu item to create custom GameObjects.
        //	// Priority 1 ensures it is grouped with the other menu items of the same kind
        //	// and propagated to the hierarchy dropdown and hierarch context menus.
        //	[MenuItem("GameObject/MyCategory/Custom Game Object", false, 10)]
        //	static void CreateCustomGameObject(MenuCommand menuCommand) {
        //		// Create a custom game object
        //		GameObject go = new GameObject("Custom Game Object");
        //		// Ensure it gets reparented if this was a context click (otherwise does nothing)
        //		GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
        //		// Register the creation in the undo system
        //		Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
        //		Selection.activeObject = go;
        //	}

        [MenuItem("GameObject/GenericBehavior/SingleDigit", false, 10)]
        static void CreateCustomScoreMechanics(MenuCommand menuCommand)
        {
            // Create a custom game object
            GameObject go = new GameObject("Custom Game Object");
            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }

        [MenuItem("GenericBehavior/Assets/StickFigure", false, 10)]
        static void CreateCustomStickFigure(MenuCommand menuCommand)
        {
            // Create a custom game object
            GameObject StickFigure = new GameObject("StickFigure");
            StickFigure.transform.position = new Vector3(0, 2.25f, 0);
            //StickFigure.transform.localScale = new Vector3(1f, 1f, 1f);
            //AudioMechanics sd = go.AddComponent<AudioMechanics> () as AudioMechanics;

            //Material material = new Material(Shader.Find("Transparent/Diffuse"));
            //material.color = Color.gray;
            //cubeFloor.GetComponent<Renderer>().material = material;
            //cubeFloor.GetComponent<Renderer>().material = Resources.Load("Gray", typeof(Material)) as Material;
            //cubeFloor.GetComponent<Renderer>().sharedMaterial.color = Color.white;

            //HipJoint.transform.parent = StickFigure.transform;
            GameObject StickHip = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject HipJoint = new GameObject();

            StickHip.transform.parent = HipJoint.transform;
            HipJoint.transform.position = StickFigure.transform.position;
            HipJoint.transform.parent = StickFigure.transform;
            StickHip.transform.localScale = new Vector3(.75f, .25f, .75f);

            GameObject StickSpine = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject SpineJoint = new GameObject();

            StickSpine.transform.localScale = new Vector3(.25f, .25f, .25f);
            StickSpine.transform.parent = SpineJoint.transform;
            SpineJoint.transform.position = HipJoint.transform.position;
            SpineJoint.transform.parent = HipJoint.transform;
            SpineJoint.transform.localPosition = new Vector3(0, .25f, 0);

            GameObject StickChest = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject ChestJoint = new GameObject();

            StickChest.transform.localScale = new Vector3(1, .5f, 1);
            StickChest.transform.parent = ChestJoint.transform;
            ChestJoint.transform.position = SpineJoint.transform.position;
            ChestJoint.transform.parent = SpineJoint.transform;
            ChestJoint.transform.localPosition = new Vector3(0, .75f, 0);

            GameObject StickNeck = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject NeckJoint = new GameObject();
            StickNeck.transform.localScale = new Vector3(.25f, .25f, .25f);
            StickNeck.transform.parent = NeckJoint.transform;
            NeckJoint.transform.position = ChestJoint.transform.position;
            NeckJoint.transform.parent = ChestJoint.transform;
            NeckJoint.transform.localPosition = new Vector3(0, .75f, 0);

            GameObject StickHead = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            GameObject HeadJoint = new GameObject();

            StickHead.transform.localScale = new Vector3(1, 1, 1);
            StickHead.transform.parent = HeadJoint.transform;
            HeadJoint.transform.position = NeckJoint.transform.position;
            HeadJoint.transform.parent = NeckJoint.transform;
            HeadJoint.transform.localPosition = new Vector3(0, .35f, 0);

            GameObject StickLLeg = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            GameObject LLegJoint = new GameObject();

            StickLLeg.transform.localScale = new Vector3(.5f, .75f, .5f);
            StickLLeg.transform.parent = LLegJoint.transform;
            LLegJoint.transform.position = HipJoint.transform.position;
            LLegJoint.transform.parent = HipJoint.transform;
            LLegJoint.transform.localPosition = new Vector3(.25f, -.75f, 0);

            GameObject StickRLeg = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            GameObject RLegJoint = new GameObject();

            StickRLeg.transform.localScale = new Vector3(.5f, .75f, .5f);
            StickRLeg.transform.parent = RLegJoint.transform;
            RLegJoint.transform.position = HipJoint.transform.position;
            RLegJoint.transform.parent = HipJoint.transform;
            RLegJoint.transform.localPosition = new Vector3(-.25f, -.75f, 0);

            GameObject StickLShin = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            GameObject LShinJoint = new GameObject();

            StickLShin.transform.localScale = new Vector3(.25f, .5f, .25f);
            StickLShin.transform.parent = LShinJoint.transform;
            LShinJoint.transform.position = LLegJoint.transform.position;
            LShinJoint.transform.parent = LLegJoint.transform;
            LShinJoint.transform.localPosition = new Vector3(0, -1.25f, 0);

            GameObject StickRShin = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            GameObject RShinJoint = new GameObject();

            StickRShin.transform.localScale = new Vector3(.25f, .5f, .25f);
            StickRShin.transform.parent = RShinJoint.transform;
            RShinJoint.transform.position = RLegJoint.transform.position;
            RShinJoint.transform.parent = RLegJoint.transform;
            RShinJoint.transform.localPosition = new Vector3(0, -1.25f, 0);

            GameObject StickLFoot = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject LFootJoint = new GameObject();

            StickLFoot.transform.localScale = new Vector3(.5f, .25f, .75f);
            StickLFoot.transform.parent = LFootJoint.transform;
            LFootJoint.transform.position = LShinJoint.transform.position;
            LFootJoint.transform.parent = LShinJoint.transform;
            LFootJoint.transform.localPosition = new Vector3(0, -.5f, .25f);

            GameObject StickRFoot = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject RFootJoint = new GameObject();

            StickRFoot.transform.localScale = new Vector3(.5f, .25f, .75f);
            StickRFoot.transform.parent = RFootJoint.transform;
            RFootJoint.transform.position = RShinJoint.transform.position;
            RFootJoint.transform.parent = RShinJoint.transform;
            RFootJoint.transform.localPosition = new Vector3(0, -.5f, .25f);


            GameObject StickLArm = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject LArmJoint = new GameObject();

            StickLArm.transform.localScale = new Vector3(.5f, .5f, .5f);
            StickLArm.transform.parent = LArmJoint.transform;
            LArmJoint.transform.position = ChestJoint.transform.position;
            LArmJoint.transform.parent = ChestJoint.transform;
            LArmJoint.transform.localPosition = new Vector3(.75f, 0f, 0);

            GameObject StickRArm = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject RArmJoint = new GameObject();

            StickRArm.transform.localScale = new Vector3(.5f, .5f, .5f);
            StickRArm.transform.parent = RArmJoint.transform;
            RArmJoint.transform.position = ChestJoint.transform.position;
            RArmJoint.transform.parent = ChestJoint.transform;
            RArmJoint.transform.localPosition = new Vector3(-.75f, 0f, 0);

            GameObject StickLForeArm = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject LForeArmJoint = new GameObject();

            StickLForeArm.transform.localScale = new Vector3(.25f, .75f, .25f);
            StickLForeArm.transform.parent = LForeArmJoint.transform;
            LForeArmJoint.transform.position = LArmJoint.transform.position;
            LForeArmJoint.transform.parent = LArmJoint.transform;
            LForeArmJoint.transform.localPosition = new Vector3(0f, -.75f, 0);

            GameObject StickRForeArm = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject RForeArmJoint = new GameObject();

            StickRForeArm.transform.localScale = new Vector3(.25f, .75f, .25f);
            StickRForeArm.transform.parent = RForeArmJoint.transform;
            RForeArmJoint.transform.position = RArmJoint.transform.position;
            RForeArmJoint.transform.parent = RArmJoint.transform;
            RForeArmJoint.transform.localPosition = new Vector3(0f, -.75f, 0);

            GameObject StickLHand = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            GameObject LHandJoint = new GameObject();

            StickLHand.transform.localScale = new Vector3(.5f, .5f, .5f);
            StickLHand.transform.parent = LHandJoint.transform;
            LHandJoint.transform.position = LForeArmJoint.transform.position;
            LHandJoint.transform.parent = LForeArmJoint.transform;
            LHandJoint.transform.localPosition = new Vector3(0f, -.5f, 0);

            GameObject StickRHand = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            GameObject RHandJoint = new GameObject();

            StickRHand.transform.localScale = new Vector3(.5f, .5f, .5f);
            StickRHand.transform.parent = RHandJoint.transform;
            RHandJoint.transform.position = RForeArmJoint.transform.position;
            RHandJoint.transform.parent = RForeArmJoint.transform;
            RHandJoint.transform.localPosition = new Vector3(0f, -.5f, 0);


            StickHip.name = "StickHip";
            HipJoint.name = "HipJoint";

            StickSpine.name = "StickSpine";
            SpineJoint.name = "SpineJoint";

            StickChest.name = "StickChest";
            ChestJoint.name = "ChestJoint";

            StickNeck.name = "StickNeck";
            NeckJoint.name = "NeckJoint";

            StickHead.name = "StickHead";
            HeadJoint.name = "HeadJoint";

            StickLLeg.name = "StickLLeg";
            LLegJoint.name = "LLegJoint";

            StickRLeg.name = "StickRLeg";
            RLegJoint.name = "RLegJoint";

            StickLShin.name = "StickLShin";
            LShinJoint.name = "LShinJoint";

            StickRShin.name = "StickRShin";
            RShinJoint.name = "RShinJoint";

            StickLShin.name = "StickLFoot";
            LShinJoint.name = "LFootJoint";

            StickRShin.name = "StickRFoot";
            RShinJoint.name = "RFootJoint";

            StickLArm.name = "StickLArm";
            LArmJoint.name = "LArmJoint";

            StickRArm.name = "StickRArm";
            RArmJoint.name = "RArmJoint";

            StickLForeArm.name = "StickLForeArm";
            LForeArmJoint.name = "LForeArmJoint";

            StickRForeArm.name = "StickRForeArm";
            RForeArmJoint.name = "RForeArmJoint";

            StickLHand.name = "StickLHand";
            LHandJoint.name = "LHandJoint";

            StickRHand.name = "StickRHand";
            RHandJoint.name = "RHandJoint";

            //StickHip.transform.parent = HipJoint.transform;


            //			GameObject StickRLeg = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            //			GameObject RLegJoint = new GameObject();
            //			GameObject StickLArm = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            //			GameObject LArmJoint = new GameObject();
            //			GameObject StickRArm = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            //			GameObject RArmJoint = new GameObject();
            //			GameObject StickLShin = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            //			GameObject LShinJoint = new GameObject();
            //			GameObject StickRShin = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            //			GameObject RShinJoint = new GameObject();
            //			GameObject StickLFoot = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //			GameObject LFootJoint = new GameObject();
            //			GameObject StickRFoot = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //			GameObject RFootJoint = new GameObject();
            //			GameObject StickLHand = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //			GameObject LHandJoint = new GameObject();
            //			GameObject StickRHand = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //			GameObject RHandJoint = new GameObject();
            //			GameObject StickLForeArm = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //			GameObject LForeArmJoint = new GameObject();
            //			GameObject StickRForeArm = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //			GameObject RForeArmJoint = new GameObject();
            //

            //HipJoint.transform.position = new Vector3(0, 1.75f, 0);
            //			HipJoint.name = "HipJoint";
            //
            //			StickSpine.name = "StickSpine";
            //			StickSpine.transform.position = new Vector3(0, 2f, 0);
            //			StickSpine.transform.localScale = new Vector3(.25f, .25f, .25f);
            //
            //			StickChest.name = "StickChest";
            //			StickChest.transform.position = new Vector3(0, 2.75f, 0);
            //			StickChest.transform.localScale = new Vector3(1, .5f, 1);
            //
            //			StickNeck.name = "StickNeck";
            //			StickNeck.transform.position = new Vector3(0, 3.5f, 0);
            //			StickNeck.transform.localScale = new Vector3(.25f, .25f, .25f);
            //
            //			StickHead.name = "StickHead";
            //			StickHead.transform.position = new Vector3(0, 3.85f, 0);
            //			StickHead.transform.localScale = new Vector3(1, 1, 1);
            //
            //			StickLLeg.name = "StickLLeg";
            //			StickLLeg.transform.position = new Vector3(-.25f, 1f, 0);
            //			StickLLeg.transform.localScale = new Vector3(.5f, .75f, .5f);
            //
            //			StickRLeg.name = "StickRLeg";
            //			StickRLeg.transform.position = new Vector3(.25f, 1f, 0);
            //			StickRLeg.transform.localScale = new Vector3(.5f, .75f, .5f);
            //
            //			StickLShin.name = "StickLShin";
            //			StickLShin.transform.position = new Vector3(-.25f, -0.25f, 0);
            //			StickLShin.transform.localScale = new Vector3(.25f, .5f, .25f);
            //
            //			StickRShin.name = "StickRShin";
            //			StickRShin.transform.position = new Vector3(.25f, -.25f, 0);
            //			StickRShin.transform.localScale = new Vector3(.25f, .5f, .25f);
            //			
            //			StickLFoot.name = "StickLFoot";
            //			StickLFoot.transform.position = new Vector3(-.25f, -0.75f, .25f);
            //			StickLFoot.transform.localScale = new Vector3(.5f, .25f, .75f);
            //
            //			StickRFoot.name = "StickRFoot";
            //			StickRFoot.transform.position = new Vector3(.25f, -0.75f, .25f);
            //			StickRFoot.transform.localScale = new Vector3(.5f, .25f, .75f);
            //
            //			StickLArm.name = "StickLArm";
            //			StickLArm.transform.position = new Vector3(-.75f, 2.75f, 0);
            //			StickLArm.transform.localScale = new Vector3(.5f, .5f, .5f);
            //
            //			StickRArm.name = "StickRArm";
            //			StickRArm.transform.position = new Vector3(.75f, 2.75f, 0);
            //			StickRArm.transform.localScale = new Vector3(.5f, .5f, .5f);
            //
            //			StickLHand.name = "StickLHand";
            //			StickLHand.transform.position = new Vector3(-.75f, 1.5f, 0);
            //			StickLHand.transform.localScale = new Vector3(.5f, .5f, .5f);
            //			
            //			StickRHand.name = "StickRHand";
            //			StickRHand.transform.position = new Vector3(.75f, 1.5f, 0);
            //			StickRHand.transform.localScale = new Vector3(.5f, .5f, .5f);
            //
            //			StickLForeArm.name = "StickLForeArm";
            //			StickLForeArm.transform.position = new Vector3(-.75f, 2f, 0);
            //			StickLForeArm.transform.localScale = new Vector3(.25f, .75f, .25f);
            //
            //			StickRForeArm.name = "StickRForeArm";
            //			StickRForeArm.transform.position = new Vector3(.75f, 2f, 0);
            //			StickRForeArm.transform.localScale = new Vector3(.25f, .75f, .25f);
            //
            //
            //			StickHip.transform.parent = HipJoint.transform;
            //
            //			StickSpine.transform.parent = HipJoint.transform;
            //
            //			StickChest.transform.parent = HipJoint.transform;
            //			StickNeck.transform.parent = HipJoint.transform;
            //			StickHead.transform.parent = HipJoint.transform;
            //			StickLLeg.transform.parent = StickFigure.transform;//StickHip.transform;
            //			StickRLeg.transform.parent = StickFigure.transform;//StickHip.transform;
            //			StickLArm.transform.parent = StickFigure.transform;//StickChest.transform;
            //			StickRArm.transform.parent = StickFigure.transform;//StickChest.transform;
            //			StickLShin.transform.parent = StickFigure.transform;//StickLLeg.transform;
            //			StickRShin.transform.parent = StickFigure.transform;//StickRLeg.transform;
            //			StickLFoot.transform.parent = StickFigure.transform;//StickLShin.transform;
            //			StickRFoot.transform.parent = StickFigure.transform;//StickRShin.transform;
            //			StickLHand.transform.parent = StickFigure.transform;//StickLForeArm.transform;
            //			StickRHand.transform.parent = StickFigure.transform;//StickRForeArm.transform;
            //			StickLForeArm.transform.parent = StickFigure.transform;//StickLArm.transform;
            //			StickRForeArm.transform.parent = StickFigure.transform;//StickRArm.transform;
            //
            GenericBehavior GB = StickFigure.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = new List<GameEvent>();

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(StickFigure, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(StickFigure, "Create " + StickFigure.name);
            Selection.activeObject = StickFigure;
        }

        [MenuItem("GenericBehavior/Assets/Vignette", false, 10)]
        static void CreateCustomVignette(MenuCommand menuCommand)
        {
            // Create a custom game object
            GameObject go = new GameObject("VignetteBox");

            //AudioMechanics sd = go.AddComponent<AudioMechanics> () as AudioMechanics;

            GameObject cubeFloor = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeFloor.transform.localScale = new Vector3(100, 1, 100);
            cubeFloor.transform.position = new Vector3(0, -1, 0);
            cubeFloor.name = "VignetteFloor";
            Material material = new Material(Shader.Find("Transparent/Diffuse"));
            material.color = Color.gray;
            cubeFloor.GetComponent<Renderer>().material = material;
            //cubeFloor.GetComponent<Renderer>().material = Resources.Load("Gray", typeof(Material)) as Material;
            cubeFloor.transform.parent = go.transform;
            //cubeFloor.GetComponent<Renderer>().sharedMaterial.color = Color.white;
            GameObject cubeWall1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeWall1.transform.localScale = new Vector3(100, 10, 1);
            cubeWall1.transform.position = new Vector3(0, 4.5f, 50);
            cubeWall1.name = "VignetteWallUp";
            cubeWall1.transform.parent = go.transform;
            GameObject cubeWall2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeWall2.transform.localScale = new Vector3(1, 10, 100);
            cubeWall2.transform.position = new Vector3(50, 4.5f, 0);
            cubeWall2.name = "VignetteWallRight";
            cubeWall2.transform.parent = go.transform;
            GameObject cubeWall3 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeWall3.transform.localScale = new Vector3(100, 10, 1);
            cubeWall3.transform.position = new Vector3(0, 4.5f, -50);
            cubeWall3.name = "VignetteWallBottom";
            cubeWall3.transform.parent = go.transform;
            GameObject cubeWall4 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeWall4.transform.localScale = new Vector3(1, 10, 100);
            cubeWall4.transform.position = new Vector3(-50, 4.5f, 0);
            cubeWall4.name = "VignetteWallLeft";
            cubeWall4.transform.parent = go.transform;
            GameObject cubeCeiling = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeCeiling.transform.localScale = new Vector3(100, 1, 100);
            cubeCeiling.transform.position = new Vector3(0, 10, 0);
            cubeCeiling.name = "VignetteCeiling";
            cubeCeiling.GetComponent<Renderer>().enabled = false;
            cubeCeiling.transform.parent = go.transform;

            GenericBehavior GB = cubeFloor.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = new List<GameEvent>();

            //		GameEvent ge = new GameEvent (
            //			               false,
            //			               "DestroyOnPlay",
            //			               KeyCode.Space,
            //			               "a",
            //			               0,
            //			               0,
            //			               Vector3.zero,
            //			               GameFunctions.DESTROY,
            //			               PlayerFunctions.START,
            //			               ObjectRelation._DEFAULT,
            //			               ObjectNameType._DEFAULT,
            //			               ObjectRoot.GLOBAL,
            //			               ObjectForce.TRANSLATION,
            //			               true,
            //			               string.Empty,
            //			               "",
            //			               "",
            //			               true);

            //cylinder.GetComponent<GenericBehavior> ().eventList.Add (ge);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }

        [MenuItem("GameObject/GenericBehavior/SoundMachine", false, 10)]
        static void CreateCustomSoundMechanics(MenuCommand menuCommand)
        {
            // Create a custom game object
            GameObject go = new GameObject("SoundMachine");

            AudioMechanics sd = go.AddComponent<AudioMechanics>() as AudioMechanics;
            if (sd.gameObject == null)
            {
                return;
            }
            GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinder.name = "Speaker";
            cylinder.transform.parent = go.transform;

            GenericBehavior GB = cylinder.AddComponent<GenericBehavior>() as GenericBehavior;

            GB.eventList = new List<GameEvent>();

            GameEvent ge = new GameEvent(
                               false,
                               "DestroyOnPlay",
                               InteractionType.KEYBOARD,
                               KeyCode.Space,
                               KeyCode.Space.ToString(),
                               "a",
                               0,
                               0,
                               Vector3.zero,
                               GameFunctions.DESTROY,
                               PlayerFunctions.START,
                               ObjectRelation._DEFAULT,
                               ObjectNameType._DEFAULT,
                               "",
                               ObjectForce.TRANSLATION,
                               true,
                               string.Empty,
                               "",
                               "",
                               true);

            cylinder.GetComponent<GenericBehavior>().eventList.Add(ge);

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }


        [MenuItem("GameObject/GenericBehavior/AddGenericBehavior", false, 10)]
        static void CreateDefaultGenericBehavior(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            //GB.eventList = GB.eventList ?? new List<GameEvent> ();

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }

        [MenuItem("GenericBehavior/AddGenericBehavior", false, 10)]
        static void CreateDefaultGenericBehaviorFromMenu(MenuCommand menuCommand)
        {

            GameObject go = Selection.activeTransform.gameObject;

            //	Check if GB component is present
            GenericBehavior GB = go.GetComponent<GenericBehavior>() ?? go.AddComponent<GenericBehavior>() as GenericBehavior;

            //GB.eventList = GB.eventList ?? new List<GameEvent> ();

            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }

        // Validate the menu item defined by the function above.
        // The menu item will be disabled if this function returns false.
        [MenuItem("GenericBehavior/AddGenericBehavior", true)]
        static bool ValidateAddGenericBehaviorFromMenu()
        {
            // Return false if no transform is selected.
            return Selection.activeTransform != null;
        }

    }
}
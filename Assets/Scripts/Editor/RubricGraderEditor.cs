﻿using UnityEngine;
using UnityEditor;

public class RubricGrid : EditorWindow {

	static string[] inputHigh = new string[] 
	{
		"HIGH-Reset","Game has multiple objects that each \n acts with its own unique input controls.",
		"Some objects react on their own \n (timer, keyboard manipulation: move ball), some  \nreact because of interaction with them (i.e. collision).",
		"Keyboard input is recognized in a \n consistent basis.",
	};



	static string[] inputMedium = new string[] 
	{
		"MEDIUM-Reset","Game has mostly one object that reacts to \n input from player or only one input command.",
		"One but not both types of keyboard \n inputs are entered and reacted to accordingly.",

	};

	static string[] inputLow = new string[] 
	{
		"LOW-Reset","GameObjects i.e. Environment, have no useful, \n comprehensive, interactions.",
		//		"Neither of the keyboard states \nhave been recognized",
		//		"None of the input devices are used",
	};
	static string[] prefabHigh = new string[] 
	{
		"HIGH-Reset","(2+) GameObjects w/multiple non-default  \ncomponent; namely, GenericBehavior.cs.",
		"Has 1 or more Nested children objects.",
		"Has Non Generic Name, and is well labeled.",
	};

	static string[] prefabMedium = new string[] 
	{
		"MEDIUM-Reset","Only One GameObject with children.",
		"Game Objects have accurate labels, \n but unnecessary prefabs.",

	};

	static string[] prefabLow = new string[] 
	{
		"LOW-Reset","Scripts and parenting aren't \n functional towards the scene.",
		//		"Neither of the keyboard states \nhave been recognized",
		//		"None of the input devices are used",
	};
	static string[] timedHigh = new string[] 
	{
		"HIGH-Reset","All timed events enhance the theme.",
		"Game Object(s) have functions that control it \n on a timed interval and is relevant to the scene.",
		"Event is managed properly without causing errors.",
	};

	static string[] timedMedium = new string[] 
	{
		"MEDIUM-Reset","A timed event occurs,  \nbut doesn't add to the scene/theme.",
		"A timed event occurs, but its frequency and \n detectability aren't noticeable in the scene.",

	};

	static string[] timedLow = new string[] 
	{
		"LOW-Reset","A timed event doesn’t report, accrue, \n nor manage gameobject in any noticeable way.",
	};

	static string[] cohesionHigh = new string[] 
	{
		"HIGH-Reset","Great theme Find!",
		"Great design exploration.",
		"Scenes and themes enhance one another.",
	};

	static string[] cohesionMedium = new string[] 
	{
		"MEDIUM-Reset","The theme is too simple.","Scenes are disjointed, but within theme.",

	};

	static string[] cohesionLow = new string[] 
	{
		"LOW-Reset","The scenes are disparate and theme is derivative.",
	};

	static string[] inputHighReply = new string[] 
	{
		"The actions chosen for the project are adequate and simple to operate through the available inputs.",
		"Simplexity is a word that comes to mind when evaluating this design. Simplicity takes a lot of complicated effort to arrive at that simplicity.",
		"Interacting with the scene feels good.",
		"Obviously, things happen within the scene in a way that it demonstrates cohesive input design.",
		"The scene feels complete as it comes to input.",
		"Noticeable things occur based on input.",
		"Since the keyboard is the only input allowed, it has allowed consistency in the interactions used for the project; i.e. the keys selected work for, and not against, the interaction.",
		"No problems with keyboard input.",
		"Keyboard keys react adequately.",

	};
	static string[] inputMediumReply = new string[] 
	{
		"Only one object reacting to input is insufficient.",
		"A complete design features more than one single object reacting to a player's input.",
		"The design leaves to the imagination other inputs that could have featured well into this implementation.",
		"The type of keypresses (repeating, or only once: KEYPRESS or KEYDOWN respectively) allow for a nuance of play that is useful for different types of interactions with the same key.",
		"In electronic inputs, pressing and holding a key continues to trigger that event; other times, it triggers only once when pressed (regardless of holding down the key). A third would be to test when the key is released. All of these could be implemented in your project to create better results.",
		"Input is something that requires a lot of testing, both for function and feel; their implementation is lacking.",


	};
	static string[] inputLowReply = new string[] 
	{
		"Input is unrecognized by the system.",
		"The project features no worthwhile interactivity.",
		"Interactivity features very poorly in the delivered project.",
	};

	static string[] prefabHighReply = new string[] 
	{
		"Project abounds with GB Game Objects.",
		"Prefabs weren't unnecessarily made, which is very useful for the maintanance of any project.",
		"All GameObjects and GenericBehavior components were included adequately.",
		"The GameObjects that needed parenting were, those that didn't weren't.",
		"The project also has a well organized hierarchy.",
		"Child objects were assigned correctly.",
		"Each game object is named accordingly.",
		"All game objects have proper labels; tags are usefully applied when necessary",
		"Game Objects in the hierarchy are well named too.",

	};
	static string[] prefabMediumReply = new string[] 
	{
		"The minimal effort was placed in organizing the project's hierarchy.",
		"It's beneficial to organize the hierarchy optimally and consistently; \nthis isn't the case in the delivered project across all scenes.",
		"Try to keep the hierarchy more organized next time.",
		"Game objects were labeled, but not with care.",
		"Game components (Inspector) were inconsistent in their implementation and addition to gameobjects.",
		"If a game object has any unneeded components, or incorrectly used ones (labels, tags, etc); remove them.",


	};
	static string[] prefabLowReply = new string[] 
	{
		"The scripts, hierarchy, or components contained in the scene(s) are a mess.",
		"The organization of your Game Objects (prefabs, etc) needs a drastic improvement.",
		"Please maintain your scene, and gameobjects, and game components, and hierarchy, and project, organized!",
	};
	static string[] timedHighReply = new string[] 
	{
		"Timer, a countdown at least, occurs and trigges an event.",
		"Time is an important component of most games; good demonstration!",
		"Time demonstrates something changing or being modified as an important element of the scene.",
		"The time demonstrations features well in the scene(s).",
		"When a clock determines some event, it's good when a player can connect it thematically.",
		"Time, or a specific event based on it, is certainly present and felt.",
		"The scene is certainly enhance by the inclusion of this timed event.",
		"Timed event adds to the scene!",
		"The scene implemented a good timed event.",

	};
	static string[] timedMediumReply = new string[] 
	{
		"Though a timed event is present, it is somewhat nonsensical to the theme.",
		"If the scene was to be enhanced by this time component, it needs to be more consistent and logical to the theme presented to the player.",
		"The idea for the timed event included should have a good connection to the central theme of the scenes.",
		"While something happens set to a moment in time, its frequency is inconsistent.",
		"Many things could have benefited from showing that timed event in a more detectable way.",
		"The timed events need to be more perceivable.",


	};
	static string[] timedLowReply = new string[] 
	{
		"Time isn't demonstrated in any worthwile perceivable way.",
		"The timed events don't work as expected.",
		"Timer events can't be considered as useful to the project.",
	};
	static string[] cohesionHighReply = new string[] 
	{
		"This is a good \"find\" for a theme.",
		"Implementation of the scene(s) feels good and elaborate, assembling itself into a good theme.",
		"Transitioning betweens scenes feels natural and good.",
		"All scene(s) work well together.",
		"The project has an adequate cohesive structure and theme in its current implementation.",
	};
	static string[] cohesionMediumReply = new string[] 
	{
		"The project has an adequate cohesive structure and theme in its current implementation.",
		"The transitions between scenes aren't implemented well. ",
		"The project should be able to show all scenes through easy transitions; this isn't the case as implemented.",
	};
	static string[] cohesionLowReply = new string[] 
	{
		"The scene(s) are incomplete; \nthus, the design suffers due to this.",
		"Design leaves a lot to be desired.",
		"This design doesn't feature well in a portfolio. \nFurther work is needed.",
	};

	string myString = "Hello World";
	string sentence = string.Empty;
	string contactInfo = string.Empty;
	string contactRequestMsg = string.Empty;
	bool groupEnabled;
	bool showstopperEnabled = false;
	bool myBool = false;
	float myFloat = 1.23f;

	int points = 100;
	int inputSelectHigh = 0;
	int inputSelectMedium = 0;
	int inputSelectLow = 0;
	int prefabSelectHigh = 0;
	int prefabSelectMedium = 0;
	int prefabSelectLow = 0;
	int timedSelectHigh = 0;
	int timedSelectMedium = 0;
	int timedSelectLow = 0;
	int cohesionSelectHigh = 0;
	int cohesionSelectMedium = 0;
	int cohesionSelectLow = 0;

	// Add menu named "My Window" to the Window menu
	[MenuItem ("Window/Rubrics/Beginnings")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		RubricGrid window = (RubricGrid)EditorWindow.GetWindow (typeof (RubricGrid));

		window.Show();
	}

	void OnGUI () {
		contactRequestMsg = "\nContact me " + (contactInfo == string.Empty?"":"at "+contactInfo) + " during office hours to review more specifics about your project";
		GUILayout.Label ("Beginnings Rubric", EditorStyles.boldLabel);

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		EditorGUILayout.BeginVertical();

		GUILayout.Label ("Input", EditorStyles.boldLabel, GUILayout.Width(100));
		if (GUILayout.Button("Random"))
		{
			inputSelectHigh = Random.Range((int)0,(int)4);
			inputSelectMedium = Random.Range((int)0,(int)3);
			inputSelectLow = Random.Range((int)0,(int)2);
			Repaint();
		}



		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		inputSelectHigh = GUILayout.SelectionGrid(inputSelectHigh,inputHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		inputSelectMedium = GUILayout.SelectionGrid(inputSelectMedium,inputMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		if (inputSelectMedium > 0) 
		{
			inputSelectHigh = 0;
			Repaint();
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		inputSelectLow = GUILayout.SelectionGrid(inputSelectLow,inputLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		EditorGUILayout.BeginVertical();
		GUILayout.Label ("Prefabs", EditorStyles.boldLabel, GUILayout.Width(100));
		if (GUILayout.Button("Random"))
		{
			prefabSelectHigh = Random.Range((int)0,(int)4);
			prefabSelectMedium = Random.Range((int)0,(int)3);
			prefabSelectLow = Random.Range((int)0,(int)2);
			Repaint();
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		prefabSelectHigh = GUILayout.SelectionGrid(prefabSelectHigh,prefabHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		prefabSelectMedium = GUILayout.SelectionGrid(prefabSelectMedium,prefabMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		if (prefabSelectMedium > 0) 
		{
			prefabSelectHigh = 0;
			Repaint();
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		prefabSelectLow = GUILayout.SelectionGrid(prefabSelectLow,prefabLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		EditorGUILayout.BeginVertical();
		GUILayout.Label ("Timers", EditorStyles.boldLabel, GUILayout.Width(100));
		if (GUILayout.Button("Random"))
		{
			timedSelectHigh = Random.Range((int)0,(int)4);
			timedSelectMedium = Random.Range((int)0,(int)3);
			timedSelectLow = Random.Range((int)0,(int)2);
			Repaint();
		}

		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		timedSelectHigh = GUILayout.SelectionGrid(timedSelectHigh,timedHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		timedSelectMedium = GUILayout.SelectionGrid(timedSelectMedium,timedMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		if (timedSelectMedium > 0) 
		{
			timedSelectHigh = 0;
			Repaint();
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		timedSelectLow = GUILayout.SelectionGrid(timedSelectLow,timedLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;
		EditorGUILayout.BeginHorizontal("Box", GUILayout.Width(300));
		EditorGUILayout.BeginVertical();
		GUILayout.Label ("Cohesion", EditorStyles.boldLabel, GUILayout.Width(100));
		if (GUILayout.Button("Random"))
		{
			cohesionSelectHigh = Random.Range((int)0,(int)4);
			cohesionSelectMedium = Random.Range((int)0,(int)3);
			cohesionSelectLow = Random.Range((int)0,(int)2);
			Repaint();
		}

		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");

		cohesionSelectHigh = GUILayout.SelectionGrid(cohesionSelectHigh,cohesionHigh,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .7f, .7f, 1);
		cohesionSelectMedium = GUILayout.SelectionGrid(cohesionSelectMedium,cohesionMedium,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		if (cohesionSelectMedium > 0) 
		{
			cohesionSelectHigh = 0;
			Repaint();
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginVertical("Box");
		GUI.color = new Color(.7f, .5f, .5f, 1);
		cohesionSelectLow = GUILayout.SelectionGrid(cohesionSelectLow,cohesionLow,1,GUILayout.ExpandHeight(true), GUILayout.Width(300));
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();

		GUI.color = Color.white;

		if (inputSelectLow == 1)
		{
			inputSelectHigh = 0;
			inputSelectMedium = 0;
		}
		if (prefabSelectLow == 1)
		{
			prefabSelectHigh = 0;
			prefabSelectMedium = 0;
		}
		if (timedSelectLow == 1)
		{
			timedSelectHigh = 0;
			timedSelectMedium = 0;
		}
		if (cohesionSelectLow == 1)
		{
			cohesionSelectHigh = 0;
			cohesionSelectMedium = 0;
		}

		//myString = EditorGUILayout.LabelField ("Grid Code Map",
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel ("Grid Code Map");
		EditorGUILayout.LabelField (
			inputSelectHigh.ToString() +
			inputSelectMedium.ToString() +
			inputSelectLow.ToString() +
			prefabSelectHigh.ToString() +
			prefabSelectMedium.ToString() +
			prefabSelectLow.ToString() +
			timedSelectHigh.ToString() +
			timedSelectMedium.ToString() +
			timedSelectLow.ToString() +
			cohesionSelectHigh.ToString() +
			cohesionSelectMedium.ToString() +
			cohesionSelectLow.ToString(),GUILayout.Width(200),GUILayout.ExpandWidth(false));
		
		if (GUILayout.Button("RandomizeAll",GUILayout.ExpandWidth(false)))
		{
			inputSelectHigh = Random.Range((int)0,(int)4);
			inputSelectMedium = Random.Range((int)0,(int)3);
			inputSelectLow = Random.Range((int)0,(int)2);
			prefabSelectHigh = Random.Range((int)0,(int)4);
			prefabSelectMedium = Random.Range((int)0,(int)3);
			prefabSelectLow = Random.Range((int)0,(int)2);
			timedSelectHigh = Random.Range((int)0,(int)4);
			timedSelectMedium = Random.Range((int)0,(int)3);
			timedSelectLow = Random.Range((int)0,(int)2);
			cohesionSelectHigh = Random.Range((int)0,(int)4);
			cohesionSelectMedium = Random.Range((int)0,(int)3);
			cohesionSelectLow = Random.Range((int)0,(int)2);
			Repaint();
		}
		EditorGUILayout.EndHorizontal();
		//string tStr = EditorGUIUtility.systemCopyBuffer;

		int randomizePhrase = Random.Range((int)1,(int)4);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Enter Your Contact Info:");
		contactInfo = EditorGUILayout.TextField(contactInfo,GUILayout.Width(200),GUILayout.ExpandWidth(false));
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		myBool = EditorGUILayout.Toggle("RequestContact", myBool,GUILayout.ExpandWidth(false));
		if (GUILayout.Button("Construct Message & Calculate Score", GUILayout.ExpandWidth(true)))
		{
			
			sentence = string.Empty;
			EditorGUIUtility.systemCopyBuffer = sentence;
			randomizePhrase = Random.Range((int)0,(int)3);
			points = 100;
			sentence += "Input- ";
			if (inputSelectLow == 0)
			{
				switch (inputSelectHigh)
				{
				case 1:
					sentence += inputHighReply[randomizePhrase];
					break;
				case 2:
					sentence += inputHighReply[randomizePhrase+3];
					break;
				case 3:
					sentence += inputHighReply[randomizePhrase+6];
					break;
				case 0:
				default:
					if (inputSelectMedium == 0 || inputSelectMedium == 0)
					{
						sentence += "As per the rubric: "+ inputHigh[Random.Range((int)1,(int)4)];
					}
					break;
				}

				sentence += " ";
				randomizePhrase = Random.Range((int)0,(int)2);
				switch (prefabSelectMedium)
				{
				case 1:
					sentence += inputMediumReply[randomizePhrase];
					break;
				case 2:
					sentence += inputMediumReply[randomizePhrase+3];
					break;
				case 0:
				default:
					if (inputSelectMedium > 0)
					{
						sentence += "" +  inputMedium[Random.Range((int)1,(int)3)];
					}
					break;
				}
			}
			else
			{
				sentence += "Unacceptable: " +  inputLowReply[Random.Range((int)0,(int)3)];
			}

			points -= Mathf.Clamp(((inputSelectHigh-1) * 5),0,10) + (inputSelectMedium>0?15:0) + (inputSelectLow>0?25:0);
			sentence += "+\n";
			randomizePhrase = Random.Range((int)0,(int)3);
			sentence += "Prefabs- ";
			if (prefabSelectLow == 0)
			{
				switch (prefabSelectHigh)
				{
				case 1:
					sentence += prefabHighReply[randomizePhrase];
					break;
				case 2:
					sentence += prefabHighReply[randomizePhrase+3];
					break;
				case 3:
					sentence += prefabHighReply[randomizePhrase+6];
					break;
				case 0:
				default:
					if (prefabSelectMedium == 0 || prefabSelectMedium == 0)
					{
						sentence += "As per the rubric: "+ prefabHigh[Random.Range((int)1,(int)4)];
					}
					break;
				}
				sentence += " ";
				randomizePhrase = Random.Range((int)0,(int)2);
				switch (prefabSelectMedium)
				{
				case 1:
					sentence += prefabMediumReply[randomizePhrase];
					break;
				case 2:
					sentence += prefabMediumReply[randomizePhrase+3];
					break;
				case 0:
				default:
					if (prefabSelectMedium > 0)
					{
						sentence += "" +  prefabMedium[Random.Range((int)1,(int)3)];
					}
					break;
				}
			}
			else
			{
				sentence += "Unacceptable: " +  prefabLowReply[Random.Range((int)0,(int)3)];
			}
			points -= Mathf.Clamp(((prefabSelectHigh-1) * 5),0,10)+ (prefabSelectMedium>0?15:0) + (prefabSelectLow>0?25:0);
			sentence += "+\n";
			randomizePhrase = Random.Range((int)0,(int)3);
			sentence += "Timer- ";
			if (timedSelectLow == 0)
			{
				switch (timedSelectHigh)
				{
				case 1:
					sentence += timedHighReply[randomizePhrase];
					break;
				case 2:
					sentence += timedHighReply[randomizePhrase+3];
					break;
				case 3:
					sentence += timedHighReply[randomizePhrase+6];
					break;
				case 0:
				default:
					if (timedSelectMedium == 0 || timedSelectMedium == 0)
					{
						sentence += "As per the rubric: "+ timedHigh[Random.Range((int)1,(int)4)];
					}
					break;
				}
				sentence += " ";
				randomizePhrase = Random.Range((int)0,(int)2);
				switch (timedSelectMedium)
				{
				case 1:
					sentence += timedMediumReply[randomizePhrase];
					break;
				case 2:
					sentence += timedMediumReply[randomizePhrase+3];
					break;
				case 0:
				default:
					if (timedSelectMedium > 0)
					{
						sentence += "" +  timedMedium[Random.Range((int)1,(int)3)];
					}
					break;
				}
			}
			else
			{
				sentence += "Unacceptable: " +  timedLowReply[Random.Range((int)0,(int)3)];
			}
			points -= Mathf.Clamp(((timedSelectHigh-1) * 5),0,10) + (timedSelectMedium>0?15:0) + (timedSelectLow>0?25:0);
			sentence += "+\n";
			randomizePhrase = Random.Range((int)0,(int)3);
			sentence += "Cohesion- ";
			if (cohesionSelectLow == 0)
			{
				switch (cohesionSelectHigh)
				{
				case 1:
					sentence += cohesionHighReply[randomizePhrase];
					break;
				case 2:
					sentence += cohesionHighReply[randomizePhrase];
					break;
				case 3:
					sentence += cohesionHighReply[randomizePhrase];
					break;
				case 0:
				default:
					if (cohesionSelectMedium == 0 || cohesionSelectMedium == 0)
					{
						sentence += "As per the rubric: "+ cohesionHigh[Random.Range((int)1,(int)4)];
					}
					break;
				}
				sentence += " ";
				randomizePhrase = Random.Range((int)0,(int)2);
				switch (cohesionSelectMedium)
				{
				case 1:
					sentence += cohesionMediumReply[randomizePhrase];
					break;
				case 2:
					sentence += cohesionMediumReply[randomizePhrase];
					break;
				case 0:
				default:
					if (cohesionSelectMedium > 0)
					{
						sentence += "" +  cohesionMedium[Random.Range((int)1,(int)3)];
					}
					break;
				}
			}
			else
			{
				sentence += "Unacceptable: " +  cohesionLowReply[Random.Range((int)0,(int)3)];
			}
			//sentence += " ";
//
			points -= Mathf.Clamp(((cohesionSelectHigh-1) * 5),0,10) + (cohesionSelectMedium>0?15:0) + (cohesionSelectLow>0?25:0);
			sentence = sentence.Replace(" \n","");
			sentence = sentence.Replace("+","");

			contactRequestMsg = "\nContact me " + (contactInfo == string.Empty?"":"at "+contactInfo) + " during office hours to review more specifics about your project";
			if (myBool == true)
			{
				sentence += contactRequestMsg;
			}
			Repaint();


			if (showstopperEnabled == true)
			{
				contactRequestMsg = "\nContact me " + (contactInfo == string.Empty?"":"at "+contactInfo) + " during office hours to review more specifics about your project";
				sentence = "A showstopper was triggered, please download the project you uploaded, verify its preparation against the showstoppers list https://docs.google.com/document/d/1uOjAW7fJYamwlcTAeEjFMVIC7SbC-IQKIVJHXDxTMIM/pub ";
				if (myBool)
				{
					sentence += "\n" + contactRequestMsg;
				}

				//	EditorGUIUtility.systemCopyBuffer = tStr;

			}
			EditorGUIUtility.systemCopyBuffer = sentence;
		}
		EditorGUILayout.EndHorizontal();
//		EditorGUILayout.LabelField("The following message will be appended if [RequestContact] is on.");
//		EditorGUILayout.LabelField(contactRequestMsg);

		//string tStr = EditorGUIUtility.systemCopyBuffer;


		//string concatenatedSentence = 
		//GUILayout.Label(sentence);
		EditorStyles.label.wordWrap = true;
		EditorGUILayout.BeginHorizontal();
		showstopperEnabled = GUILayout.Toggle(showstopperEnabled,"SHOWSTOPPER?",GUILayout.ExpandWidth(false));

		GUILayout.Label("Suggested Grade Percentage:",GUILayout.ExpandWidth(false));
		string dStr = points.ToString();
		if (showstopperEnabled == true)
		{
			dStr = "0";
		}
		EditorGUILayout.LabelField (dStr);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.LabelField("Message has been copied to the clipboard. You may add to it, select all + Copy/Paste. Press escape to reset.");
		//EditorGUILayout.LabelField(tStr,GUILayout.Height(70),GUILayout.ExpandHeight(true));
		EditorGUILayout.TextArea(sentence,GUILayout.Height(70),GUILayout.ExpandHeight(true));
	
		Repaint();

//		groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
//		//myBool = EditorGUILayout.Toggle ("Toggle", myBool);
//		myFloat = EditorGUILayout.Slider ("Slider", myFloat + points, -25, 25);
//		EditorGUILayout.EndToggleGroup ();
	}
}
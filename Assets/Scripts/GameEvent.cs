﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
//  Developer Name: 
        Keyvan Acosta ©      
//  Contribution:
Keyvan Acosta, Technical Design, Implementation
//  Feature        
        Generic Behavior System Event Class
//  Start & End dates
        June 1st, 2018
//  References:
        Unity Online Documentation,
//  Links:
        Original Design
//*/


public enum GameFunctions
{
    _NO_OP = 0,
    BEHAVIOR,
    CALL_EVENT,
    COMPARE_DATA,
    COMPUTE,
    DESTROY,
    GET_MOVE_DATA,
    IF_EVENT_ON,
    IMITATE,
    INFO_IO,
    INSTANTIATE,
    LOAD_SCENE,
    MODIFY_VALUES,
    MOTION,
    PAUSE,
    PLAY,
    RANDOMIZE,
    REGISTER,
    SEND_MSG,
    SET,
    TIMER,
    TOGGLE_EVENT,
};

public enum ObjectRelation
{
    _DEFAULT = 0,
    ME,
    IT,
    BOTH,
    ALL,
    ALL_EXCEPT,
    PARENT,
};

public enum ObjectNameType
{
    _DEFAULT = 0,
    TAG,
    NAME,
}
;

public enum ObjectRoot
{
    GLOBAL = 0,
    LOCAL,
    CAMERA,
    SCREEN,
    ABSOLUTE,
    RELATIVE,
};

public enum CallImmediateType
{
    IMMEDIATE = 0,
    DELAY,
};

public enum InteractionType
{
    KEYBOARD = 0,
    JOYPAD,
    MOUSE,
    INPUTMANAGER,
    TOUCH,
    OTHER,
};


public enum ObjectForce
{
    TRANSLATION = 0,
    FORCE,
    RELATIVEFORCE,
    TORQUE,
    RELATIVETORQUE,
    ROTATION,
    //	SET,
    //	STOP,
};


public enum PlayerFunctions
{
    _NOTHING = 0,
    AWAKE,
    COLLISIONENTER,
    COLLISIONSTAY,
    COLLISIONEXIT,
    INPUTPRESS,
    INPUTDOWN,
    INPUTANY,
    INPUTUP,
    MESSAGE_EVENT,
    START,
    SYSTEM,
    TRIGGERENTER,
    TRIGGERSTAY,
    TRIGGEREXIT,
    UPDATE,
    UPDATEFIXED,
    UPDATELATE,
};

//[System.Serializable]
//public class GameInput
//{
//	InteractionType it;
//	KeyCode key;
//
//};



[System.Serializable]
public class GameEvent
{

    //public unsigned int order;
    public string eventName;
    public float amount;
    public float otherAmount;
    public Vector3 vectorStorage;
    public string word;
    public PlayerFunctions playerAction;
    public GameFunctions gameAction;
    public ObjectRelation relation;
    public ObjectNameType filterType;
    public string group;
    public ObjectForce force;
    public string objFilter;
    public string msgFilter;
    public KeyCode key;
    public InteractionType inputType;
    public string inputString;
    public string letters;
    public bool showEvent;
    public Vector3 editorPosition;
    // running/working/engage/powered/connect/interfaced/attached/
    //	public bool engaged; // running/working/engage/powered/connect/interfaced/attached/
    public bool on;
    // running/executing/working/engage/powered/connect/interfaced/attached/
    public bool alive;
    //Loaded/alive/armed/standby/live/online/onair


    public GameEvent(
        bool _enabled,
        string _customName,
        InteractionType _inputType,
        KeyCode _key,
        string _inputString,
        string _letters,
        float _amount,
        float _oamount,
        Vector3 _dir,
        GameFunctions _action,
        PlayerFunctions _pAction,
        ObjectRelation _oRelation,
        ObjectNameType _oNameType,
        string _group,
        ObjectForce _force,
        bool _showEvent,
        string _word,
        string _msgFilter,
        string _objFilter,
        bool _alive
    )
    {
        eventName = _customName;
        key = _key;
        amount = _amount;

        vectorStorage = _dir;
        gameAction = _action;
        playerAction = _pAction;
        msgFilter = _msgFilter;
        objFilter = _objFilter;
        word = _word;
        relation = _oRelation;
        filterType = _oNameType;
        on = _enabled;
        group = _group;
        force = _force;
        letters = _letters;
        alive = _alive;
        showEvent = _showEvent;
        otherAmount = _oamount;
        //editorPosition = Vector3.zero;
    }

    public GameEvent()
    {
        eventName = "_BlankName";
        key = KeyCode.None;
        inputString = KeyCode.None.ToString();
        inputType = InteractionType.KEYBOARD;
        amount = 0;
        otherAmount = 0;
        vectorStorage = Vector3.zero;
        gameAction = GameFunctions._NO_OP;
        playerAction = PlayerFunctions._NOTHING;
        relation = ObjectRelation._DEFAULT;
        filterType = ObjectNameType._DEFAULT;
        group = string.Empty;//ObjectRoot.GLOBAL;
        force = ObjectForce.FORCE;
        word = "";
        letters = "";
        objFilter = "";
        msgFilter = "";
        on = false;
        alive = true;
        showEvent = true;
        otherAmount = amount;
        //editorPosition = Vector3.zero;
    }

    public void Init()
    {
        eventName = "_BlankName";
        key = KeyCode.None;
        inputType = InteractionType.KEYBOARD;
        inputString = KeyCode.None.ToString();
        amount = 0;
        otherAmount = 0;
        vectorStorage = Vector3.zero;
        gameAction = GameFunctions._NO_OP;
        playerAction = PlayerFunctions._NOTHING;
        relation = ObjectRelation._DEFAULT;
        filterType = ObjectNameType._DEFAULT;
        group = string.Empty;//ObjectRoot.GLOBAL;
        force = ObjectForce.FORCE;
        letters = "";
        word = "";
        objFilter = "";
        msgFilter = "";
        on = false;
        alive = true;
        showEvent = true;
        otherAmount = amount;
        //editorPosition = Vector3.zero;
    }

    public bool toggleOnOff()
    {
        on = !on;
        return on;
    }

    public bool alivetoggle()
    {
        alive = !alive;
        return alive;
    }

    public void act()
    {

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Developer Name: Shaun Sullivan
// Contribution: Code/Script
// Feature: NPC function for Black-Handed Spider Monkey
// Start & End dates: 10/28/2018 to 11/07/2018
// References: Keyvan Acosta, Items & Events, Unity Manual
// Links: https://online.fullsail.edu/class_sections/14838/modules/116043/activities/750903 
//        https://docs.unity3d.com/ScriptReference/Transform.Rotate.html
//        https://docs.unity3d.com/ScriptReference/GameObject.SetActive.html
//        https://docs.unity3d.com/ScriptReference/Collider.OnTriggerEnter.html

public class SRS_MonkeyMove : MonoBehaviour {

    public float Walk;
    public GameObject SRS_Poo;
    public GameObject SRS_TestPlayer;
    public GameObject SRS_Monkey;
    public bool isWalking;
    public bool Dazel_Wall;
    public bool PooTime;
    private Rigidbody rb;
   

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Walk = 0.1f;
        isWalking = true;
        Dazel_Wall = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (isWalking == true)
        {
            MoveMeForward();
        }
    }
    public void MoveMeForward()
    {
        transform.Translate(new Vector3(0, 0, Walk * 1));
    }
    private void OnTriggerEnter (Collider other)
    {
       if (Dazel_Wall == true && other.tag == "Dazel_Wall")
        {
            rb.transform.Rotate(0f, Random.Range(-120f, 120f), 0f);
            Dazel_Wall = false;
        }
        else if (Dazel_Wall == false && other.tag == "Dazel_Wall")
        {
            rb.transform.Rotate(0f, Random.Range(-120f, 120f), 0f);
            Dazel_Wall = true;
        }
        else if (other.tag == "NPCItem")
        {
            SRS_Poo.SetActive(false);
            Debug.Log ("Time to rain poo on the player");
            PooTime = true;
        }
        else
        {
            return;
        }
    }
}
